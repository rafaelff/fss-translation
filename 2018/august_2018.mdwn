# Free Software Supporter
Issue 124, August 2018

Welcome to the Free Software Supporter, the Free Software Foundation's
(FSF) monthly news digest and action update -- being read by you and
191,868 other activists. That's 1,847 more than last month!

### FSF job opportunity: Copyright and Licensing Associate 

*From July 9*

The FSF seeks a motivated and talented
Boston-based individual to be our full-time Copyright and Licensing
Associate. This position, reporting to the executive director, works
as part of our licensing and compliance team to protect and promote
the use of freely licensed works of software and documentation. For
this position, we are looking for a strong writer who is familiar with
free software copyright licenses, and understands the basics of how
software is written, compiled, and distributed. Neither a legal nor
computer science education is required, but both would be a
plus. Ideal candidates will also have experience with administrative
tasks and record keeping. Visit this link to learn how to apply.

 * <https://www.fsf.org/news/fsf-job-opportunity-copyright-and-licensing-associate-1>

## TABLE OF CONTENTS

* A key victory against European copyright filters and link taxes -- but what's next?
* Trump's Supreme Court nominee is a major net neutrality opponent
* RMS, May 2018: Photos from Brazil and Argentina 
* How ProPublica Illinois uses GNU Make to load 1.4GB of data every day
* *Metal Gear Rising* Mac unplayable because of Digital Restrictions Management (DRM)
* GNOME Foundation opens recruitment for further expansion
* Introducing Alyssa Rosenzweig, intern with the FSF tech team 
* Sonali's progress on the Free Software Directory, weeks 1-2 
* David's progress on the Free Software Directory, internship weeks 2-3 
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Hardware/Workarounds
* GNU Spotlight with Mike Gerwitz: 15 new GNU releases!
* GNU Toolchain update: Support GNU Toolchain
* Richard Stallman's speaking schedule and other FSF events
* Thank GNUs!
* GNU copyright contributions
* Take action with the FSF!

View this issue online here: <https://www.fsf.org/free-software-supporter/2018/august>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui: <https://www.fsf.org/free-software-supporter/2018/agosto-spanish>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici: <https://www.fsf.org/free-software-supporter/2018/aout>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em Português. Para ver a
versão em Português, clique aqui: <https://www.fsf.org/free-software-supporter/2018/agosto-portuguese>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em Português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

#

### A key victory against European copyright filters and link taxes -- but what's next?

*From July 16 by Danny O'Brien*

Against all the odds, but with the support of nearly a million
Europeans, Members of the European Parliament (MEPs) voted earlier
this month to reject the European Union's [proposed copyright
reform](https://www.fsf.org/blogs/community/take-a-stand-before-july-5th-contact-your-mep-today)
-- including controversial proposals to create a new "snippet" right
for news publishers, and mandatory copyright filters for sites that
published user uploaded content. Now all MEPs will have a chance in
September to submit new amendments and vote on a final text, or reject
the directive entirely. To make sure the right amendments pass in
September, we need to keep that conversation going.

 * <https://www.eff.org/deeplinks/2018/07/key-victory-against-european-copyright-filters-and-link-taxes-whats-next>

The FSF was part of the effort to urge Europeans to contact their MEPs
and demand that they reject the copyright filters: thank you so much
to our supporters who participated, and great work!

 * <https://www.fsf.org/blogs/community/take-a-stand-before-july-5th-contact-your-mep-today>

### Trump's Supreme Court nominee is a major net neutrality opponent
President Donald Trump has nominated federal Judge Brett Kavanaugh, a major opponent of net neutrality, to the United States Supreme Court.
Kavanaugh argued that net neutrality rules were unconstitutional
because they violated the free speech rights of Internet Service
Providers (ISPs). Access to the tools we use to build, download, and
share free software are at risk when net neutrality is no longer there
to protect them -- and users -- from ISPs controlling access to those
sites.

 * <https://motherboard.vice.com/en_us/article/43pyxd/one-of-trumps-top-scotus-contenders-is-a-major-net-neutrality-opponent>

### RMS, May 2018: Photos from Brazil and Argentina 

*From July 2*

Free Software Foundation president Richard Stallman (RMS) went on a
twelve-city visit to Brazil and Argentina this past May and June. The
trip took him to the Federal Institute of São Paulo, in Araraquara,
São Paulo, Brazil, where, on May 14th, he gave his speech, "A Free
Digital Society." The event was well-attended, with universities and
schools from neighboring cities having organized to shuttle their
students to it by bus.

 * <https://www.fsf.org/blogs/rms/may-2018-photos-from-brazil-and-argentina>

### How ProPublica Illinois uses GNU Make to load 1.4GB of data every day

*From July 10 by David Eads*

I avoided using GNU Make in my data journalism work for a long time,
but this year, I needed to load 1.4GB of Illinois political
contribution and spending data every day, and the extract, transform,
load (ETL) process was taking hours, so I gave Make another
chance. Now the same process takes less than 30 minutes -- it's both
practical AND free!

 * <https://www.propublica.org/nerds/gnu-make-illinois-campaign-finance-data-david-eads-propublica-illinois>

### *Metal Gear Rising* Mac unplayable because of Digital Restrictions Management (DRM)

*From June 25 by Astrid Johnson*

Proprietary software is an injustice and it breeds more injustice. A
recent example: the *Metal Gear Rising* Mac version has suddenly
become completely unplayable. The Mac port of the game included DRM
that made a constant online connection necessary. Now that the port
developer has shut down, there’s no server for the game to contact,
rendering it useless. This is a fine example of how any item with DRM
is damaged right out of the gate (or, if you prefer, [Defective By
Design](https://www.defectivebydesign.org)). Join us to protest this
disgrace and others on the [International Day Against
DRM](https://www.defectivebydesign.org/dayagainstdrm) on September 18,
2018!

 * <http://www.gamerevolution.com/news/400087-metal-gear-rising-mac-unplayable-drm>

### GNOME Foundation opens recruitment for further expansion

*From July 6*

The GNOME Foundation has announced a number of positions it is
recruiting for to help drive the GNOME Project and free software,
thanks to a generous grant that the Foundation has received. The
positions are: development coordinator, program coordinator,
devops/sysadmin, and GTK+ core developer. See the link below to apply.

 * <https://www.gnome.org/news/2018/07/gnome-foundation-opens-recruitment-for-further-expansion/>

### Introducing Alyssa Rosenzweig, intern with the FSF tech team 

*From July 12*

Howdy there, fellow cyber denizens; 'tis I, Alyssa Rosenzweig, your
friendly local biological life form! I'm a certified goofball,
licensed to be silly under the GPLv3, but more importantly, I'm
passionate about free software's role in society. I'm excited to join
the Free Software Foundation as an intern this summer to expand my
understanding of our movement.

 * <https://www.fsf.org/blogs/sysadmin/introducing-alyssa-rosenzweig-intern-with-the-fsf-tech-team>

### Sonali's progress on the Free Software Directory, weeks 1-2 

*From July 12*

The last few weeks have been very enlightening. I learned about
MediaWiki extensions, like MobileFrontend, CSS, vim, and other mobile
extensions. I installed MobileFrontend, and resolved a few issues I
faced regarding HeaderTabs and in-line view. It feels great to have
been able to get the basic structure for mobile view by now.

 * <https://www.fsf.org/blogs/sysadmin/sonalis-progress-on-the-free-software-directory-weeks-1-2>

### David's progress on the Free Software Directory, internship weeks 2-3 

*From July 20*

I'm working on creating a list of free software extensions for
Mozilla-based browsers on the [Free Software
Directory](https://directory.fsf.org/wiki/Main_Page) based on data
from
[addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/). This
is needed because the official extensions repository includes many
proprietary extensions.

 * <https://www.fsf.org/blogs/sysadmin/davids-progress-on-the-free-software-directory-internship-weeks-2-3>

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, August 3, from 12pm to 3pm EDT (16:00 to
19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Free_Software_Directory:Meetings>

### LibrePlanet featured resource: Hardware/Workarounds

Every month on LibrePlanet, we highlight one resource that is
interesting and useful -- often one that could use your help.

For this month, we are highlighting Hardware/Workarounds, which
provides information about how to work around the device features that
may be missing when you're using a fully free operating system; the
missing components can be worked around to enable a fully free
operating system with minimal compromises on usability. You are
invited to adopt, spread and improve this important resource.

  * <https://libreplanet.org/wiki/Hardware/Workarounds>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 15 new GNU releases!

* [autogen-5.18.14](https://www.gnu.org/software/autogen/)
* [binutils-2.31.1](https://www.gnu.org/software/binutils/)
* [coreutils-8.30](https://www.gnu.org/software/coreutils/)
* [gcc-8.2.0](https://www.gnu.org/software/gcc/)
* [gdbm-1.16](https://www.gnu.org/software/gdbm/)
* [gnupg-2.2.9](https://www.gnu.org/software/gnupg/)
* [gnu-pw-mgr-2.3.3](https://www.gnu.org/software/gnu-pw-mgr/)
* [gnutls-3.6.3](https://www.gnu.org/software/gnutls/)
* [guile-2.2.4](https://www.gnu.org/software/guile/)
* [guix-0.15.0](https://www.gnu.org/software/guix/)
* [libextractor-1.7](https://www.gnu.org/software/libextractor/)
* [libredwg-0.5](https://www.gnu.org/software/libredwg/)
* [linux-libre-4.17.9-gnu](https://www.gnu.org/software/linux-libre/)
* [parallel-20180722](https://www.gnu.org/software/parallel/)
* [tramp-2.4.0](https://www.gnu.org/software/tramp/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>. You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

This month, we welcome Jan Nieuwenhuizen as maintainer of the new GNU
Mes.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### GNU Toolchain update: Support GNU Toolchain

Donate to support the GNU Toolchain, a collection of foundational
freely licensed software development tools including the [GNU C
Compiler collection (GCC)](https://gcc.gnu.org/), the [GNU C Library
(glibc)](https://www.gnu.org/software/libc/libc.html), and the [GNU
Debugger (GDB)](https://sourceware.org/gdb/).

* <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57>

### Richard Stallman's speaking schedule

For event details, as well as to sign-up to be notified for future
events in your area, please visit <https://www.fsf.org/events>.

So far, Richard Stallman has the following event this month:

 * August 27, 2018, Brussels, Belgium, ["Contrôle ton ordinateur pour ne pas être contrôlé!"](https://www.fsf.org/events/rms-20180827-brussels)

### Other FSF and free software events

 * August 2, 2018, Hsinchu City, Taiwan, [Karen Sandler and Molly de Blanc, "That's a free software issue!"](https://www.fsf.org/events/karen-sandler-molly-deblanc-20180802-hsinchucity-debconf)
 * August 3, 2018, Hsinchu City, Taiwan, [Molly de Blanc and John Sullivan, "The Free Software Foundation, Debian, and the free software movement"](https://www.fsf.org/events/molly-deblanc-john-sullivan-20180803-hsinchucity-debconf)
 * November 9, 2018, Seattle, WA, [SeaGL 2018](https://www.fsf.org/events/conference-20181109-seagl-seattle)
 * November 23, 2018, Las Palmas de Gran Canaria, Spain, [GNU Health Con 2018](https://www.fsf.org/events/event-20181123-laspalmas-gnuhealthcon)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2018supporters.html>

This month, a big Thank GNU to:

* Arthur Gleckler
* Blue Systems
* Boone Gorges
* Clint Priest
* Daniel Hoodin
* David Turner
* John Sullivan
* Kendall Griffith
* Martin Krafft
* Mridul Muralidharan
* René Genz
* Tegonal GmbH
* Todd Weaver
* Yidong Chong

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GPL and keep software free. The following individuals have
assigned their copyright to the FSF in the past month:

* Federico Tedin (GNU) (Emacs)
* Siraphob Phipathananunth (GNU) (Emacs)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something
here for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software
patents, Digital Restrictions Management (DRM), free software
adoption, OpenDocument, Recording Industry Association of America
(RIAA), and more.


###

Copyright © 2018 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

