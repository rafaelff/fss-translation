# Suporte ao Software Livre
Edição 121, maio de 2018

Bem-vindo(a) ao Suporte ao Software Livre, um resumo de notícias e atualização mensal da Free Software Foundation (FSF) -- sendo lido por você e 187.209 outros ativistas. São mais 610 este mês!

### O *Suporte ao Software Livre* está disponível agora em Português do Brasil!

Liberdade em Software é uma questão mundial, e nós temos uma missão
global para defender os direitos de todos os usuários de software em
todos os lugares. Assim, nós fazemos todo o esforço para ter os
materiais da Free Software Foundation (FSF) e o Projeto GNU traduzidos
em todos os idiomas possíveis. Graças à sugestão de um de nossos
maravilhosos voluntários, estamos lançando uma edição do boletim mensal
do *Suporte ao Software Livre* em Português do Brasil! Se você quer
receber a versão por e-mail do *Suporte* em Português, clique
[aqui](https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum})
e simplesmente atualizar sua preferência de idioma. (Atenção -- não
compartilhe esse _link_, pois ele permitirá a outros modificar sua conta.)

  * <https://www.fsf.org/blogs/community/the-monthly-free-software-supporter-newsletter-is-now-available-in-brazilian-portuguese>

## ÍNDICE

 * Richard Stallman: "Nenhuma empresa é tão importante que sua existência justifique a criação de um estado policial"
 * RMS em * The Guardian *: “Uma proposta radical para manter seus dados pessoais seguros”
 * “A história da impressora” revivida: um depoimento sobre a injustiça do _firmware_ proprietário
 * Conheça os mais recentes drones com Digital Restrictions Management (DRM)
 * Private Internet Access: VPNs, educação e liberdade de software
 * Trisquel 8.0 LTS Flidas
 * As inscrições estão abertas para a GNU Health Con 2018!
 * A AMD promete correções de _firmware_ para _bugs_ relativos à segurança do microprocessador, mas não corrigirá o problema fundamental
 * Contrato de serviços da Microsoft impede o uso de linguagem ofensiva
 * Free Software Foundation anuncia campanha de crowdfunding para comprar o Facebook (piadas de 1º de abril!)
 * Empresas de eletrodomésticos estão fazendo lobby para proteger seus monopólios de conserto alimentados por DRM
 * Mais entradas para o "Catálogo de Dispositivos Desaparecidos", cortesia dos apoiadores da Electronic Frontier Foundation (EFF)
 * GnuCash 3.0 lançado
 * TorBirdy 0.2.4 foi lançado
 * Venda de benefícios GPL: Impressoras LulzBot 3D recondicionadas
 * Recapitulação de reunião do Diretório de Software Livre
 * Junte-se à FSF e amigos na atualização do Diretório de Software Livre
 * Recurso do LibrePlanet em destaque: FreedSoftware
 * Holofote GNU com Mike Gerwitz: 14 novos lançamentos GNU!
 * Atualização do GNU Toolchain: Suporte ao GNU Toolchain
 * Programação das palestras do Richard Stallman
 * Agradecimentos do GNU!
 * Contribuições aos diretos autorais do GNU
 * Entre em ação com a FSF!

Veja esta edição _online_ aqui: <https://www.fsf.org/free-software-supporter/2018/may>

Incentive seus amigos a se inscrever e nos ajudar a construir um
público adicionando nosso _widget_ de assinante ao seu site.

  * Assine: <https://www.fsf.org/free-software-supporter>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma edição? Você pode recuperar o atraso em
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui: <https://www.fsf.org/free-software-supporter/2018/mayo>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici: <https://www.fsf.org/free-software-supporter/2018/mai>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em Português. Para ver a
versão em Português, clique aqui:
<https://www.fsf.org/free-software-supporter/2018/maio>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em Português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

#

### Richard Stallman: "Nenhuma empresa é tão importante que sua existência justifique a criação de um estado policial"

*De 18 de abril por Noah Kulwin*

Nesta entrevista, parte da série "A Internet pede desculpas",
o presidente da FSF, Richard Stallman (RMS), discute direitos de
privacidade, Facebook, o mau uso dos dados (e, portanto, por que
os dados não devem ser coletados em primeiro lugar) e muito mais.

   * <http://nymag.com/selectall/2018/04/richard-stallman-rms-on-privacy-data-and-free-software.html>

### RMS in *The Guardian*: “Uma proposta radical para manter seus dados pessoais seguros”

*De 9 de abril*

Algo precisa ser feito para deter o abuso generalizado sobre os
dados e, na edição de hoje do * The Guardian *, RMS oferece uma
proposta ousada: sistemas precisam ser legalmente obrigados a não
coletar dados em primeiro lugar. "O princípio básico é que um sistema
deve ser projetado para não coletar certos dados," ele escreve,
"se sua função básica puder ser realizada sem esses dados." Ele
exige que essa mudança vá bem mais longe do que Facebook também.

   * <https://www.fsf.org/blogs/rms/rms-in-the-guardian-201ca-radical-proposal-to-keep-your-personal-data-safe201d-1>
   * <https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance>

### “A história da impressora” revivida: um depoimento sobre a injustiça do _firmware_ proprietário

*De 26 de abril*

Como parte de sua explicação sobre o início da história do software
livre, Richard Stallman (RMS) frequentemente conta uma história sobre
sua frustração com o software bloqueado em uma impressora a laser.
No depoimento de hoje, um apoiador anônimo detalha um incidente muito
mais recente que mostrou  concretamente a ele como são injustas, cruéis
e arbitrárias podem ser as restrições impostas pelos fabricantes de software.

   * <https://www.fsf.org/blogs/community/201cthe-printer-story201d-redux-a-testimonial-sobre-a-iniciali-de-firmware-propriedade>

### Conheça os mais recentes drones com Digital Restrictions Management (DRM)

*De 18 de abril*

De volta [o último
round](https://www.defectivebydesign.org/blog/help_fight_against_dmca_anticircumvention_rules_december_15th)
do processo de isenções antievasão Digital Millennium Copyright Act (DMCA),
escrevemos sobre os "drones DRM" que estavam se opondo a isenções.
Essas empresas, juntamente com o Departamento de Meio Ambiente dos EUA,
Agência de Proteção Ambiental (EPA), estava tentando sufocar o trabalho
de ativistas atuando para recuperar um pouco de liberdade em face das
provisões draconianas do DMCA. Alguns anos depois, e é uma nova rodada
do processo de isenções, talvez com alguns rostos novos, mas o mesmo
problema.

   * <https://www.defectivebydesign.org/blog/meet_latest_drm_drones>

### Private Internet Access: VPNs, educação e liberdade de software

*De 13 de abril*

A Private Internet Access foi uma generosa defensora do LibrePlanet 2018
e tem a Free Software Foundation como patrono. Sendo um dos maiores
Serviços VPN disponíveis, eles têm clientes em todo o mundo. Eles
recentemente anunciaram sua intenção de lançar alguns dos softwares
que eles produzem sob uma licença livre.

   * <https://www.fsf.org/blogs/community/private-internet-access-vpns-education-and-software-freedom>

### Trisquel 8.0 LTS Flidas lançado

*De 5 de abril por Ruben Rodriguez*

O Trisquel 8.0 se beneficiou de testes exaustivos, uma vez que muitas
pessoas usaram as versões de desenvolvimento como seu principal sistema
operacional por algum tempo. Além disso, a Free Software Foundation
vem usando-o para executar a conferência LibrePlanet desde o ano passado,
e está sendo usado para alimentar toda a sua nova infraestrutura de
servidores!

   * <https://trisquel.info/en/trisquel-80-lts-flidas>

### Inscrições abertas para o GNU Health Con 2018!

*De 22 de abril por Luis Falcón Martín, MD*

O GNU Health Con é uma conferência anual que reúne
entusiastas e desenvolvedores do Sistema de Informações
Free/Libre Health & Hospital, e este é o seu décimo aniversário!
Eventos incluem sessões temáticas, oficinas gratuitas, a apresentação
do Prêmio de Medicina Social da Saúde GNU, um jantar de 10º aniversário
e um GNU Helth Hack Day. A conferência acontece de 23 a 25 de novembro,
em Las Palmas, Espanha.

   * <http://www.gnuhealthcon.org/2018-las_palmas/>

O Dr. Falcón também apresentou uma palestra sobre o GNU Health no
LibrePlanet 2018, "Software livre como catalisador da libertação,
justiça social e medicina social."

   * <https://media.libreplanet.org/u/libreplanet/m/free-software-as-a-catalyst-for-liberation-social-justice-and-social-medicine/>

### A AMD promete correções de _firmware_ para _bugs_ relativos à segurança do microprocessador, mas não corrigirá o problema fundamental

*De 20 de março por Peter Bright*

A AMD respondeu aos relatórios da semana passada sobre uma série
de falhas de segurança afetando seu Platform Security Processor (PSP)
e o chipset. o empresa reconhece os erros e diz que, nas próximas
semanas, ela terá  um novo firmware disponível para resolver os erros
do PSP. Essas correções no firmware também irão mitigar os erros do
chipset. Essas falhas de segurança são ruins, mas elas ainda são
secundárias à injustiça da Platform Security Processor (PSP), que é
um _back door_ intencional que você não pode controlar. As correções
da AMD não resolvem isso.

   * <https://arstechnica.com/gadgets/2018/03/amd-promises-firmware-fixes-for-security-processor-bugs/>

### Contrato de serviços da Microsoft impede o uso de linguagem ofensiva

*De 28 de março por Simon Sharwood*

A Microsoft impôs censura no Skype e e-mail, bem como em sua
suíte de escritório, que é conectada a um servidor Microsoft, e vários
outros desserviços. Nosso conselho, claro, não é "garantir que você
fique dentro do código de conduta" -- simplesmente feche suas contas!
Como está, significa que eles já estão forçando você a usar software
não-livre, e isso já é uma injustiça.

   * <https://www.theregister.co.uk/2018/03/28/microsoft_services_agreement_bars_offensive_language/>

### Free Software Foundation anuncia campanha de crowdfunding para comprar o Facebook (piadas de 1º de abril!)

*De 1º de abril*

"Quando recebemos uma doação de 91,45 Bitcoins de fundo anônimo, 
planejamos usar o dinheiro para vários propósitos, incluindo levar o
movimento de software livre a novas audiências ", disse o diretor
executivo da FSF John Sullivan, "O recente escândalo sobre a ilegalidade
da coleta em massa de dados de usuários do Facebook pela Cambridge Analytica
deixou o futuro do site no ar. Nossa equipe de administradores de sistemas
está pronta para revisar todo o _site_ para se tornar a maior plataforma
de media social a respeitar a privacidade, o software livre e baseada
em Javascript livre. Ainda não temos certeza de qual nome dar ao novo
_site_, mas temos certeza que envolverá um inteligente acrônimo."

   * <https://www.fsf.org/blogs/community/free-software-foundation-announces-crowdfunding-campaign-to-buy-facebook>

### Empresas de eletrodomésticos estão fazendo lobby para proteger seus monopólios de conserto alimentados por DRM

*De 25 de abril por Jason Koebler*

Os argumentos que Dyson, LG e Wahl estão usando para justificar a
manutenção de software bloqueado em aparelhos estão ficando cada vez
mais tolos a cada dia. Primeiro, eles estão alegando que seus produtos
são muito complexos para pessoas comuns aprenderem a reparar, e se você
tentar reparar a sua navalha elétrica, você vai acabar queimando sua casa;
em seguida, estão reivindicando que se você deixar um profissional
independente em sua casa para reparar seu aspirador de pó ou torradeira,
eles vão roubar sua casa (que, de alguma forma, um certificado da empresa
impedirá isso automaticamente).

   * <https://motherboard.vice.com/en_us/article/vbxk3b/appliance-companies-are-lobbying-against-right-to-repair>

### Mais entradas para o "Catálogo de Dispositivos Desaparecidos", cortesia dos apoiadores da Electronic Frontier Foundation (EFF)

*De 10 de abril por Cory Doctorow*

Não temos certeza de quão realistas seriam estes "dispositivos
desaparecidos" que apoiadores da EFF trouxeram mesmo sem as restrições
antiéticas do DMCA, mas é uma ótima ilustração de como a criatividade
poderia florescer se o DRM não estivesse nos impedindo.

   * <https://www.eff.org/deeplinks/2018/04/entries-catalog-missing-devices-courtesy-eff-supporters-you>

### GnuCash 3.0 lançado

*De 3 de abril por Jonathan Corbet*

A equipe de desenvolvimento do GnuCash orgulhosamente anuncia o
GnuCash 3.0, o primeiro lançamento em sua nova série estável 3.x.

   * <https://lwn.net/Articles/750813/>

### TorBirdy 0.2.4 foi lançado

*De 2 de abril por Sukhbir Singh*

TorBirdy é uma extensão do Mozilla Thunderbird que o configura
para fazer conexões através da rede Tor. TorBirdy aprimora
automaticamente as configurações de privacidade do Thunderbird
e o configura para uso sobre o Tor - pense nisso como botão Tor
para o Thunderbird.

   * <https://blog.torproject.org/torbirdy-024-released>

### Venda de Benefícios GPL: Impressoras LulzBot 3D recondicionadas

*De 17 de abril por Aleph Objects*

Você planeja comprar uma impressora 3D? Obtenha uma impressora mais
ética e ao mesmo tempo ajude a FSF: em homenagem à GPL, Aleph está
dando a clientes 15% de desconto em cada impressora 3D recondicionada
LulzBot, e doando $50 para nós!

   * <https://www.lulzbot.com/gpl-benefit-sale-refurbished-3d-printers>

### Recapitulação de reunião do Diretório de Software Livre

Confira o ótimo trabalho que nossos voluntários realizaram nas reuniões
de abril do Diretório de Software Livre. Toda semana ativistas de software
livre de todo o mundo se reúnem no canal #fsf do irc.freenode.org
para ajudar a melhorar o Diretório de Software Livre.

   * <https://www.fsf.org/blogs/directory/free-software-directory-meeting-recap-april-2018>

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam o directory.fsf.org todos
os meses para descobrir software livre. Cada entrada no diretório
contém uma riqueza de informações úteis, da categoria básica e
descrições à  controle de versão, canais de IRC, documentação e
licenciamento. O Diretório  de Software Livre tem sido um ótimo
recurso para usuários de software na última  década, mas precisa
de sua ajuda para se manter atualizados projetos emocionantes de
software livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org,
e geralmente incluem um punhado de participantes regulares, bem como
recém-chegados. O Freenode é acessível a partir de qualquer cliente
de IRC -- Todos são bem-vindos!

A próxima reunião é sexta-feira, 4 de maio, das 12h às 15h (16h
19:00 UTC). Detalhes aqui:

   * <https://directory.fsf.org/wiki/Free_Software_Directory:Meetings>

### Recurso do LibrePlanet em destaque: FreedSoftware

Todos os meses no LibrePlanet, destacamos um recurso que é
interessante e útil -- muitas vezes algum que poderia se
beneficiar de sua ajuda.

Para este mês, estamos destacando o FreedSoftware, que fornece
coordenação para projetos de software a fim de preencher lacunas
e possibilitar distribuições de software GNU/Linux 100% livres.
Você está convidado a adotar, divulgar e melhorar este importante
recurso.

   * <https://libreplanet.org/wiki/Group:FreedSoftware>

Tem uma sugestão para o recurso em destaque do próximo mês? Envie 
para <campanhas@fsf.org>.

### Holofote GNU com Mike Gerwitz: 14 novos lançamentos GNU!

* [aspell6-en-2018.04.16-0] (https://www.gnu.org/software/aspell/)
* [gama-1.22] (https://www.gnu.org/software/gama/)
* [gnudos-1.11] (https://www.gnu.org/software/gnudos/)
* [gnupg-2.2.6] (https://www.gnu.org/software/gnupg/)
* [gnurl-7.59.0] (https://lists.gnu.org/archive/html/info-gnu/2018-04/msg00003.html)
* [libidn-1.34] (https://www.gnu.org/software/libidn/)
* [linux-libre-4.16.4-gnu] (https://www.gnu.org/software/linux-libre/)
* [mcron-1.1.1] (https://www.gnu.org/software/mcron/)
* [nano-2.9.6] (https://www.gnu.org/software/nano/)
* [paralelo-20180422] (https://www.gnu.org/software/parallel/)
* [sed-4.5] (https://www.gnu.org/software/sed/)
* [taler-bank-0.5.1] (https://www.gnu.org/software/taler/)
* [taler-exchange-0.5.0] (https://www.gnu.org/software/taler/)
* [taler-comerciante-0.5.0] (https://www.gnu.org/software/taler/)

Para receber os anúncios da maioria dos novos lançamentos do GNU,
assine a lista de discussão info-gnu:
<https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para fazer o download: quase todo o software GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou preferencialmente um dos seus espelhos
<https://www.gnu.org/prep/ftp.html>. Você pode usar a URL
<https://ftpmirror.gnu.org/> para ser automaticamente redirecionado
para um (esperamos) espelho próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como
um todo, estão procurando por mantenedores e outras formas de
assistência: veja <https://www.gnu.org/server/takeaction.html#unmaint>
se você quiser dar uma ajuda. A página geral sobre como ajudar o GNU
está em <https://www.gnu.org/help/help.html>.

Se você tem um programa de trabalho integral ou parcial que você
gostaria de oferecer ao projeto GNU como sendo um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, sinta-se à vontade para nos escrever em <maintainers@gnu.org>
com quaisquer questões ou sugestões sobre o GNU para futuras análises.

### Atualização do GNU Toolchain: Suporte ao GNU Toolchain

Doe para apoiar o GNU Toolchain, uma coleção de
ferramentas de desenvolvimento de software licenciadas livremente,
incluindo o [GNU C Compiler collection (GCC)](https://gcc.gnu.org/),
a [Biblioteca GNU C](glibc)](https://www.gnu.org/software/libc/libc.html),
e o [GNU Debugger (GDB)](https://sourceware.org/gdb/).

* <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57>

### Programação das palestras do Richard Stallman

Para detalhes do evento, bem como para se inscrever e ser notificado
dos futuros eventos em sua área, visite <https://www.fsf.org/events>.

Até agora, Richard Stallman tem os seguintes eventos neste mês:

  * 2 de maio de 2018, Montreal, Canadá, ["Enseignement et liberté"](https://www.fsf.org/events/rms-20180502-montreal)
  * 19 de maio de 2018, Misiones Posadas, Argentina, ["Title TBA"](https://www.fsf.org/events/rms-20180519-misionesposadas)
  * 21 de maio de 2018, Tucumán, Argentina, ["Title TBA"](https://www.fsf.org/events/rms-20180521-tucuman)
  * 24 de maio de 2018, Mendoza, Argentina, ["El software libre en la ética y en la prática"](https://www.fsf.org/events/rms-20180524-mendoza)
  * 26 de maio de 2018, Río Cuarto, Argentina, ["Title TBA"](https://www.fsf.org/events/rms-20180526-riocuarto)
  * 30 de maio de 2018, Buenos Aires, Argentina, ["Copyright vs Comunidad"](https://www.fsf.org/events/rms-20180530-buenosaires)

### Agradecimentos do GNU!

Agradecemos a todos que doam para a Free Software Foundation,
e gostaríamos de dar reconhecimento especial para as pessoas que 
doaram US$500 ou mais no último mês.

  * <https://www.gnu.org/thankgnus/2018supporters.html>

Este mês, um grande agradecimento GNU para:

* Alexandre Blanc
* Cătălin Frâncu
* Donald e Jill Knuth
* Donnie Pennington
* Edward Flick
* Hiroshi Takekawa
* Ms. Leah Rowe
* Nicolae Carabut

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Contribuições aos diretos autorais do GNU

Atribuir seus direitos autorais à Free Software Foundation nos ajuda
defender a GPL e manter o software livre. Os seguintes indivíduos 
atribuíram seus direitos autorais à FSF no mês passado:

* John Shahid (Emacs)
* Muir Manders (Emacs)
* Robert Michael Irelan (Emacs)
* Ruslan Bukin (GCC) (GNU Binutils) (GDB)
* Russell Black (Emacs)
* Tee Kiah Chia (GNU Binutils)
* Xi Xu (Emacs)
* Yuya Minami (Emacs)

Quer ver seu nome nesta lista? Contribua para o GNU e atribua seus
direitos autorais à FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Entre em ação com a FSF!

Contribuições de milhares de membros individuais tornam possível o
trabalho da FSF. Você pode contribuir juntando-se a nós em
<https://my.fsf.org/join>. E se você já é um membro, pode ajudar a
indicar novos membros (e ganhar algumas recompensas) adicionando uma
linha com o seu número de membro à assinatura do seu e-mail como por exemplo:

   Sou um membro da FSF -- Ajude-nos a apoiar a liberdade do software!
   <https://my.fsf.org/join>

A FSF está sempre procurando voluntários
(<https://www.fsf.org/volunteer>). De ansiosos para ajudar até _hackers_,
na coordenação de tarefas até o envelopamento de cartas - há algo
aqui para todo mundo fazer. Além disso, vá na nossa seção de campanhas
(<https://www.fsf.org/campaigns>) e aja em relação às patentes de software,
Digital Restrictions Management (DRM), adoção de software livre,
OpenDocument, Associação da Indústria de Gravação da América
(Recording Industry Association of America - RIAA) e muito mais.


###

Copyright © 2018 Free Software Foundation, Inc.

Este trabalho está licenciado sob a Licença Creative Commons Atribuição 4.0
Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>.

