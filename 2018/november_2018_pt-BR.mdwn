# Suporte ao Software Livre
Edição 127, novembro de 2018

Bem-vindo(a) ao Suporte ao Software Livre, um resumo de notícias e
atualização mensal da Free Software Foundation (FSF) -- sendo lido
por você e mais 194.208 outros ativistas. São 635 a mais em relação
ao mês passado!

### Prazo final da chamada para as Sessões do LibrePlanet 2019 estendido até 9 de novembro

*23 de outubro*

Você ainda não enviou uma palestra para o [LibrePlanet
2019](https://libreplanet.org/2019/)? [A Chamada para as Sessões
(CfS)](https://my.fsf.org/lp-call-for-sessions) foi estendida
até sexta-feira, 9 de novembro de 2018 às 10:00 EST (14:00 UTC).
Visite <https://my.fsf.org/lp-call-for-sessions> e faça o login
para enviar sua sessão.

LibrePlanet é uma conferência anual organizada pela Free Software
Foundation (FSF), que explora a interseção da tecnologia e justiça
social através de uma variedade de palestras e atividades. Todo ano,
o LibrePlanet reúne desenvolvedores, especialistas em políticas
públicas, ativistas, hackers e usuários finais para aprender novas
habilidades, compartilhar realizações e enfrentar desafios relativos
à liberdade de computação, como uma comunidade.
O tema do LibrePlanet 2019 é “Software Livre Pioneiro”, e será
realizado no sábado e domingo, 23 e 24 de março na Grande Boston, MA.

 * <https://www.fsf.org/blogs/community/libreplanet-2019-call-for-sessions-deadline-extended-until-nov-9th>

## ÍNDICE

* Oportunidade de trabalho na FSF: Gerente de Programa
* Anunciando os palestrantes principais do LibrePlanet - e não perca sua chance de dar uma palestra
* Declaração da FSF sobre a adesão da Microsoft à Open Invention Network
* Apresentando nosso novo fórum de associados!
* Isenções de DMCA concedidas, mas o processo geral ainda está quebrado 
* Os novos bloqueios de software proprietário da Apple eliminarão os reparos independentes em novos MacBook Pros
* A cada minuto, durante três meses, a GM coletou secretamente dados sobre os hábitos e locais de escuta de rádio de 90.000 motoristas.
* Obrigado por participar no Dia Internacional Contra o DRM 2018!
* Biografia inovadora do RMS retorna ao GNU Press Shop
* Guia de computador de placa única atualizado: o software livre está ganhando no ARM!
* A conclusão do trabalho de estágio Outreachy da Sonali no Diretório de Software Livre
* GNU Compiler Collection (GCC) 6.5 lançado
* Junte-se à FSF e amigos na atualização do Diretório de Software Livre
* Recurso em destaque no LibrePlanet: Community Toolkit
* Holofote GNU com Mike Gerwitz: 8 novos lançamentos GNU!
* Atualização da GNU Toolchain: Apoie a GNU Toolchain
* Agenda de palestras do Richard Stallman e outros eventos da FSF
* Agradecimentos do GNU!
* Entre em ação com a FSF!

Veja este boletim online aqui: <https://www.fsf.org/free-software-supporter/2018/november>

Incentive seus amigos a se inscrever e nos ajudar a construir um público
adicionando nosso widget de assinante ao seu site.

  * Inscreva-se: <https://www.fsf.org/free-software-supporter>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma edição? Você pode recuperar o atraso em
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2018/noviembre>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2018/novembre>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em Português. Para ver a
versão em Português, clique aqui:
<https://www.fsf.org/free-software-supporter/2018/novembro>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em Português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

#

### Oportunidade de trabalho na FSF: Gerente de Programa

*10 de outubro*

A FSF busca um indivíduo motivado e talentoso, morador de Boston,
para ser nosso gerente de programa em tempo integral. Reportando
ao diretor executivo, o gerente do programa co-lidera nossa equipe
de campanhas. Este cargo desenvolve e promove recursos de longo
prazo e programas de defesa de direitos relacionados ao aumento do
uso de software livre, expandindo e avançando o movimento do
software livre. O gerente do programa tem um papel chave nas
comunicações externas, captação de recursos, e engajamento em
eventos especiais.

Os candidatos ideais têm pelo menos três a cinco anos de experiência
profissional em gerenciamento de projetos, captação de recursos,
gerenciamento de eventos  e gerenciamento de programas sem fins
lucrativos. Proficiência, experiência e conforto com a escrita
profissional e os relacionamentos com a mídia são preferidos. Porque
a FSF trabalha globalmente e procura ter nossos materiais distribuídos
em tantas línguas quanto possível, os candidatos multilíngues terão
uma vantagem. Para mais informações sobre como se inscrever, veja o
link abaixo.

 * <https://www.fsf.org/news/fsf-job-opportunity-program-manager>

### Anunciando palestrantes para o LibrePlanet - e não perca sua chance de dar uma palestra

*18 de outubro*

Estamos orgulhosos de anunciar os principais palestrantes do
LibrePlanet 2019: o colaborador do projeto Debian Bdale Garbee,
o ativista de software livre Micky Metts, o médico Tarek Loubani
e o fundador e presidente da FSF, Richard Stallman, todos eles
pioneiros do software livre por direito próprio.

 * <https://www.fsf.org/blogs/community/announcing-keynote-speakers-for-libreplanet-and-dont-miss-your-chance-to-give-a-talk>
 * <https://www.fsf.org/news/keynotes-announced-for-libreplanet-2019-free-software-conference-1>

### Declaração da FSF sobre a adesão da Microsoft à Open Invention Network

*10 de outubro*

Os comunicados da Microsoft, nos dias 4 e 10 de outubro, de que ela
se juntou tanto à LOT quanto à Open Invention Network (OIN), são
passos significativos na direção certa, potencialmente fornecendo
alívio da bem conhecida extorsão de bilhões de dólares que a Microsoft
impõe sobre redistribuidores de software livre.

Esses passos, no entanto, não resolvem totalmente o problema das
patentes sobre ideias computacionais, ou mesmo as reivindicações de
violações específicas da Microsoft. Com essas limitações em mente,
a FSF dá as boas vindas aos comunicados e pede à Microsoft que tome
três passos adicionais para continuar o impulso para uma resolução
completa.

 * <https://www.fsf.org/news/fsf-statement-on-microsoft-joining-the-open-invention-network>

### Apresentando nosso novo fórum de associados!

*15 de outubro*

Esperamos que você considere este fórum um ótimo lugar para
compartilhar suas experiências e perspectivas em torno do software
livre e criar novos laços com a comunidade de software livre.
Se você é um membro da FSF, vá até <https://forum.members.fsf.org>
para começar. Você poderá fazer login usando a conta do Serviço de
Autenticação Central (CAS) usada para criar sua associação.

A participação neste fórum é apenas um dos muitos
[benefícios](https://www.fsf.org/associate/benefits) de ser um membro
da FSF -- se você ainda não é um membro, nós encorajamos você a
[participar hoje](https://my.fsf.org/join), por apenas US$10 por mês,
ou US$5 por mês para estudantes.

 * <https://www.fsf.org/blogs/membership/introducing-our-new-associate-member-forum>

### Isenções de DMCA concedidas, mas o processo geral ainda está quebrado

*26 de outubro, por Kit Walsh e Mitch Stolz*

A Biblioteca do Congresso dos EUA, através de sua subsidiária, o
Escritório de _Copyright_, concedeu várias novas isenções ao 
Artigo 1201 da Lei de Direitos Autorais do Milênio (DMCA, na sigla em
inglês). A Seção 1201 torna ilegal contornar o código de computador 
que impede a cópia ou modificação na maioria dos produtos controlados
por software - que hoje inclui desde  geladeiras e babás eletrônicas,
até tratores e caixas de som inteligentes. A EFF e a FSF travam há
anos a luta contra o abuso anticonsumidor da DMCA, uma vez que seu
estatuto dá a fabricantes, empresas de tecnologia e estúdios de
Hollywood um controle descomunal sobre os produtos que possuímos.

 * <https://www.eff.org/press/releases/eff-wins-dmca-exemption-petitions-tinkering-echos-and-repairing-appliances-new>
 * <https://www.defectivebydesign.org/blog/help_fight_against_dmca_anticircumvention_rules_december_15th>

### Os novos bloqueios de software proprietário da Apple eliminarão os reparos independentes em novos MacBook Pros

*4 outubro, por Jason Koebler*

A iFixit descobriu que a Apple tem um "kill switch" no novo MacBook
Pro. Embora essa chaves ("switches") ainda não estejam operacionais,
quando funcionarem, elas efetivamente impedirão reparos independentes
e de terceiros nos computadores MacBook Pro 2018, de acordo com
documentos internos da Apple obtidos pela *Motherboard*. Este é apenas
um dos muitos notórios [ataques à liberdade do
usuário](https://www.fsf.org/blogs/community/apple-app-store-anniversary-marks-ten-years-of-proprietary-appsploitation)
cometidos pela Apple.

 * <https://motherboard.vice.com/en_us/article/yw9qk7/macbook-pro-software-locks-prevent-independent-repair>

### A cada minuto, durante três meses, a GM coletou secretamente dados sobre os hábitos e locais de escuta de rádio de 90.000 motoristas.

*23 de outubro, por Cory Doctorow*

A GM rastreou as escolhas de programas de rádio em seus carros
"conectados", minuto a minuto. A GM não obteve o consentimento dos
usuários, mas poderia ter facilmente obtido-o, mencionando isso no
contrato que os usuários assinam por algum serviço digital ou outro.
Um requisito para consentimento é efetivamente zero proteção.

Os carros também podem coletar muitos outros dados: escutando você,
te observando, acompanhando seus movimentos, rastreando o celular
dos passageiros. *Todos* esses tipos de coleta de dados deve ser
proibida. Mas se você realmente quiser estar seguro, devemos ter
certeza de que  o hardware do carro não pode coletar nenhum desses
dados.

 * <https://boingboing.net/2018/10/23/dont-touch-that-dial.html>

### Obrigado por participar no Dia Internacional Contra o DRM 2018!

*4 de outubro*

Centenas de vocês ao redor do mundo agiram no Dia Internacional
Contra o DRM (IDAD): saindo em seus campi, comunidades e em toda
a Web e compartilhando sua oposição a como o Digital Restrictions
Management (DRM) restringe sua liberdade como usuário de software
e mídia. As 17 organizações participantes tomaram suas próprias
ações, criando vídeos, publicando relatórios e escrevendo artigos.
Aqui em Boston, visitamos a Apple Store e conversamos com compradores
sobre seus direitos digitais, e como os dispositivos da Apple abusam
destes direitos usando DRM.

 * <https://www.defectivebydesign.org/blog/thank_you_participating_international_day_against_drm_2018>

### Biografia inovadora do RMS retorna ao GNU Press Shop

*31 de outubro*

Após um período de indisponibilidade, a GNU Press Shop está orgulhosa
de oferecer uma vez mais *Free As In Freedom (2.0)*, a inovadora
biografia do fundador e presidente da Free Software Foundation,
Richard M. Stallman. A biografia original foi escrita por Sam
Williams em 2002, mas esta versão é revisada pelo próprio Stallman
-- enquanto preserva o ponto de vista de Williams, inclui correções
factuais e um extenso novo comentário escrito por Stallman, bem como
novos prefácios por ambos os autores, escritos para a ocasião.

 * <https://www.fsf.org/blogs/gnu-press/innovative-biography-of-rms-returns-to-gnu-press-shop>

### Guia de computador de placa única atualizado: o software livre está ganhando no ARM!

*18 de outubro por Alyssa Rosenzweig*

A Free Software Foundation mantém [uma lista de famílias de
sistemas-em-chip](https://www.fsf.org/resources/hw/single-board-computers),
classificadas por seu status de liberdade. O software livre está
melhorando constantemente, e hoje, mais e mais placas são utilizáveis
com software livre. A lista revisada inclui muito mais detalhes
do que seus predecessores, grupos de placas por sistema-em-chip,
em vez de marca para concisão, documenta falhas de liberdade não
identificadas anteriormente e, é claro, descreve o progresso na
liberação dos elementos restantes.

 * <https://www.fsf.org/blogs/sysadmin/single-board-computer-guide-updated-free-software-is-winning-on-arm>
 * <https://www.fsf.org/resources/hw/single-board-computers>

### A conclusão do trabalho de estágio Outreachy da Sonali no Diretório de Software Livre

*12 de outubro por Sonali Singhal*

Após muito trabalho, finalmente completei a atualização do [Diretório
de Software Livre](https://directory.fsf.org/wiki/Main_Page) da
versão de suporte anterior de longo prazo do MediaWiki, 1.27, para a
versão atual, 1.31, que foi lançado logo após o meu estágio iniciar.
Eu também fiz algumas melhorias gerais, incluindo o download das
extensões Semantic MediaWiki usando o composer; removendo código
obsoleto em `LocalSettings.php`; e mais.

 * <https://www.fsf.org/blogs/sysadmin/the-completion-of-sonalis-outreachy-internship-work-on-the-free-software-directory>

### GNU Compiler Collection (GCC) 6.5 lançado 

*26 de outubro pela Equipe do GCC*

O projeto GNU e os desenvolvedores do GCC têm o prazer de anunciar o
lançamento do GCC 6.5. Esta versão é uma versão de correção de bugs,
contendo correções para regressões no GCC 6.4 em relação às versões
anteriores do GCC.

 * <https://gcc.gnu.org/gcc-6/>

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam o directory.fsf.org todos os
meses para descobrir software livre. Cada registro no Diretório contém
uma grande quantidade de informações úteis, desde categorias básicas e
descrições até controle de versão, canais de IRC, documentação e
licenciamento. O Diretório de Software Livre tem sido um ótimo recurso
para usuários de software na última década, mas precisa de sua ajuda
para manter-se atualizado com novos e interessantes projetos de
software livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org
e geralmente incluem um punhado de regulares, bem como recém-chegados.
O Freenode é acessível a partir de qualquer cliente de IRC – Todos são
bem-vindos!

A próxima reunião é sexta-feira, 2 de novembro, das 12h às 15h (16h à
19h UTC). Detalhes aqui:

  * <https://directory.fsf.org/wiki/Free_Software_Directory:Meetings>

### Recurso em destaque do LibrePlanet: Community Toolkit

Todos os meses no LibrePlanet, destacamos um recurso que é útil e
interessante – geralmente um recurso que poderia receber sua ajuda.

Para este mês, estamos destacando o Community Toolkit, que fornece
informações sobre software que podem ser usados por entidades sem
fins lucrativos, grupos da comunidade e indivíduos para avançar a
causa do software livre. Você está convidado a adotar, divulgar e
melhorar este importante recurso.

  * <https://libreplanet.org/wiki/Community_toolkit>

Você tem uma sugestão para o recurso em destaque do próximo mês?
Nos avise em <campaigns@fsf.org>.

### Holofote GNU com Mike Gerwitz: 8 novos lançamentos GNU!
(em 25 de outubro de 2018):

* [gama-2.01](https://www.gnu.org/software/gama/)
* [gcc-6.5.0](https://www.gnu.org/software/gcc/)
* [gvpe-3.1](http://software.schmorp.de/pkg/gvpe.html)
* [help2man-1.47.8](https://www.gnu.org/software/help2man/)
* [mes-0.18](https://www.gnu.org/software/mes/)
* [mtools-4.0.19](https://www.gnu.org/software/mtools/)
* [parallel-20181022](https://www.gnu.org/software/parallel/)
* [units-2.18](https://www.gnu.org/software/units/)

Para anúncios da maioria dos novos lançamentos GNU, inscreva-se
na lista de discussão info-gnu:
<https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para baixar: quase todo o software GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou, de preferência, em um dos espelhos em
<https://www.gnu.org/prep/ftp.html>. Você pode usar o URL
<https://ftpmirror.gnu.org/> para ser redirecionado automaticamente
para um espelho (por exemplo) próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como um
todo, estão procurando mantenedores e outras formas de auxílio: veja
<https://www.gnu.org/server/takeaction.html#unmaint> se você quiser
ajudar. A página geral sobre como ajudar o GNU está em
<https://www.gnu.org/help/help.html>.

Se você tem um programa funcionando, totalmente ou parcialmente, que
gostaria de oferecer ao projeto GNU como um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, por favor sinta-se à vontade para nos escrever em
<maintainers@gnu.org> com quaisquer perguntas ou sugestões do
GNUish para futuras parcelas.

### Atualização da GNU Toolchain: Apoie a GNU Toolchain

Doe para apoiar GNU Toolchain, uma coleção de ferramentas de
desenvolvimento fundacional de software licenciadas livremente,
incluindo [GNU C Compiler collection (GCC)](https://gcc.gnu.org/),
[GNU C Library (glibc)](https://www.gnu.org/software/libc/libc.html)
e [GNU Debugger (GDB)](https://sourceware.org/gdb/).

* <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57>

### Agenda de palestras do Richard Stallman

Para detalhes do evento, bem como para se inscrever para ser
notificado para futuros eventos em sua área, por favor visite
<https://www.fsf.org/events>.

Até agora, Richard Stallman tem os seguintes eventos neste mês:

 * 10 de novembro de 2018, Burgos, Espanha, ["Libertad de usuario, libertad del lector"](https://www.fsf.org/events/rms-20181110-burgos)
 * 12 de novembro de 2018, Madrid, Espanha, ["¿El software que usas deniega tu libertad?"](https://www.fsf.org/events/rms-20181112-madrid)
 * 23 de novembro de 2018, Madrid, Espanha, ["Soberania tecnológica, privacidad y democracia"](https://www.fsf.org/events/rms-20181123-madrid)

###  Outros eventos da FSF e de software livre

 * 3 de novembro de 2018, Bristol, UK, [John Sullivan, "How can free communication tools win?"](https://www.fsf.org/events/john-sullivan-how-can-free-communication-tools-win-freenode-live-bristol-uk)
 * 9 de novembro de 2018, Seattle, WA, [SeaGL 2018](https://www.fsf.org/events/conference-20181109-seagl-seattle)
 * 9 de novembro de 2018, Seattle, WA, [Molly de Blanc, "Insecure Connections: Love and mental health in our digital lives"](https://www.fsf.org/events/insecure-connections-love-and-mental-health-in-our-digital-lives-seagl-seattle-wa) 
 * 9 de novembro de 2018, Seattle, WA, [Rubén Rodríguez, "Freedom and privacy in the Web: Fighting licensing, tracking, fingerprinting and other issues, from both sides of the cable"](https://www.fsf.org/events/ruben-rodriguez-20181109-seattle-seagl)
 * 23 de novembro de 2018, Las Palmas de Gran Canaria, Espanha, [GNU Health Con 2018](https://www.fsf.org/events/event-20181123-laspalmas-gnuhealthcon)

### Agradecimentos do GNU!

Agradecemos a todos que doam para a Free Software Foundation e
gostaríamos de dar reconhecimento especial às pessoas que doaram
US$500 ou mais no mês passado.

  * <https://www.gnu.org/thankgnus/2018supporters.html>

Este mês, um grande Agradecimento do GNU para:

* Christopher Padovano
* Christopher Samuel
* Colin Carr
* John Trudeau
* René Genz
* Rich Haase
* Tom Gedra
* Trevor Spiteri

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Entre em ação com a FSF!

Contribuições de milhares de membros individuais possibilitam o trabalho
da FSF. Você pode contribuir unindo-se em <https://my.fsf.org/join>.
Se você já é um membro, você pode ajudar a indicar novos membros
(e ganhar algumas recompensas) adicionando uma linha com seu número
de membro à sua assinatura de e-mail como:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

A FSF está sempre procurando voluntários
(<https://www.fsf.org/volunteer>). Da agitação à hacking, da
coordenação de problemas ao preenchimento de envelopes -- há algo
aqui para todo mundo fazer. Além disso, dirija-se à nossa seção de
campanhas (<https://www.fsf.org/campaigns>) e entre em ação contra
patentes de software, Gestão Digital de Restrições (DRM), adoção de
software livre, OpenDocument, Associação da Indústria de Gravação
da América (RIAA), e mais.

###

Copyright © 2018 Free Software Foundation, Inc.

Esta obra está licenciada sob uma licença Creative Commons
Atribuição 4.0 Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>.

