# Suporte ao Software Livre
Edição 126, outubro de 2018

Bem-vindo(a) ao Suporte ao Software Livre, um resumo de notícias e atualização
mensal da Free Software Foundation (FSF) -- sendo lido por você e mais 193.513
outros ativistas. São 660 a mais em relação ao mês passado!

### Envie suas palestras para a conferência LibrePlanet 2019, anunciada para 23 e 24 de março de 2019

*5 de setembro*

A FSF anunciou as datas da 11º conferência anual de software livre LibrePlanet,
a ser realizada de 23 a 24 de março de 2019, em Boston.
O [convite à apresentação de propostas](https://my.fsf.org/node/20/)
está aberto de agora até 26 de outubro de 2018.
[Inscrições gerais](https://my.fsf.org/civicrm/event/info?id=79&reset=1) e
[inscrição de expositor e patrocinador](https://my.fsf.org/civicrm/event/info?id=80&reset=1)
estão também abertas.

LibrePlanet é uma conferência anual para usuários de software livre e todos
que se importam com a interseção entre a tecnologia e a justiça social. Por
uma década, a LibrePlanet reuniu milhares de vozes e bases de conhecimento
diversos, incluindo desenvolvedores de software livre,  especialistas em
políticas, ativistas, _hackers_, estudantes e pessoas que recém começaram a
aprender sobre software livre.

LibrePlanet é definida por sua combinação de palestras técnicas com sessões
não técnicas sobre ativismo, cultura e eventos atuais. Estamos especialmente
interessados em ver propostas de pessoas que usam software livre ou aplicam
seus valores para benefício social, desde a pesquisa acadêmica até em
organizações comunitárias, educação, medicina e nas artes.

 * <https://www.fsf.org/news/eleventh-annual-libreplanet-conference-set-for-march-23-24-2019>
 * <https://my.fsf.org/node/20/>

## ÍNDICE

* FSF levou a cabo o dia internacional de ação por um Dia sem DRM, em 18 de setembro
* A culpa sempre é do DRM
* Escalando nossa maneira de derrotar DRM
* Algemas digitais: como o DRM tira o poder dos consumidores
* O DRM ainda está aqui, e ainda é uma porcaria
* Novos poderes de direitos autorais, novos regulamentos para "conteúdo terrorista": um dia sombrio para os direitos digitais na Europa
* Destacando alguns de nossos líderes no kernel Linux
* A liberdade do software garante o verdadeiro software comum
* W3C vende a Web com o EME: 1 ano depois
* Como a Tutanota substituiu o Firebase Cloud Messaging (FCM) do Google por seu próprio sistema de notificação
* O Departamento de Patentes e Marcas dos EUA rejeita inicialmente oferta do Google para codificar o ANS
* Mensagens secretas para Alexa
* Principal companhia de seguros dos EUA para vender apenas seguro de vida apoiado pelo rastreador de saúde
* O que acontece quando o Facebook bloqueia, por engano, notícias locais?
* O trabalho de estágio de Sonali no Diretório de Software Livre, parte 2
* O trabalho de estágio de David no Diretório de Software Livre, parte 2
* O calendário do SeaGL 2018 já está disponível!
* GNOME 3.30 lançado
* Oportunidade de trabalho da FSF: gerente de operações de negócios
* Junte-se à FSF e amigos na atualização do Diretório de Software Livre
* Recurso em destaque no LibrePlanet: Defective by Design/Ideas/Guide
* Holofote GNU com Mike Gerwitz: 15 novos lançamentos GNU!
* Atualização da GNU Toolchain: Apoie a GNU Toolchain
* Agenda de palestras do Richard Stallman e outros eventos da FSF
* Agradecimentos do GNU!
* Entre em ação com o FSF!

Veja esse boletim online em: <https://www.fsf.org/free-software-supporter/2018/october>

Incentive seus amigos a se inscrever e nos ajudar a construir um público
adicionando nosso widget de assinante ao seu site.

  * Inscreva-se: <https://www.fsf.org/free-software-supporter>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma edição? Você pode recuperar o atraso em
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2018/octubre>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2018/octobre>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em Português, clique aqui:
<https://www.fsf.org/free-software-supporter/2018/outubro>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em Português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### FSF levou a cabo o dia internacional de ação por um Dia sem DRM, em 18 de setembro

*18 de setembro*

A campanha Defective by Design (Defeituoso por construção, DbD na sigla
em inglês) da FSF realizou o 12º Dia Internacional Contra o DRM 
(IDAD, na sigla em inglês) na terça-feira, 18 de setembro de
2018. IDAD é um dia para tomarmos medidas contra o Digital Restrictions
Management (Gestão Digital de Restrições, DRM, na sigla em inglês),
enquanto imaginamos como o mundo poderia ser sem DRM.

Por 12 anos, nós comemoramos o IDAD - construindo, organizando, protestando,
e tomando medidas para apoiar a demolição do DRM - e 2018 não foi 
diferente. Este ano continuamos a luta contra o DRM e comemoramos
o trabalho de ativistas, artistas e tecnólogos que criam
mídia e tecnologia sem DRM.

Alguns dos excelentes artigos publicados pelas organizações participantes
no IDAD, seguem abaixo.

 * <https://www.fsf.org/blogs/community/thank-you-for-participating-in-international-day-against-drm-2018>
 * <https://www.defectivebydesign.org/dayagainstdrm/2018>
 * <https://www.fsf.org/news/international-day-against-drm-takes-action-for-a-day-without-drm-on-september-18th>
 * <https://www.fsf.org/blogs/community/boston-supporters-join-us-against-drm>

### A culpa sempre é do DRM

*18 de setembro, por John Bergmayer*

Recentemente, ocorreu uma história viral sobre a Apple "excluir" filmes
comprados da biblioteca de uma pessoa. Como sempre acontece com essas
histórias, há um pouco mais para se falar sobre isso, mas estou aqui
para te dizer que os detalhes realmente não importam. E porque isso está
sendo publicado no Dia Internacional Contra o DRM, estou aqui para lhe
dizer que é culpa do DRM.

 * <https://www.publicknowledge.org/news-blog/blogs/its-always-drms-fault/>

### Escalando nossa maneira de derrotar DRM

*18 de setembro, por Cory Doctorow*

A metáfora do alpinismo não é apenas aplicável à ciência da computação:
é também uma maneira importante de pensar em lutas políticas grandes,
ambiciosas, plenas, como as que lutamos na Eletronic Frontier Foundation (EFF).
Nosso projeto Apollo 1201 visa matar todo o DRM no mundo dentro de uma
década, mas não temos um roteiro elaborado mostrando todas as direções
que tomaremos no caminho.

 * <https://www.eff.org/deeplinks/2018/09/defeating-drm-hill-climbing-our-way-glory>

### Algemas digitais: como o DRM tira o poder dos consumidores

*18 de setembro, por Slavka Bielikova e Javier Ruiz Diaz*

Este relatório examina questões decorrentes das tecnologias DRM e da
legislação que protege estas tecnologias. O relatório analisa como o
uso do DRM pode afetar a segurança, a privacidade e os direitos de acesso
dos usuários, enquanto também explora como o DRM sufoca a inovação e
concorrência. Além disso, o relatório analisa os fenômenos da
obsolescência e bloqueio de fornecedor facilitado pelo DRM.

 * <https://www.openrightsgroup.org/assets/files/pdfs/Digital_Handcuffs.pdf>

### O DRM ainda está aqui, e ainda é uma porcaria

*18 de setembro, por Creative Commons*

Aqui estamos nós, outro Dia Internacional Contra o DRM e, claro, ainda há
inúmeras estórias sobre como o DRM continua a frustrar os usuários, que
devem ser capazes de acessar, aproveitar e redefinir a mídia que eles já
compraram. O DRM consiste em tecnologias ou acordos de licenciamento
restritivos que tentam restringir o uso, modificação e distribuição de
obras adquiridas legalmente.

 * <https://medium.com/@creativecommons/drm-is-still-here-and-it-still-sucks-aad7a8c33b1f>

### Novos poderes de direitos autorais, novos regulamentos para "conteúdo terrorista": um dia sombrio para os direitos digitais na Europa

*12 de setembro, por Danny O'Brien*

Apesar das ondas de chamadas e e-mails dos usuários europeus da Internet, o
Parlamento Europeu votou a favor da aceitação do princípio de um
filtro universal de _copyright_ preventivo, para sites de
compartilhamento de conteúdo, bem como a ideia de que os editores de
notícias devem ter o direito de processar pessoas por citar notícias _online_
- ou até mesmo usando seus títulos como _links_ para artigos. De todas as
potenciais alterações propostas que poderiam corrigir ou melhorar os
danos causados por estas propostas, eles votaram absolutamente
sem considerá-las.

Ainda existem oportunidades, no nível da UE, no nível nacional,
e, finalmente, nos tribunais da Europa, para limitar os danos. Mas não se
engane, este é um sério revés para a Internet e os direitos digitais
na Europa.

 * <https://www.eff.org/deeplinks/2018/09/new-copyright-powers-new-terrorist-content-regulations-grim-day-digital-rights>

### Destacando alguns de nossos líderes no kernel Linux

*20 de setembro, por Karen Sandler*

No início deste mês, seguindo um artigo no *New Yorker*, que
apontou que o comportamento abusivo no projeto Linux havia criado um
ambiente inamistoso e hostil para grupos sub-representados,
Linus Torvalds, líder do projeto Linux, admitiu que seu comportamento passado
foi problemático e anunciou que estaria [se retirando momentaneamente do
projeto](https://www.newyorker.com/science/elements/after-years-of-abusive-e-mails-the-creator-of-linux-steps-aside).
Enquanto isto é um passo a frente, é apenas o começo das mudanças
necessárias para tornar o mundo do software livre mais inclusivo, e assim o
Conservancy está usando esta oportunidade de diálogo para destacar
os talentosos pupilos em seu programa Outreachy, que ajuda as pessoas
de grupos sub-representados a se envolverem no software livre.

 * <https://sfconservancy.org/blog/2018/sep/20/kernelLeaders/>

### A liberdade do software garante o verdadeiro software comum

*22 de agosto, por Bradley M. Kuhn*

Software proprietário sempre representou uma relação de poder.
Direitos autorais e outros sistemas jurídicos dão aos autores o poder
para decidir qual licença escolher e, geralmente, eles escolhem uma licença
que lhes favorece e retira direitos e permissões de outras.

A assim chamada "Cláusula Commons" confunde e mistura muitos problemas.
A iniciativa é apoiada pela FOSSA, uma empresa que vende material no complexo
industrial de conformidade proprietária. Esta cláusula recentemente tornou-se
notícia novamente, desde que outras partes já adotaram este mesma licença.

 * <https://sfconservancy.org/blog/2018/aug/22/commons-clause/>

### W3C vende a Web com o EME: 1 ano depois

*18 de setembro*

Faz um ano desde que o World Wide Web Consortium (W3C) votou
para trazer Extensões de Mídia Criptografada
(Encrypted Media Extensions, EME, na sigla em inglês) aos padrões da Web.
Eles alegaram querer levar a Web ao seu potencial máximo, mas em um
voto secreto, membros do W3C, com a benção do criador da Web Tim
Berners-Lee, concordou em colocar a indústria de direitos autorais no
controle de acesso à mídia. A consagração do EME como uma recomendação
oficial não é como imaginamos todo o potencial da Web, na FSF.

 * <https://www.defectivebydesign.org/blog/w3c_sells_out_web_eme_1_year_later>

### Como a Tutanota substituiu o Firebase Cloud Messaging (FCM) do Google por seu próprio sistema de notificação

*3 de setembro, por Ivan*

Tutanota está agora no F-Droid! Neste _post_ especial, Ivan de Tutanota
nos conta a história de como a Tutanota substituiu o FCM do Google por seu
próprio sistema de notificação.

 * <https://f-droid.org/en/2018/09/03/replacing-gcm-in-tutanota.html>

### O Departamento de Patentes e Marcas dos EUA rejeita inicialmente oferta do Google para codificar o ANS

*9 de setembro, por Jakub Kulas*

O Escritório de Marcas e Patentes dos EUA rejeitou inicialmente um pedido
apresentado pelo Google, que dizia respeito a uma solução dada pelo Ph.D.
Jarosław Duda, funcionário e professor da Universidade Jaguelônica (UJ).
Um no atrás, o estudioso, juntamente com a UJ exigiu a retirada da oferta
por parte do Google, e Duda escreveu sobre seu caso para o nosso [blog Fim
das Patentes de Software](http://endsoftpatents.org/2018/01/jarek-duda-on-software-patents/).

 * <http://wbj.pl/us-patent-office-initially-rejects-googles-application-on-ans-coding/>


### Mensagens secretas para Alexa

*24 de setembro por Julia Weiler*

Uma equipe da Ruhr-Universität Bochum conseguiu integrar comandos
secretos ao sistema de reconhecimento de fala Kaldi – que se acredita
estar contido no Alexa da Amazon e em muitos outros sistemas – em
arquivos de áudio. Estes não são audíveis para o ouvido humano, mas
Kaldi reage a eles. Os pesquisadores mostraram que podiam esconder
qualquer frase que gostassem em diferentes tipos de sinais de áudio,
como fala, gorjeio de pássaros ou música, e que Kaldi os entendia.

 * <http://news.rub.de/english/press-releases/2018-09-24-it-security-secret-messages-alexa-and-co>

### Principal companhia de seguros dos EUA para vender apenas seguro de vida apoiado pelo rastreador de saúde

*20 de setembro por Rob Beschizza*

No início deste mês, a Apple anunciou um smartwatch redesenhado que
poderia rastrear dados cardíacos, executar eletrocardiogramas e até
mesmo detectar fibrilação atrial, prometendo que isso salvaria vidas.
Uma semana depois, uma das maiores seguradoras da América matou suas
apólices tradicionais de seguro de vida, substituindo-as por um
"seguro interativo que incentiva os usuários a usar esses dispositivos
e compartilhar os dados com eles para obter benefícios. Esse é apenas
mais um exemplo de como os produtos da Apple rotineiramente [atropelam
os direitos dos
usuários](https://www.fsf.org/blogs/community/apple-app-store-anniversary-marks-ten-years-of-proprietary-appsploitation),
com o risco adicional de que as seguradoras dados para selecionar
os clientes mais rentáveis e aumentar as taxas para aqueles que não
participam.


 * <https://boingboing.net/2018/09/20/major-insurance-company-to-onl.html>

### O que acontece quando o Facebook bloqueia, por engano, notícias locais?

*30 de agosto por Louise Matsakis*

O Facebook é um concentrador para coleta de notícias para milhões de
usuários. Então, o que acontece quando seus algoritmos e processos
ocultos e misteriosos repentinamente começam a censurar notícias,
intencionalmente ou não?

 * <https://www.wired.com/story/what-happens-when-facebook-mistakenly-blocks-local-news-stories/>

### O trabalho de estágio de Sonali no Diretório de Software Livre, parte 2

*21 de setembro por Sonali Singhal*

Eu tornei a seção de licenças do Diretório compatível com dispositivos
móveis e iniciei a tarefa de atualizar o Diretório de Software Livre da
MediaWiki versão 1.27 para a versão 1.31. Nas próximas semanas, planejo
modificar o módulo Vector ainda mais para obter uma organização correta
do conteúdo no wiki e adicionar extensões semânticas do MediaWiki à
nova instalação.

 * <https://www.fsf.org/blogs/sysadmin/sonalis-internship-work-on-the-free-software-directory-part-2-1>

### O trabalho de estágio de David no Diretório de Software Livre, parte 2

*21 de setembro por David Hedlund*

Durante este período, trabalhei em várias questões relacionadas ao
GNU IceCat. A nova API do IceCat para extensões, chamada WebExtensions,
significa que muitas extensões herdadas não funcionarão mais em um
navegador compatível e serão apenas para referência histórica.

 * <https://www.fsf.org/blogs/sysadmin/internship-work-on-the-free-software-directory-part-2>

### O calendário do SeaGL 2018 já está disponível!

A Conferência Seattle GNU/Linux 2018 está programada para os
dias 9 e 10 de novembro no Seattle Central College, com quatro
palestras incríveis, cinquenta e seis palestras espetaculares e
uma programação completa disponível on-line. Vá dar uma olhada!

 * <https://seagl.org/news/2018/09/21/2018_Schedule_Now_Available.html>

### GNOME 3.30 lançado

*5 de setembro por Projeto GNOME*

Esta versão apresenta algumas melhorias significativas no desempenho.
A área de trabalho inteira agora usa menos recursos do sistema, o que
significa que você pode executar mais aplicativos de uma vez sem encontrar
problemas de desempenho. Outros destaques incluem um novo modo de leitura
no aplicativo Web, aprimoramentos de pesquisa no aplicativo Arquivos e
melhorias na gravação de tela e no compartilhamento de tela. O aplicativo
Configurações agora possui um painel de Thunderbolt para gerenciar
dispositivos e mostra dinamicamente painéis relacionados a hardware
somente quando um hardware relevante é detectado.

 * <https://www.gnome.org/news/2018/09/gnome-3-30-released/>

### Oportunidade de trabalho da FSF: gerente de operações de negócios

*9 de Agosto*

A FSF procura um indivíduo motivado e talentoso domiciliado em Boston para
ser nosso Gerente de Operações de Negócios em tempo integral. Estamos à
procura de um profissional hands-on e detalhista que esteja confortável
trabalhando de forma independente e com várias equipes, incluindo alguns
colegas de trabalho remotos. Os candidatos ideais serão pró-ativos e
altamente adaptáveis, com aptidão para aprender novas ferramentas e
propor soluções criativas. Os candidatos devem ter pelo menos três anos
de experiência em contabilidade e operações sem fins lucrativos; os
recursos humanos experimentam um plus. Clique no link abaixo para saber
como se inscrever!

 * <https://www.fsf.org/news/fsf-job-opportunity-business-operations-manager>

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam o directory.fsf.org todos os
meses para descobrir software livre. Cada registro no Diretório contém
uma grande quantidade de informações úteis, desde categorias básicas e
descrições até controle de versão, canais de IRC, documentação e
licenciamento. O Diretório de Software Livre tem sido um ótimo recurso
para usuários de software na última década, mas precisa de sua ajuda
para manter-se atualizado com novos e interessantes projetos de
software livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org
e geralmente incluem um punhado de regulares, bem como recém-chegados.
O Freenode é acessível a partir de qualquer cliente de IRC – Todos são
bem-vindos!

A próxima reunião é sexta-feira, 5 de outubro, das 12h às 15h (16h à
19h UTC). Detalhes aqui:

  * <https://directory.fsf.org/wiki/Free_Software_Directory:Meetings>

### Recurso em destaque no LibrePlanet: Defective by Design/Ideas/Guide

Todos os meses no LibrePlanet, destacamos um recurso que é interessante
e útil – geralmente um recurso que poderia usar sua ajuda.

Para este mês, estamos destacando Defective by Design/Ideas/Guide, que
fornece um local para você sugerir itens para adicionar ao guia de vida
livre de DRM da Defective by Design. Você está convidado a adotar,
divulgar e melhorar este importante recurso.

  * <https://libreplanet.org/wiki/Group:Defective_by_Design/Ideas/Guide>

Você tem uma sugestão para o recurso em destaque do próximo mês?
Nos avise em <campaigns@fsf.org>.

### Holofote GNU com Mike Gerwitz: 15 novos lançamentos GNU!

15 novas versões do GNU no último mês (em 26 de setembro de 2018):

* [autogen-5.18.16](https://www.gnu.org/software/autogen/)
* [bison-3.1](https://www.gnu.org/software/bison/)
* [dico-2.7](https://www.gnu.org/software/dico/)
* [gdb-8.2](https://www.gnu.org/software/gdb/)
* [gnupg-2.2.10](https://www.gnu.org/software/gnupg/)
* [gnu-pw-mgr-2.4.2](https://www.gnu.org/software/gnu-pw-mgr/)
* [gnutls-3.6.4](https://www.gnu.org/software/gnutls/)
* [guile-cv-0.2.0](https://www.gnu.org/software/guile-cv/)
* [help2man-1.47.7](https://www.gnu.org/software/help2man/)
* [indent-2.2.12](https://www.gnu.org/software/indent/)
* [librejs-7.17.0](https://www.gnu.org/software/librejs/)
* [mes-0.17.1](https://www.gnu.org/software/mes/)
* [nano-3.1](https://www.gnu.org/software/nano/)
* [parallel-20180922](https://www.gnu.org/software/parallel/)
* [xorriso-1.5.0](https://www.gnu.org/software/xorriso/)

Para anúncios da maioria dos novos lançamentos GNU, inscreva-se na lista
de discussão info-gnu: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para fazer o download: quase todo o software GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou, de preferência, em um de seus espelhos em
<https://www.gnu.org/prep/ftp.html>. Você pode usar o URL
<https://ftpmirror.gnu.org/> para ser redirecionado automaticamente para
um espelho (por exemplo) próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como um todo,
estão procurando mantenedores e outras formas de assistência: veja
<https://www.gnu.org/server/takeaction.html#unmaint> se você quiser
ajudar. A página geral sobre como ajudar o GNU está em
<https://www.gnu.org/help/help.html>.

Se você tem um programa de trabalho ou parcialmente funcionando que
gostaria de oferecer ao projeto GNU como um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, por favor sinta-se à vontade para nos escrever em
<maintainers@gnu.org> com quaisquer perguntas ou sugestões do
GNUish para futuras parcelas.

### Atualização da GNU Toolchain: Apoie a GNU Toolchain

Doe para apoiar o GNU Toolchain, uma coleção de ferramentas de
desenvolvimento de software fundadas livremente licenciadas,
incluindo o [GNU C Compiler collection (GCC)](https://gcc.gnu.org/),
a [GNU C Library (glibc)](https://www.gnu.org/software/libc/libc.html)
e o [GNU Debugger (GDB)](https://sourceware.org/gdb/).

* <https://my.fsf.org/civicrm/contribute/transact?reset=1&id=57>

### Agenda de palestras do Richard Stallman

Para detalhes do evento, bem como para se inscrever para ser
notificado para futuros eventos em sua área, por favor visite
<https://www.fsf.org/events>.

Até agora, Richard Stallman tem o seguinte evento no próximo mês:

  * 10 de novembro de 2018, Burgos, Espanha, ["Libertad de usuario, libertad del lector"](https://www.fsf.org/events/rms-20181110-burgos)

### Outros eventos da FSF e de software livre

 * 3-4 de novembro de 2018, Bristol, UK, [John Sullivan, "How can free communication tools win?"](https://www.fsf.org/events/john-sullivan-how-can-free-communication-tools-win-freenode-live-bristol-uk)
 * 9 de novembro de 2018, Seattle, WA, [SeaGL 2018](https://www.fsf.org/events/conference-20181109-seagl-seattle)
 * 9 de novembro de 2018, Seattle, WA, [Molly de Blanc, "Insecure Connections: Love and mental health in our digital lives"](https://www.fsf.org/events/insecure-connections-love-and-mental-health-in-our-digital-lives-seagl-seattle-wa) 
 * 9 de novembro de 2018, Seattle, WA, [Rubén Rodríguez, "Freedom and privacy in the Web: Fighting licensing, tracking, fingerprinting and other issues, from both sides of the cable"](https://www.fsf.org/events/ruben-rodriguez-20181109-seattle-seagl)
 * 23 de novembro de 2018, Las Palmas de Gran Canaria, Espanha, [GNU Health Con 2018](https://www.fsf.org/events/event-20181123-laspalmas-gnuhealthcon)

### Agradecimentos do GNU!

Agradecemos a todos que doam para a Free Software Foundation
e gostaríamos de dar reconhecimento especial às pessoas que
doaram US $500 ou mais no mês passado.

  * <https://www.gnu.org/thankgnus/2018supporters.html>

Este mês, um grande Agradecimento do GNU para:

* Adam Ymeren
* Eric Brown
* John Poduska
* Kevin Fleming
* Peter Rock 
* Siva Dharmalingam

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Entre em ação com o FSF!

Contribuições de milhares de membros individuais possibilitam o trabalho
da FSF. Você pode contribuir unindo-se em <https://my.fsf.org/join>.
Se você já é um membro, você pode ajudar a indicar novos membros
(e ganhar algumas recompensas) adicionando uma linha com seu número de
membro à sua assinatura de e-mail como:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

A FSF está sempre procurando voluntários (<https://www.fsf.org/volunteer>).
Da agitação à hacking, da coordenação de problemas ao preenchimento de
envelopes - há algo aqui para todo mundo fazer. Além disso, dirija-se à
nossa seção de campanhas (<https://www.fsf.org/campaigns>) e atue sobre
patentes de software, Gestão Digital de Restrições (DRM), adoção de
software livre, OpenDocument, Associação da Indústria de Gravação da
América (RIAA), e mais.

###

Copyright © 2018 Free Software Foundation, Inc.

Esta obra está licenciada sob uma licença Creative Commons Atribuição 4.0
Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/>.
