# Free Software Supporter
Issue 136, August 2019

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 210,274 other activists. That's 1,831 more than last month!

## TABLE OF CONTENTS

* Thank you for advancing free software: Read FSF spring news in the latest *Bulletin* 
* Strengthen free software by telling the US Congress to reject the STRONGER Patents Act 
* Microsoft's ebook apocalypse shows the dark side of DRM 
* Doctorow's novella "Unauthorized Bread" explains why we have to fight DRM today to avoid a grim future
* Fall internships at the FSF! Apply by September 2 
* June 2019: Photos from RMS talks in Brno 
* GNU Linux-libre 5.2 kernel released for those seeking 100% freedom for their PCs
* gNewSense needs a new maintainer
* Job listing: GNU/Linux system administrator for Aleph Objects 
* Net Neutrality Defense Guide: Summer 2019 edition 
* Adblocking: How about nah?
* Discovering whether your iPhone has been hijacked is nearly impossible thanks to Apple's walled garden
* Gnuastro 0.8 released, goal defined for 1.0
* FaceApp makes today's privacy laws look antiquated
* I found your data. It's for sale.
* Pearson shifts to Netflix-style subscription model for textbooks 
* EmacsConf 2019 opens call for proposals
* Call for GUADEC 2020 bid proposals
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Free Software Foundation/Ideas
* GNU Spotlight with Mike Gerwitz: 16 new GNU releases!
* Upcoming FSF and free software events
* Thank GNUs!
* GNU copyright contributions
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2019/august>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2019/agosto-sp>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2019/aout>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2019/agosto-pr>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### 

###

### Thank you for advancing free software: Read FSF spring news in the latest *Bulletin* 

*From July 8*

In the beginning of June, we sent a physical copy of our biannual
spring [*Free Software Foundation (FSF)
Bulletin*](https://www.fsf.org/bulletin/2019/spring/issue-34-spring-2019)
to 11,715 of our most active free software supporters around the
globe. Our *Bulletin* is written by FSF staff and free software
activists, and it is now also available online.

 * <https://www.fsf.org/blogs/community/thank-you-for-advancing-free-software-read-fsf-spring-news-in-the-latest-bulletin>

### Strengthen free software by telling the US Congress to reject the STRONGER Patents Act 

*From July 25*

Despite its failure to pass in 2017, a bill with the appropriately
Orwellian title of "Support Technology and Research of Our Nation's
Growth and Economic Resilience" (STRONGER) Patents Act was
reintroduced into the US Congress on July 10th, 2019. If passed, the
Act would make software idea patents much more easily claimed and
enforceable against developers in the free software
community. Whatever its effects on other types of patents may be, the
fact that it will prop up software idea patents is reason enough to
reject it.

 * <https://www.fsf.org/blogs/community/strengthen-free-software-by-telling-congress-to-reject-the-stronger-patents-act>

### Microsoft's ebook apocalypse shows the dark side of DRM 

*From June 30 by Brian Barrett*

Of course, there isn't really a "light" side to [Digital Restrictions
Management (DRM)](https://www.defectivebydesign.org/) -- but this
article in *Wired* gave FSF executive director John Sullivan a chance
to talk about the dangers of DRM on a wider stage: because of DRM, you
don't really own the ebooks you buy, which enabled Microsoft to
effectively burn millions of books when they shuttered their ebook
store this summer.

 * <https://www.wired.com/story/microsoft-ebook-apocalypse-drm/>

### Doctorow's novella "Unauthorized Bread" explains why we have to fight DRM today to avoid a grim future

*From July 18*

Salima has a problem: her Boulangism toaster is locked down with
software that ensures that it will only toast bread sold to her by the
Boulangism company… and as Boulangism has gone out of business,
there's no way to buy authorized bread. Thus, Salima can no longer
have toast.

This sneakily familiar scenario sends our resourceful heroine down a
rabbit hole into the world of hacking appliances, but it also puts her
in danger of losing her home -- and prosecution under the draconian
terms of the Digital Millennium Copyright Act (DMCA). Her story, told
in the novella “Unauthorized Bread,” which opens Cory Doctorow’s
recent book *Radicalized*, guides readers through a process of
discovering what [Digital Restrictions Management
(DRM)](https://www.defectivebydesign.org/) is, and how the future can
look mightily grim if we don’t join forces to stop DRM now.

 * <https://www.defectivebydesign.org/blog/doctorows_novella_unauthorized_bread_explains_why_we_have_fight_drm_today_avoid_grim_future>

### Fall internships at the FSF! Apply by September 2 

*From July 25*

The Free Software Foundation (FSF) is looking for interns to
contribute to our campaigns, licensing, or technical team. These
positions are unpaid, educational opportunities, and the FSF will
provide any appropriate documentation you might need to receive
funding and school credit from outside sources. We place an emphasis
on providing hands-on educational opportunities for interns, in which
they work closely with staff mentors on projects that match their
skills and interests. The deadline to apply is September 2. See the
link below to learn how to apply.

 * <https://www.fsf.org/blogs/community/fall-internships-at-the-fsf-apply-by-september-2>

### June 2019: Photos from RMS talks in Brno 

*From July 5*

Free Software Foundation president Richard Stallman (RMS) was in Brno,
Czech Republic on June 6, 2019, to give two speeches. In the morning,
he took part in the URBIS Smart City Fair, at the Brno Fair Grounds,
giving his speech "Computing, freedom, and privacy." In the afternoon,
at the Masaryk University's University Cinema Scala, he gave his
speech "The free software movement and the GNU/Linux operating
system," to about three hundred people.

 * <https://www.fsf.org/blogs/rms/photo-blog-2019-june-brno>

### GNU Linux-libre 5.2 kernel released for those seeking 100% freedom for their PCs

*From July 9 by Marius Nestor*

The GNU Linux-libre project has released the GNU Linux-libre 5.2
kernel, a 100% free version of the Linux kernel that doesn't include
any proprietary drivers, firmware, or code.

Based on the recently released Linux 5.2 kernel series, which
introduces the Sound Open Firmware support for DSP audio devices, the
GNU Linux-libre 5.2 kernel also ships with the free firmware, which
wasn't included in previous versions of the GNU Linux-libre kernel
because it was overlooked.

 * <https://news.softpedia.com/news/gnu-linux-libre-5-2-kernel-released-for-those-seeking-100-freedom-for-their-pcs-526671.shtml>

### gNewSense needs a new maintainer

*From July 29*

One of our earliest recommended, fully free distributions of GNU/Linux
is seeking a new maintainer. If you'd like to help out with
[gNewSense](http://gnewsense.org/), please get in touch with its
development community at the link below.

 * <https://lists.nongnu.org/mailman/listinfo/gnewsense-dev>

### Job listing: GNU/Linux system administrator for Aleph Objects 

*From July 24*

This is a unique opportunity in the growing field of 3D printing, in
one of the world's only IT environments running exclusively free
software. As a GNU/Linux system administrator, you will be responsible
for planning, deploying, and maintaining a variety of GNU/Linux
servers running services such as Postfix, Asterisk, NextCloud,
OpenNebula, Matomo, and more. A four-year degree in a related technical
field and two to five years of GNU/Linux system administration
experience are required. Help the Aleph Objects team prove that you
can do more with "free as in freedom" tools!

 * <https://www.fsf.org/resources/jobs/gnu-linux-system-administrator-aleph-objects>

### Net Neutrality Defense Guide: Summer 2019 edition 

*From July 2019 by EFF*

Net neutrality is in an interesting place. The Federal Communications
Commission (FCC) repealed the 2015 Open Internet Order in 2017, and
had to defend its decision in court. That case is still pending. In
2018, the Senate voted to overturn the FCC’s repeal, but the House of
Representatives did not. In 2019, the House of Representatives voted
for the *Save the Internet Act*, which would make the 2015 Open
Internet Order a law that the FCC cannot repeal. So now, once again,
it is up to the Senate to stand up for net neutrality.

To that end, EFF has prepared a toolkit designed to help local groups
and individuals take advantage of the fact that senators will be home
while Congress is on recess.

 * <https://www.eff.org/net-neutrality-defense-2019-edition>

### Adblocking: How about nah?

*From July 25 by Cory Doctorow*

The rise and rise of ad-blockers (and ad-blocker-blocker-blockers) is
without parallel: 26% of Internet users are now blocking ads, and the
figure is rising. It’s been called the biggest boycott in human
history. However, the adoption of [Encrypted Media Extensions
(EME)](https://www.defectivebydesign.org/blog/w3c_sells_out_web_eme_1_year_later)
by the World Wide Web Consortium is the first crack in the wall that
protected browsers from those who would thwart adversarial operability
and take "how about nah?" off the table, leaving us with the kind of
take-it-or-leave-it Web that the marketing industry has been striving
for since the first pop-up ad.

 * <https://www.eff.org/deeplinks/2019/07/adblocking-how-about-nah>

### Discovering whether your iPhone has been hijacked is nearly impossible thanks to Apple's walled garden

*From May 15 by Cory Doctorow*

[Apple](https://www.fsf.org/campaigns/apple) has received a lot of
praise for the security of its iOS devices, which are said to be so
secure in part because of Apple's walled garden strategy, which
prevents iPhone owners from running third-party software unless it
comes through Apple's App Store; and which limits who can repair Apple
devices, and whether they can use third-party replacement parts. All
of this control is said to produce a much more limited attack surface,
with fewer bugs, which are corrected more quickly.

However, this same security model treats owners of Apple devices as
potential attackers and goes to enormous lengths to prevent someone
who owns an Apple device from ever learning exactly how it works, so
that some processes can run in areas that users can't inspect or
control. If a hijacker sneaks into those areas, they can attack you
without you knowing what they're doing and reconfiguring your device
to kick them out. You're left almost completely defenseless.

Apple's censorship of apps is an attack on the user, and would be
inexcusable even if it didn't lead to insecurity against other
attackers. You can read more about why to avoid Apple, and what
alternatives are available so far,
[here](https://www.fsf.org/blogs/community/the-apple-is-still-rotten-why-you-should-avoid-the-new-iphone).

 * <https://boingboing.net/2019/05/15/brittle-security.html>

### Gnuastro 0.9 released, goal defined for 1.0

*From April 17 by Mohammad Akhlaghi*

The GNU Astronomy Utilities (Gnuastro) is an official GNU package
consisting of various programs and library functions for the
manipulation and analysis of astronomical data. All the programs share
the same basic command line user interface for the comfort of both the
users and developers. Gnuastro is written to comply fully with GNU
coding standards, so it integrates finely with the GNU/Linux operating
system. This also enables astronomers to expect a fully familiar
experience in the source code, building, installing and command line
user interaction that they have seen in all the other GNU software
that they use.

The current stable release is [Gnuastro
0.9](http://ftp.gnu.org/gnu/gnuastro/gnuastro-0.9.tar.gz). Use a
mirror if possible. New releases are announced in
[info-gnuastro](https://lists.gnu.org/mailman/listinfo/info-gnuastro). To
stay up to date, please subscribe.

 * <https://www.gnu.org/savannah-checkouts/gnu/gnuastro/gnuastro.html>
 * <https://git.savannah.gnu.org/cgit/gnuastro.git/plain/NEWS?id=gnuastro_v0.9>

### FaceApp makes today's privacy laws look antiquated

*From July 20 by Tiffany C. Li*

Yes, you should stop using FaceApp, because there are few controls on
how your data, including your face data, will be used. But the
problems that FaceApp poses aren’t unique. Walking around anywhere can
get your face included in facial-recognition databases. How that
information can be mined, manipulated, bought, or sold is minimally
regulated -- in the United States and elsewhere. Militaries, law
enforcement agencies, and commercial interests alike envision
far-reaching uses of AI and facial recognition, but legal and
regulatory controls lag far behind the pace of technology.

For most people, never going outside is not an option. So laws in the
United States and elsewhere need to be tuned up quickly -- and not
just because of FaceApp.

One deeper problem with this app, and other similar apps, is that they
send photos to a remote server to edit it.  We call that ["service as
a software
substitute,"](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html)
because in order to get the job done, you have to relinquish control
over your photo by sending it far away, where people can do whatever
they like to it. We maintain that any manipulation of your photos
ought to be done by a program you install on your own computer -- a
free program, in order to respect your freedom and autonomy -- and
never sent anywhere.

 * <https://www.theatlantic.com/ideas/archive/2019/07/faceapp-reveals-huge-holes-todays-privacy-laws/594358/>

### I found your data. It's for sale.

*From July 18 by Geoffrey A. Fowler*

Working with an independent security researcher, the author of this
article found that as many as 4 million people have been leaking
personal and corporate secrets through browser extensions on Chrome
and Firefox. Of course, these leaks are only one of myriad examples of
nonfree software acting as malware, performing actions you don't
control and might not even know about as you browse the Internet.

 * <https://www.washingtonpost.com/technology/2019/07/18/i-found-your-data-its-sale>

### Pearson shifts to Netflix-style subscription model for textbooks 

*From July 16 by Jim Waterson*

Pearson is switching to a "Netflix-style subscription-based model."
Which is to say: students will be pressured to rent textbooks on
subscription, readable only with proprietary malware with DRM.

 * <https://www.theguardian.com/media/2019/jul/16/pearson-netflix-style-rental-academic-textbooks>

### EmacsConf 2019 opens call for proposals

*From July 29 by Amin Bandali*

EmacsConf 2019 has issued its call for proposals! The conference will
be held virtually on November 2. They're also looking for suggestions
on how to run this year's conference using 100% free software, so
please have a look!

 * <https://emacsconf.org/2019/>

### Call for GUADEC 2020 bid proposals

*From July 19 by the GNOME Foundation*

The GNOME Foundation would like to invite bids for hosting GUADEC
2020.  GUADEC is the biggest gathering of GNOME users and developers,
which takes place every year, and you could make it happen next year!

If you are interested in submitting a bid to host GUADEC 2020 in your
city, please send an intention to bid by end of the day on Friday,
August 16th. Bids will be due on September 13th, and you can talk to
the Foundation Board and previous GUADEC organizers to find out more
about what is involved.

 * <https://www.gnome.org/news/2019/07/call-for-guadec-2020-bid-proposals/>

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, August 2, from 12pm to 3pm EDT (16:00 to
19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: Free Software Foundation/Ideas

Every month on LibrePlanet, we highlight one resource that is
interesting and useful -- often one that could use your help.

For this month, we are highlighting Free Software Foundation/Ideas,
which provides a space for you to post and discuss ideas for the
FSF. You are invited to adopt, spread and improve this important
resource.

  * <https://libreplanet.org/wiki/Group:Free_Software_Foundation/Ideas>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 16 new GNU releases!

16 new GNU releases in the last month (as of July 25, 2019):

* [denemo-2.3.0](https://www.gnu.org/software/denemo/)
* [direvent-5.2](https://www.gnu.org/software/direvent/)
* [gama-2.06](https://www.gnu.org/software/gama/)
* [gnunet-0.11.6](https://www.gnu.org/software/gnunet/)
* [gnupg-2.2.17](https://www.gnu.org/software/gnupg/)
* [grub-2.04](https://www.gnu.org/software/grub/)
* [guile-2.2.6](https://www.gnu.org/software/guile/)
* [libmicrohttpd-0.9.65](https://www.gnu.org/software/libmicrohttpd/)
* [libtasn1-4.14](https://www.gnu.org/software/libtasn1/)
* [linux-libre-5.2](https://www.gnu.org/software/linux-libre/)
* [nettle-3.5](https://www.gnu.org/software/nettle/)
* [parallel-20190722](https://www.gnu.org/software/parallel/)
* [pies-1.4](https://www.gnu.org/software/pies/)
* [rush-2.1](https://www.gnu.org/software/rush/)
* [stow-2.3.0](https://www.gnu.org/software/stow/)
* [tramp-2.4.2](https://www.gnu.org/software/tramp/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>. You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### Upcoming FSF and free software events

 * August 23-28, Thessaloniki, Greece, [GUADEC, the GNOME User and Developer European Conference](https://2019.guadec.org/)
 * September 4-6, Madrid, Spain, [GNU Hackers Meeting](https://www.fsf.org/events/event-20190904-madrid-gnuhackersmeeting)
 * September 7-13, Milan, Italy, [Akademy 2019](https://akademy.kde.org/2019)
 * September 16-20, Huntsville, AL, USA, [GNU Radio Conference 2019 (GRCon)](https://www.gnuradio.org/grcon/grcon19/)
 * November 2, 2019, online, [EmacsConf 2019](https://emacsconf.org/2019/)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month. This month's list includes
some donors who we neglected to thank in 2018 -- we're so sorry we
overlooked you, and grateful for your continued support!

  * <https://www.gnu.org/thankgnus/2019supporters.html>

This month, a big Thank GNU to:

* Alexandre BLANC
* Balta Katei 
* Blue Systems
* Bret Fisher
* Brian Strand 
* Bruno Dantas 
* Christoph Reichenbach 
* Donald Craig 
* Eric Culp
* Eric Herman
* François Badier
* Harry Mangalam
* Jerome Quinn
* joerg kunze
* John Owen
* Julio Claudio Matus Ramirez
* Kevin McCarthy 
* Li-Cheng Tai
* Mark Wielaard
* Martin Jansche
* Martin Krafft 
* Masaru KIMURA
* Michael Cornelius
* Michael Henderson 
* Michael Mauger 
* Nicolas Pottier
* Olivier Warin
* Paul Reiber 
* Mr. Pete Batard 
* Peter Mastren
* Philipp Weis 
* Reed Loden
* René Genz 
* Scott Anecito
* Sebastian Gfeller
* Stefan Hagen
* Steven Hay
* Tegonal GmbH
* Thomas Saglio 
* Tyler Romeo
* William Bolella 
* Yidong Chong

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF in the past month:

* Elizabeth Han (GNU Gzip)
* Hengda Shi (GNU Gzip)
* Valve Corporation (glibc)
* Will Fehrnstrom (GNU Gzip)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something
here for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software
patents, Digital Restrictions Management (DRM), free software
adoption, OpenDocument, and more.


###

Copyright © 2019 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

