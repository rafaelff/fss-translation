# Free Software Supporter
Edição 136, agosto de 2019

Bem-vindo(a) ao Suporte ao Software Livre, um resumo de notícias e
ações mensal da Free Software Foundation (FSF) -- sendo lido por você
e mais 210.274 outros ativistas. São 1.831 a mais em relação ao mês
passado!


## ÍNDICE 

* Obrigado por avançar o software livre: leia as notícias da primavera da FSF no mais recente *Boletim*
* Fortaleça o software livre dizendo ao Congresso dos EUA para rejeitar o Decreto STRONGER Patents
* E-book do apocalipse da Microsoft mostra o lado escuro do DRM
* A novela de Doctorow, "Unauthorized Bread", explica por que precisamos combater o DRM hoje para evitar um futuro sombrio
* Estágios de outono na FSF! Inscreva-se até 2 de setembro
* Junho de 2019: Fotos das conversas de RMS em Brno
* Kernel GNU Linux-libre 5.2 lançado para aqueles que buscam 100% de liberdade para seus PCs
* O gNewSense precisa de um novo mantenedor
* Lista de vagas: administrador de sistema GNU/Linux para Aleph Objects
* Guia de Defesa de Neutralidade da Rede: edição de verão de 2019
* Adblocking: Que tal não?
* Descobrir se o seu iPhone foi sequestrado é quase impossível graças ao jardim murado da Apple
* Gnuastro 0.9 lançado, objetivo definido para 1.0
* FaceApp faz com que as leis de privacidade de hoje pareçam antiquadas
* Encontrei seus dados. Estão à venda.
* Pearson muda para o modelo de assinatura do estilo Netflix para livros didáticos
* EmacsConf 2019 abre chamada para propostas
* Chamada para intenções de proposta para o GUADEC 2020
* Junte-se à FSF e amigos na atualização do Diretório de Software Livre
* Recurso em destaque na LibrePlanet: Free Software Foundation/Ideas
* Holofote GNU com Mike Gerwitz: 16 novos lançamentos GNU!
* Próximos eventos da FSF e software livre
* Agradecimentos do GNU!
* Contribuições para o copyright do GNU
* Entre em ação com a FSF!

Leia esse boletim <em>on-line</em> aqui:
<https://www.fsf.org/free-software-supporter/2019/august>

Incentive seus amigos a se inscrever e nos ajudar a aumentar nosso
público adicionando nosso <em>widget</em> de assinante ao seu <em>site</em>.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma notícia? Você pode recuperá-la nos boletins passados em
<https://www.fsf.org/free-software-supporter>.

###

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2019/agosto-sp>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2019/aout>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2019/agosto-pr>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### 

###

### Obrigado por avançar o software livre: leia as notícias da primavera da FSF no mais recente *Boletim*

*8 de julho*

No início de junho, enviamos uma cópia física do nosso [*Boletim Free
Software Foundation (FSF)*](https://www.fsf.org/bulletin/2019/spring/issue-34-spring-2019)
bianual de primavera para 11.715 dos nossos mais ativos defensores de
software livre em todo o globo. Nosso *Boletim* é escrito pela equipe
da FSF e ativistas do software livre, e agora também está disponível
<em>on-line</em>.

 * <https://www.fsf.org/blogs/community/thank-you-for-advancing-free-software-read-fsf-spring-news-in-the-latest-bulletin>

### Fortaleça o software livre dizendo ao Congresso dos EUA para rejeitar o STRONGER Patents Act

*25 de julho*

Apesar de não ter conseguido passar em 2017, um projeto de lei com o
apropriado título orwelliano de Decreto de patentes "Tecnologia de
Suporte e Pesquisa do Crescimento e da Resiliência Econômica de Nossa
Nação" (STRONGER na sigla em inglês) foi reintroduzido no Congresso
dos EUA em 10 de julho de 2019. Se aprovado, o Decreto faria as
patentes sobre ideias de software serem mais facilmente reivindicadas
e executáveis contra desenvolvedores na comunidade do software livre.
Quaisquer que sejam seus efeitos em outros tipos de patentes, o fato
de que vai sustentar patentes sobre ideias de software é razão
suficiente para rejeitá-lo.

 * <https://www.fsf.org/blogs/community/strengthen-free-software-by-telling-congress-to-reject-the-stronger-patents-act>

### Ebook do apocalipse da Microsoft mostra o lado escuro do DRM

*30 de junho, por Brian Barrett*

Claro, não há realmente um lado "light" para o [Gestão Digital de
Restrições (DRM)](https://www.defectivebydesign.org/) -- mas este
artigo da *Wired* deu uma chance ao diretor executivo da FSF, John
Sullivan, para falar sobre os perigos do DRM em um estágio mais amplo:
por causa do DRM, você realmente não possui os e-books que você
compra, o que permitiu à Microsoft efetivamente queimar milhões de
livros quando eles fecharam sua loja de e-books neste verão.

 * <https://www.wired.com/story/microsoft-ebook-apocalypse-drm/>

### A novela de Doctorow, "Unauthorized Bread", explica por que precisamos combater o DRM hoje para evitar um futuro sombrio

*18 de julho*

Salima tem um problema: sua torradeira Boulangism está travada com
software que garante que só vai torrar pão vendido a ela pela Empresa
Boulangism… e como a Boulangism saiu do negócio, não há como comprar
pão autorizado. Assim, Salima não pode mais fazer torradas.

Este cenário sorrateiramente familiar envia nossa heroína engenhosa
por um buraco de coelho no mundo dos aparelhos <em>hackeados</em>, mas
]também a coloca em perigo de perder sua casa -- em um processo sob os
termos draconianos do Digital Millennium Copyright Act (DMCA). Sua
história, contada na novela "Unauthorized Bread" (Pão Não Autorizado),
que abre o recente livro de Cory Doctorow *Radicalized*, orienta os
leitores através de um processo de descobrimento do que o [Digital
Restrictions Management (DRM)](https://www.defectivebydesign.org/) é,
e como o futuro pode parecer poderosamente sombrio se não unirmos
forças para parar o DRM agora.

 * <https://www.defectivebydesign.org/blog/doctorows_novella_unauthorized_bread_explains_why_we_have_fight_drm_today_avoid_grim_future>

### Estágios de outono na FSF! Inscreva-se até 2 de setembro 

*25 de julho*

A Free Software Foundation (FSF) está procurando estagiários para
contribuir para nossas campanhas, área de licenciamento ou equipe
técnica. Estas posições são, oportunidades educacionais não
remuneradas, e a FSF fornecerá qualquer documentação apropriada que
você possa precisar para receber financiamento e crédito escolar de
fontes externas. Nós colocamos ênfase na oferta de oportunidades
educacionais práticas para estagiários, nas quais eles trabalham em
estreita colaboração com os mentores da equipe em projetos que
correspondem às suas habilidades e interesses. O prazo para inscrição
é 2 de setembro. Veja <em>link</em> abaixo para saber como se
inscrever.

 * <https://www.fsf.org/blogs/community/fall-internships-at-the-fsf-apply-by-september-2>

### Junho de 2019: Fotos das conversas de RMS em Brno 

*5 de julho*

O presidente da Free Software Foundation, Richard Stallman (RMS)
esteve em Brno, República Tcheca, em 6 de junho de 2019, para dar duas
palestras. De manhã, participou na Feira URBIS Smart City, no Brno
Fair Grounds, para sua palestra "Computação, liberdade e privacidade".
à tarde, na Universidade de Masaryk University Cinema Scala, ele deu
sua palestra "O movimento software livre e o funcionamento do sistema
GNU/Linux", para cerca de trezentas pessoas.

 * <https://www.fsf.org/blogs/rms/photo-blog-2019-june-brno>

### Kernel GNU Linux-libre 5.2 lançado para aqueles que buscam 100% de liberdade para seus PCs

*9 de julho, por Marius Nestor*

O projeto GNU Linux-libre lançou o kernel GNU Linux-libre 5.2, uma
versão 100% livre do kernel Linux que não inclui qualquer driver
proprietário, <em>firmware</em> ou código.

Baseado na recém-lançada série do kernel Linux 5.2, que introduz o
suporte Sound Open Firmware para dispositivos de áudio DSP, o kernel
GNU Linux-libre 5.2 também vem com o <em>firmware</em> livre, que não
foi incluído nas versões anteriores do kernel GNU Linux-libre por um
descuido.

 * <https://news.softpedia.com/news/gnu-linux-libre-5-2-kernel-released-for-those-seeking-100-freedom-for-their-pcs-526671.shtml>

### O gNewSense precisa de um novo mantenedor

*29 de julho*

Uma das nossas primeiras distribuições totalmente livres do
GNU/Linux está procurando um novo mantenedor. Se você gostaria de
ajudar com o [gNewSense](http://gnewsense.org/), por favor entre em
contato com a comunidade de desenvolvimento no <em>link</em> abaixo.

 * <https://lists.nongnu.org/mailman/listinfo/gnewsense-dev>

### Lista de vagas: administrador de sistema GNU/Linux para Aleph Objects

*24 de julho*

Esta é uma oportunidade única no crescente campo da impressão 3D, em
um dos únicos ambientes de TI do mundo rodando exclusivamente software
livre. Como administrador do sistema GNU/Linux, você será responsável
por planejar, implantar e manter uma variedade de servidores GNU/Linux
executando serviços como Postfix, Asterisk, NextCloud, OpenNebula,
Matomo e muito mais. É necessária uma experiência de quatro anos como
técnico relacionado ao campo e de dois a cinco anos de administração de
sistemas GNU/Linux. Ajude o time da Aleph Objects a provar que você
pode fazer mais com ferramentas "livres como em liberdade"!

 * <https://www.fsf.org/resources/jobs/gnu-linux-system-administrator-aleph-objects>

### Guia de Defesa de Neutralidade da Rede: edição de verão de 2019

*Julho de 2019, por EFF*

A neutralidade da rede está em uma posição interessante. A Federal
Communications Commission (FCC) revogou a Ordem de Acesso Aberto à
Internet de 2015 em 2017, e teve que defender sua decisão no tribunal.
Esse caso ainda está pendente. Em 2018, o Senado votou para anular a
revogação da FCC, mas a Câmara dos Representantes não acatou. Em 2019,
a Câmara dos Representantes votou o *Save the Internet Act*, que
tornaria a Ordem de Acesso Aberto à Internet, de 2015, uma lei que a
FCC não pode revogar. Então agora, mais uma vez, cabe ao Senado
defender a neutralidade da rede.

Para esse fim, a EFF preparou um <em>kit</em> de ferramentas projetado
para ajudar grupos locais e indivíduos aproveitarem o fato de que os
senadores estarão em casa enquanto o Congresso está em recesso.

 * <https://www.eff.org/net-neutrality-defense-2019-edition>

### Adblocking: Que tal um não?

*25 de julho, por Cory Doctorow*

A contínua ascensão dos bloqueadores de anúncios (e bloqueadores de
bloqueadores de anúncios) é sem paralelo: 26% dos usuários da Internet
estão bloqueando anúncios, e este número está subindo. Isto tem sido
chamado de o maior boicote na história da humanidade. No entanto, a
adoção do [Extensões de Mídia Criptografada (EME na sigla em
inglês)](https://www.defectivebydesign.org/blog/w3c_sells_out_web_eme_1_year_later)
pelo World Wide Web Consortium é a primeira rachadura na parede que
protegia navegadores daqueles que impediriam a operacionalidade
do adversário e jogariam o "que tal um não?" fora da mesa,
deixando-nos com o tipo de Web fique-com-ele-ou-deixe-o com que a
indústria de <em>marketing</em> tem se esforçado desde o primeiro
anúncio <em>pop-up</em>.

 * <https://www.eff.org/deeplinks/2019/07/adblocking-how-about-nah>

### Descobrir se o seu iPhone foi sequestrado é quase impossível graças ao jardim murado da Apple

*15 de maio, por Cory Doctorow*

A [Apple](https://www.fsf.org/campaigns/apple) recebeu muitos elogios
pela segurança de seus dispositivos iOS, que dizem ser tão seguros, em
parte, por causa da estratégia de jardim murado da Apple, que impede
que proprietários de iPhone executem software de terceiros, a menos
que através da App Store da Apple; e que limita quem pode reparar os
dispositivos da Apple, e se eles podem usar peças de reposição de
terceiros. É dito que todo este controle é para produzir uma
superfície de ataque muito mais limitada, com menos <em>bugs</em>, que
são corrigidos mais rapidamente.

No entanto, esse mesmo modelo de segurança trata os proprietários de
dispositivos da Apple como atacantes em potencial, e vai ainda mais
longe executando processos em áreas que os usuários não podem
inspecionar ou tenham controle, impedindo que o dono de um dispositivo
da Apple possa aprender exatamente como ele funciona. Se um
sequestrador se infiltra nessas áreas, eles podem atacá-lo sem você
saber o que eles estão fazendo, do contrário você poderia reconfigurar
seu dispositivo para expulsá-los. Você está quase completamente
indefeso.

A censura de aplicativos da Apple é um ataque ao usuário e é
indesculpável, mesmo que não leve à insegurança contra outros
atacantes. Você pode ler mais sobre por que evitar a Apple e que
alternativas estão disponíveis até agora,
[aqui](https://www.fsf.org/blogs/community/the-apple-is-still-rotten-why-you-devem-evitar-o-new-iphone).

 * <https://boingboing.net/2019/05/15/brittle-security.html>

### Gnuastro 0.9 lançado, objetivo definido para 1.0

*17 de abril, por Mohammad Akhlaghi*

O GNU Astronomy Utilities (Gnuastro) é um pacote GNU oficial que
consiste em vários programas e funções de biblioteca para a
manipulação e análise de dados astronômicos. Todos os programas
compartilham a mesma interface de usuário de linha de comando básica
para o conforto de usuários e desenvolvedores. O Gnuastro foi escrito
para atender totalmente aos padrões de codificação GNU, para que ele
se integre perfeitamente ao sistema operacional GNU/Linux. Isso também
permite que os astrônomos esperem uma experiência totalmente familiar
no código-fonte, construção, instalação e interação do usuário da
linha de comando que eles viram em todos os outros softwares GNU que
eles usam.

A versão estável atual é
[Gnuastro 0.9](http://ftp.gnu.org/gnu/gnuastro/gnuastro-0.9.tar.gz).
Use um espelho, se possível. Novos lançamentos são anunciados em
[info-gnuastro](https://lists.gnu.org/mailman/listinfo/info-gnuastro).
Para se manter atualizado, por favor, inscreva-se.

 * <https://www.gnu.org/savannah-checkouts/gnu/gnuastro/gnuastro.html>
 * <https://git.savannah.gnu.org/cgit/gnuastro.git/plain/NEWS?id=gnuastro_v0.9>

### FaceApp faz com que as leis de privacidade de hoje pareçam antiquadas

*20 de julho, por Tiffany C. Li*

Sim, você deve parar de usar o FaceApp, porque há poucos controles
sobre como seus dados, incluindo seus dados de rosto, serão usados.
Mas os problemas que o FaceApp apresenta não são exclusivos. Andar
por aí em qualquer lugar pode incluir seu rosto em bancos de dados
de reconhecimento facial. Como essa informação pode ser extraída,
manipulada, comprada ou vendida é minimamente regulamentada - nos
Estados Unidos e em outros lugares. Militares, órgãos de segurança
pública e interesses comerciais vislumbram usos de longo alcance de
inteligência artificial e reconhecimento facial, mas os controles
legais e regulatórios estão muito aquém do ritmo da tecnologia.

Para a maioria das pessoas, nunca sair de casa não é uma opção. Assim,
as leis nos Estados Unidos e em outros lugares precisam ser ajustadas
rapidamente - e não apenas por causa do FaceApp.

Um problema mais profundo com este aplicativo e outros aplicativos
semelhantes é que eles enviam fotos para um servidor remoto para
editá-lo. Nós chamamos isso de ["serviço como substituto de
software"](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html)
porque, para fazer o trabalho, você tem que abandonar o controle sobre
sua foto enviando-a para longe, onde as pessoas podem fazer o que
quiserem. Nós sustentamos que qualquer manipulação de suas fotos deve
ser feita por um programa que você instala em seu próprio computador -
um programa livre, a fim de respeitar sua liberdade e autonomia - e
nunca enviado para qualquer lugar.

 * <https://www.theatlantic.com/ideas/archive/2019/07/faceapp-reveals-huge-holes-todays-privacy-laws/594358/>

### Encontrei seus dados. Estão à venda.

*18 de julho, por Geoffrey A. Fowler*

Trabalhando com um pesquisador de segurança independente, o autor
deste artigo descobriu que cerca de 4 milhões de pessoas vazaram
segredos pessoais e corporativos através de extensões de navegador
no Chrome e no Firefox. Naturalmente, esses vazamentos são apenas
um dos muitos exemplos de softwares não livres agindo como malware,
realizando ações que você não controla e nem sequer conhece enquanto
navega na Internet.

 * <https://www.washingtonpost.com/technology/2019/07/18/i-found-your-data-its-sale>

### Pearson muda para o modelo de assinatura do estilo Netflix para livros didáticos

*16 de julho, por Jim Waterson*

Pearson está mudando para um "modelo baseado em assinatura no estilo
Netflix". O que quer dizer: os estudantes serão pressionados a alugar
livros didáticos por assinatura, somente legíveis com malware
proprietário com DRM.

 * <https://www.theguardian.com/media/2019/jul/16/pearson-netflix-style-rental-academic-textbooks>

### EmacsConf 2019 abre chamada para propostas

*29 de julho, por Amin Bandali*

O EmacsConf 2019 lançou sua chamada para propostas! A conferência será
realizada virtualmente em 2 de novembro. Eles também estão procurando
sugestões sobre como executar a conferência deste ano usando software
100% livre, então, por favor, dê uma olhada!

 * <https://emacsconf.org/2019/>

### Chamada para intenções de proposta para o GUADEC 2020

*19 de julho, pela GNOME Foundation*

A GNOME Foundation gostaria de convidar propostas para hospedar o
GUADEC 2020. O GUADEC é o maior encontro de usuários e desenvolvedores
do GNOME, que acontece todo ano, e você pode fazer acontecer no
próximo ano!

Se você estiver interessado em enviar uma proposta para sediar o
GUADEC 2020 em sua cidade, envie uma intenção de fazer uma proposta
até o final do dia na sexta-feira, 16 de agosto. As intenções vão
terminar no dia 13 de setembro, e você pode conversar com o Conselho
da Fundação e os organizadores da GUADEC para saber mais sobre o que
está envolvido.

 * <https://www.gnome.org/news/2019/07/call-for-guadec-2020-bid-proposals/>

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam directory.fsf.org todos os
meses para descobrir <em>software</em> livre. Cada registro no
Diretório contém uma grande quantidade de informações úteis, desde
categorias básicas e descrições até controle de versão, canais de IRC,
documentação e licenciamento. O Diretório de Software Livre tem sido
um ótimo recurso para usuários de <em>software</em> na última década,
mas precisa de sua ajuda para manter-se atualizado com novos e
interessantes projetos de <em>software</em> livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org
e geralmente incluem um punhado de regulares, bem como recém-chegados.
O Freenode é acessível a partir de qualquer cliente de IRC -- Todos
são bem-vindos!

A próxima reunião é sexta-feira, 2 de agosto, das 13h às 16h BRT
(16h à 19h UTC). Detalhes aqui:

  * <https://directory.fsf.org/wiki/Main_Page>

### Recurso em destaque na LibrePlanet: Free Software Foundation/Ideas

Todos os meses na LibrePlanet, destacamos um recurso que é útil e
interessante -- geralmente um recurso que poderia receber sua ajuda.

Para este mês, estamos destacando Free Software Foundation/Ideas,
que fornece um espaço para você publicar e discutir ideias para a
FSF. Você está convidado para adotar, espalhar e melhorar este
importante recurso.

  * <https://libreplanet.org/wiki/Group:Free_Software_Foundation/Ideas>

Você tem uma sugestão para o recurso em destaque do próximo mês?
Nos avise em <campaigns@fsf.org>.

### Holofote GNU com Mike Gerwitz: 16 novos lançamentos GNU!

16 novos lançamentos GNU neste último mês (até 25 de julho de 2019):

* [denemo-2.3.0](https://www.gnu.org/software/denemo/)
* [direvent-5.2](https://www.gnu.org/software/direvent/)
* [gama-2.06](https://www.gnu.org/software/gama/)
* [gnunet-0.11.6](https://www.gnu.org/software/gnunet/)
* [gnupg-2.2.17](https://www.gnu.org/software/gnupg/)
* [grub-2.04](https://www.gnu.org/software/grub/)
* [guile-2.2.6](https://www.gnu.org/software/guile/)
* [libmicrohttpd-0.9.65](https://www.gnu.org/software/libmicrohttpd/)
* [libtasn1-4.14](https://www.gnu.org/software/libtasn1/)
* [linux-libre-5.2](https://www.gnu.org/software/linux-libre/)
* [nettle-3.5](https://www.gnu.org/software/nettle/)
* [parallel-20190722](https://www.gnu.org/software/parallel/)
* [pies-1.4](https://www.gnu.org/software/pies/)
* [rush-2.1](https://www.gnu.org/software/rush/)
* [stow-2.3.0](https://www.gnu.org/software/stow/)
* [tramp-2.4.2](https://www.gnu.org/software/tramp/)

Para anúncios da maioria dos novos lançamentos GNU, inscreva-se
na lista de discussão info-gnu:
<https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para baixar: quase todo <em>software</em> GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou, de preferência, em um dos espelhos em
<https://www.gnu.org/prep/ftp.html>. Você pode usar a URL
<https://ftpmirror.gnu.org/> para ser redirecionado automaticamente
para um espelho próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como um
todo, estão procurando mantenedores e outras formas de auxílio: veja
<https://www.gnu.org/server/takeaction.html#unmaint> se você quiser
ajudar. A página geral sobre como ajudar o GNU está em
<https://www.gnu.org/help/help.html>.

Se você tem um programa funcionando, totalmente ou parcialmente, que
gostaria de oferecer ao projeto GNU como um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, sinta-se à vontade para escrever para nós em
<maintainers@gnu.org> com quaisquer perguntas sobre o GNU
ou sugestões para futuras edições.

### Próximos eventos da FSF e software livre

 * 23-28 de agosto, Thessaloniki, Grécia, [GUADEC, the GNOME User and Developer European Conference](https://2019.guadec.org/)
 * 4-6 de setembro, Madrid, Espanha, [GNU Hackers Meeting](https://www.fsf.org/events/event-20190904-madrid-gnuhackersmeeting)
 * 7-13 de setembro, Milão, Itália, [Akademy 2019](https://akademy.kde.org/2019)
 * 16-20 de setembro, Huntsville, AL, EUA, [GNU Radio Conference 2019 (GRCon)](https://www.gnuradio.org/grcon/grcon19/)
 * 2 de novembro de 2019, online, [EmacsConf 2019](https://emacsconf.org/2019/)

### Agradecimentos do GNU!

Agradecemos a todos que doam à Free Software Foundation e gostaríamos
de dar um reconhecimento especial às pessoas que doaram US$ 500,00 ou
mais no último mês. A lista deste mês inclui alguns doadores que
deixamos de agradecer em 2018 - lamentamos muito não ter mencionado
vocês e agradecemos por seu apoio contínuo!

  * <https://www.gnu.org/thankgnus/2019supporters.html>

Neste mês, um grande Agradecimento do GNU para:

* Alexandre BLANC
* Balta Katei 
* Blue Systems
* Bret Fisher
* Brian Strand 
* Bruno Dantas 
* Christoph Reichenbach 
* Donald Craig 
* Eric Culp
* Eric Herman
* François Badier
* Harry Mangalam
* Jerome Quinn
* joerg kunze
* John Owen
* Julio Claudio Matus Ramirez
* Kevin McCarthy 
* Li-Cheng Tai
* Mark Wielaard
* Martin Jansche
* Martin Krafft 
* Masaru KIMURA
* Michael Cornelius
* Michael Henderson 
* Michael Mauger 
* Nicolas Pottier
* Olivier Warin
* Paul Reiber 
* Mr. Pete Batard 
* Peter Mastren
* Philipp Weis 
* Reed Loden
* René Genz 
* Scott Anecito
* Sebastian Gfeller
* Stefan Hagen
* Steven Hay
* Tegonal GmbH
* Thomas Saglio 
* Tyler Romeo
* William Bolella 
* Yidong Chong

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Contribuições para o copyright do GNU

Atribuir seus direitos autorais (_copyright_) à Free Software
Foundation nos ajuda a defender a GPL e manter o <em>software</em>
livre. As seguintes pessoas atribuíram seus direitos autorais à FSF
no mês passado:

* Elizabeth Han (GNU Gzip)
* Hengda Shi (GNU Gzip)
* Valve Corporation (glibc)
* Will Fehrnstrom (GNU Gzip)

Quer ver seu nome nesta lista? Contribua para o GNU e atribua seus
direitos autorais à FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Entre em ação com a FSF!

Contribuições de milhares de membros individuais possibilitam trabalho
da FSF. Você pode contribuir unindo-se em <https://my.fsf.org/join>.
Se você já é um membro, você pode ajudar a indicar novos membros
(e ganhar algumas recompensas) adicionando uma linha com seu número
de membro à sua assinatura de e-mail como:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

A FSF está sempre procurando voluntários
(<https://www.fsf.org/volunteer>). Da agitação ao hacking, da
coordenação de problemas ao preenchimento de envelopes -- há algo
aqui para todo mundo fazer. Além disso, dirija-se à nossa seção de
campanhas (<https://www.fsf.org/campaigns>) e entre em ação contra
patentes de <em>software</em>, Gestão Digital de Restrições (DRM),
adoção de <em>software</em> livre, OpenDocument, Associação da
Indústria de Gravação da América (RIAA), e mais.


###

Copyright © 2019 Free Software Foundation, Inc.

Esta obra está licenciada sob uma licença Creative Commons
Atribuição 4.0 Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>.

