# Free Software Supporter
Issue 140, December 2019

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 216,727 other activists. That's 1,270 more than last month!

<table width="205" align="right" style="background-color: #EEEEEE; text-align: center; margin:10px; border: none !important;  padding: 0px;">
    <tbody>
        <tr>
           <td style="border: none !important;"><a href="https://my.fsf.org/join?pk_campaign=frfall2019&pk_source=decsupporter"><img width="200"src="https://static.fsf.org/nosvn/appeal2019/allpremiums.png" alt="fall 2019 fundraiser premiums"></a></td>
        </tr>
    </tbody>
</table>

### Strengthen the free software movement -- join the FSF today! 

*From November 18th*

No movement has ever succeeded without strength in numbers, which is
why we are looking to add 600 advocates to this cause by December
31st. [Becoming an FSF associate
member](https://my.fsf.org/join?pk_campaign=frfall2019&pk_source=decsupporter)
is easy. If you can spare a monthly $10 ($5 for students), your member
dues amplify your voice in the fight for your user rights, and help
the FSF fight to protect freedom for everyone. In appreciation, we
offer associate members many
[benefits](https://www.fsf.org/associate/benefits). If you have
already been an FSF member and your membership has expired, please
take this moment to
[renew](https://my.fsf.org/renew?pk_campaign=frfall2019&pk_source=decsupporter).

For this year-end fundraiser, we even have some exclusive gifts for
all associate members who join us, or renew, as well as some extra
motivation for those who can give a bit more than the base
membership. Already a member? Get your friends and family to join in
order to get the premiums and name you as the reason why they joined:
with three referrals, you get the thermos, with five, you get the
backpack, and with ten referrals, you can get all three gifts!

 * <https://www.fsf.org/appeal?pk_campaign=frfall2019&pk_source=decsupporte>
 * <https://www.fsf.org/blogs/community/come-together-for-free-software>

## TABLE OF CONTENTS

* Ethical Tech Giving Guide: Freedom is the gift that keeps on giving 
* LibrePlanet returns in 2020 to Free the Future! March 14-15, Boston area 
* New RYF Web site: It's now easier to support companies selling devices that Respect Your Freedom 
* Talos II Mainboard and Talos II Lite Mainboard now FSF-certified to Respect Your Freedom 
* Flying with SeaGL, blasting GNU Radio, and more from the Working Together for Free Software Fund 
* The FSF's EmacsConf 2019 satellite was an M-x success! 
* FSF contract opportunity: Bookkeeper 
* US federal court rules suspicionless searches of travelers' phones and laptops unconstitutional
* Digital Restrictions Management (DRM) cancels Netflix from older "smart" TVs and Roku media players
* GNU Radio first steps: An FM receiver
* School apps violate children's privacy by tracking when they go to the bathroom (and everything else that they do)
* Amazon Ring watched your kids trick or treat and then bragged about it
* The team that powers VLC
* The story of *Janayugom*: The first newspaper in the world to use 100% free software for news publishing
* How we fixed DRM in Portugal (and so can you)
* Best Buy is leaving "smart home" users in the cold
* Uber to allow audio recording of rides, aiming to launch feature in US 
* GUADEC 2020 and 2021 announcements
* GCC 7.5 released
* November GNU Emacs news
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Group: Hardware/research/e-readers
* GNU Spotlight with Mike Gerwitz: 17 new GNU releases!
* FSF and other free software events
* Thank GNUs!
* GNU copyright contributions
* Translations of the *Free Software Supporter*
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2019/december>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

Want to read this newsletter translated into another language? Scroll
to the end to read the *Supporter* in French, Spanish, or Portuguese.

***

### Ethical Tech Giving Guide: Freedom is the gift that keeps on giving 

*From November 27th*

Every year, we publish our [Ethical Tech Giving
Guide](https://www.fsf.org/givingguide), as a way to help free
software supporters choose gifts that won't burden the people they
care about with proprietary software or venomous Digital Restrictions
Management (DRM). Devices may come and go, but introducing another
person to software freedom is the start of a lifelong journey. And,
who wants to be responsible for burdening our loved ones with devices
that abuse their freedom or spy on them? Our Giving Guide helps you
give gifts you can feel good about instead!

 * <https://www.fsf.org/blogs/community/ethical-tech-giving-guide-freedom-is-the-gift-that-keeps-on-giving>
 * <https://www.fsf.org/givingguide>

### LibrePlanet returns in 2020 to Free the Future! March 14-15, Boston area 

*From November 7th*

The annual technology and social justice conference will be held in
the Boston area on March 14 and 15, 2020, with the theme "Free the
Future." The FSF invites activists, hackers, law professionals,
artists, students, developers, young people, policymakers, tinkerers,
newcomers to free software, and anyone looking for technology that
respects their freedom to [register to
attend](https://my.fsf.org/civicrm/event/info?id=87&reset=1)!

 * <https://www.fsf.org/news/libreplanet-returns-in-2020-to-free-the-future-march-14-15-boston-area>
 * <https://www.fsf.org/blogs/community/register-now-for-libreplanet-2020-free-the-future-in-boston-area-ma>

### New RYF Web site: It's now easier to support companies selling devices that Respect Your Freedom 

*From November 7th*

We are proud to announce that we're launching a new, stand-alone Web
presence for our Respects Your Freedom (RYF) certification program,
capable of facilitating its continued expansion. Users can check out
the new site at <https://ryf.fsf.org>. There, they can browse
certifications by vendor and device type, and learn about the most
recent certifications. Each device has its own page which directs
users to the certification announcement, date of certification, and a
link to the retailer site where they can purchase it.

 * <https://www.fsf.org/blogs/licensing/new-ryf-web-site-its-now-easier-to-support-companies-selling-devices-that-respect-your-freedom>

### Talos II Mainboard and Talos II Lite Mainboard now FSF-certified to Respect Your Freedom 

*From November 7th*

The FSF has awarded Respects Your Freedom (RYF) certification to the
Talos II and Talos II Lite mainboards from Raptor Computing Systems,
LLC. The RYF certification mark means that these products meet the
FSF's standards in regard to users' freedom, control over the product,
and privacy. These two mainboards are the first PowerPC devices to
receive certification. Several GNU/Linux distributions endorsed by the
FSF are currently working towards offering support for the PowerPC
platform.

 * <https://www.fsf.org/news/talos-ii-mainboard-and-talos-ii-lite-mainboard-now-fsf-certified-to-respect-your-freedom>

### Flying with SeaGL, blasting GNU Radio, and more from the Working Together for Free Software Fund 

*From November 4th*

Working Together for Free Software is one of our initiatives that
focuses on the broader world of free software: the community,
programs, and funding that we’re coalescing to mount the crucial
resistance to the abuses of proprietary software. This is a category
that covers a lot of people and a lot of work, and the [Working
Together for Free Software
Fund](https://www.fsf.org/working-together/fund) is just one piece of
the picture.

While all of the projects under the umbrella of the Working Together
for Free Software Fund are absolutely worthy of your attention and
donations, today we're highlighting just a few projects with some
noteworthy announcements. Want to know if your free software project
qualifies? [Learn more
here](https://www.fsf.org/working-together/fiscal-sponsorship)!

 * <https://www.fsf.org/blogs/community/flying-with-seagl-blasting-gnu-radio-and-more-from-the-working-together-for-free-software-fund>

### The FSF's EmacsConf 2019 satellite was an M-x success! 

*From November 22nd*

Together with Boston locals and more than 400 remote participants, the
FSF hosted a successful satellite to this year's EmacsConf, which was
held largely online on November 2nd. Don't worry if you missed it: you
can see 31 out of the 32 total talks [on
video](https://emacsconf.org/2019/videos)!

 * <https://www.fsf.org/blogs/community/the-fsfs-emacsconf-2019-satellite-was-an-m-x-success>

### FSF contract opportunity: Bookkeeper 

*From November 25th*

The contractor will work closely with our business operations manager
and the rest of the operations team to ensure that the organization's
day-to-day financial functions run smoothly. We are looking for a
hands-on and detail-oriented professional who is comfortable working
both independently and with multiple teams as needed. Ideal candidates
will be proactive and highly adaptable, with an aptitude for learning
new tools and paying close attention to minutiae despite dense
financial material. Applicants should have at least three years of
experience with nonprofit bookkeeping and finance. Familiarity with
tools we use is a plus, such as SQL Ledger, CiviCRM, LibreOffice, and
Request Tracker.

 * <https://www.fsf.org/news/contract-opportunity-bookkeeper>

### US federal court rules suspicionless searches of travelers' phones and laptops unconstitutional

*From November 12th by EFF*

In a major victory for privacy rights at the border, a federal court
in Boston ruled that suspicionless searches of travelers’ electronic
devices by federal agents at airports and other US ports of entry are
unconstitutional. Congratulations to the Electronic Frontier
Foundation (EFF) and the American Civil Liberties Union (ACLU) on
their victory in the fight for digital freedom!

 * <https://www.eff.org/press/releases/federal-court-rules-suspicionless-searches-travelers-phones-and-laptops>

### Digital Restrictions Management (DRM) cancels Netflix from older "smart" TVs and Roku media players

*From November 11th by Hilbert Hagedoorn*

In November, Samsung issued a warning that owners of particular
2010/2011 models of "smart" TVs would no longer be able to use
Netflix, starting in December 2019. This also applies to a number of
Roku media players, as well as some Panasonic televisions. Apparently,
the problem is that the DRM protocol in these earlier televisions has
been superseded and can't be upgraded. So now, thanks to DRM, these
TVs aren't just [defective by
design](https://www.defectivebydesign.org/): they're also obsolete by
design.

 * <https://www.guru3d.com/news-story/netflix-halts-supporting-samsung-and-other-televisions-from-2010-and-2011.html>

### GNU Radio first steps: An FM receiver

*From November 17th by Jan Hrach*

Per the request of a poster on *Hacker News*: in this walkthrough, you
can learn how to use GNU Radio with the GUI tool Companion, with an
SDR, to capture a portion of spectrum containing public FM
broadcasting and decode it to audio! The writer did this live at
InstallFest 2016, but this is the first time they've given the
instructions entirely in English. If you try it out, please let us
know how it went at <campaigns@fsf.org>!

 * <https://www.abclinuxu.cz/blog/jenda/2019/11/gnu-radio-first-steps-a-fm-receiver>

### US school apps violate children's privacy by tracking when they go to the bathroom (and everything else that they do)

*From October 29th by Heather Kelly*

When Christian Chase wants to take a bathroom break at his high
school, he can’t just raise his hand. Instead, the 17-year-old senior
makes a special request on his school-issued Chromebook computer. A
teacher approves it pending any red flags in the system, such as
another student he should avoid out in the hall at the same time, then
logs him back in on his return. If he were out of class for more than
a set amount of time, the application would summon an administrator to
check on him.

Classroom management software is growing more and more invasive and
impossible for children or parents to limit or reasonably consent
to. Anywhere from 200 to 600 applications might be used in a single
district, creating a bewildering maze of privacy agreements, school
policies, and federal privacy regulations. We encourage concerned
parents and kids to band together and demand that all nonfree software
be banished from their schools, and replaced by learning software that
respects their children's freedom (and doesn't concern itself with
their bathroom habits).

 * <https://www.washingtonpost.com/technology/2019/10/29/school-apps-track-students-classroom-bathroom-parents-are-struggling-keep-up/?arc404=true>

### Amazon Ring watched your kids trick or treat and then bragged about it

*From November 1st by Rachel Kraus*

"Where were you at 6pm last night? If you were trick-or-treating, you
were part of the millions of people out ringing doorbells this
Halloween!" The morning after Halloween, Ring shared videos from Ring
users, which featured masked and unmasked children and adults engaging
in Halloween antics. Some were funny moments of pranks and cuteness,
while others featured misbehavior like pumpkin theft.

The videos are pretty innocent, but it's tremendously creepy that Ring
decided to use video captured on Halloween as a public relations stunt
to show that, uh, Ring is always watching. *Mashable* asked Ring
whether the people featured in the videos gave their consent to be
used in a publicity stunt. Ring did not immediately respond, and since
the *Mashable* story has not been updated nearly a month later, they
apparently still have not deigned to answer these concerns.

 * <https://mashable.com/article/ring-halloween-surveillance/>

### The team that powers VLC

*From November 1st by Chris Stokel-Walker*

This article provides a look behind the curtain at one of the most
popular free software projects: VideoLAN Client, or VLC media
player. This customizable, high-powered media player was released in
2001 under a GNU General Public License, and has been downloaded 3
billion times since 2005!

 * <https://increment.com/teams/the-team-that-powers-vlc/>

### The story of *Janayugom*: The first newspaper in the world to use 100% free software for news publishing

*From November 11th, by Alpha Fork Technologies*

*Janayugom* is a Malayalam (a South-Indian language spoken in Kerala)
daily newspaper with nearly 100,000 subscribers and 14 bureaus in
Kerala. Previously, *Janayugom* used to depend upon an outdated
version of a proprietary software program (Adobe PageMaker) for their
layout and production related works. When they realized that the
modern software recommended to them for an upgrade was going to betoo
expensive, the solution led them to free software instead.

 * <https://poddery.com/posts/4691002>

### How we fixed DRM in Portugal (and so can you)

*From November 13th by Paula Simões and Marcos Marado*

After 15 years of trying to solve the Digital Restrictions Management
(DRM) problem, the Portuguese Association for Free Software (ANSOL)
and the Association for Free Education (AEL) finally managed to get
what they sought: a fix to the DRM situation in Portugal.

 * <https://fsfe.org/news/2019/news-20191113-01.en.html>

### Best Buy is leaving "smart home" users in the cold

*From September 6th by Nick Statt*

When you depend on connection to a company's servers to run the
software that controls your devices, you give them control over those
devices... including the abillity to decide arbitrarily that those
devices will no longer work. In September, Best Buy quietly announced
that the mobile app platform for controlling their Insignia brand of
"smart home" devices was shutting down, so while most basic functions
will still work, the ones controlled through the app are kaput.

 * <https://www.theverge.com/2019/9/6/20853671/best-buy-connect-insignia-smart-plug-wifi-freezer-mobile-app-shutdown-november-6>

### Uber to allow audio recording of rides, aiming to launch feature in US 

*From November 20th by Associated Press*

Yes, this feature will allow you to opt into recording all trips or
select trips, which is nice, and yes, the recordings will be
encrypted. However, as with all proprietary software, this feature,
touted as a potential way to make riding with Uber safer, is still out
of your control: you can't listen to the recordings, and because
they'll be stored on Uber's servers, you don't get to decide who
listens to them or how they'll be used (and, what happens if their
server is compromised?). Nice try, Uber, but no thanks!

 * <https://www.theguardian.com/technology/2019/nov/20/uber-record-rides-safety-brazil-mexico-us>

### GUADEC 2020 and 2021 announcements

*From November 25th by GNOME Project*

The GNOME Foundation is excited to announce that GUADEC 2020, the
annual GNOME conference, will take place between the 22nd - 28th of
July in Zacatecas, Mexico. In 2021, GUADEC will be returning to
Europe, taking place in Riga, Latvia.

 * <https://www.gnome.org/news/2019/11/locations-for-guadec-2020-and-2021-announced/>
 * <https://www.gnome.org/news/2019/11/guadec-2020-announcement/>

### GCC 7.5 released

*From November 14th by GCC*

The GNU Project and the GCC developers are pleased to announce the
release of GCC 7.5. This release is a bug-fix release, containing
fixes for regressions in GCC 7.4 relative to previous releases of GCC.

 * <https://gcc.gnu.org/gcc-7/>

### November GNU Emacs news

*From November 25 by Sacha Chua*

In these issues: EmacsConf 2019 videos, configuration options to copy,
configuring Emacs from scratch, a proposal to rename windows to
"panes," and more!

  * <https://sachachua.com/blog/2019/11/2019-11-25-emacs-news/>
 * <https://sachachua.com/blog/2019/11/2019-11-18-emacs-news/>
 * <https://sachachua.com/blog/2019/11/2019-11-11-emacs-news/>
 * <https://sachachua.com/blog/2019/11/2019-11-04-emacs-news/>

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, December 6th, from 12pm to 3pm EST (16:00
to 19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: Group: Hardware/research/e-readers

Every month on [the LibrePlanet
wiki](https://libreplanet.org/wiki/Main_Page), we highlight one
resource that is interesting and useful -- often one that could use
your help.

For this month, we are highlighting Group:
Hardware/research/e-readers, which provides information about the
quest to determine which existing ebook readers could become Respects
Your Freedom (RYF) certified without too much work. You are invited to
adopt, spread and improve this important resource.

  * <https://libreplanet.org/wiki/Group:Hardware/research/e-readers>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 17 new GNU releases!

17 new GNU releases in the last month (as of November 25, 2019):

* [auctex-12.2](https://www.gnu.org/software/auctex/)
* [cpio-2.13](https://www.gnu.org/software/cpio/)
* [emms-5.3](https://www.gnu.org/software/emms/)
* [gcc-7.5.0](https://www.gnu.org/software/gcc/)
* [gnuastro-0.11](https://www.gnu.org/software/gnuastro/)
* [gnunet-0.11.8](https://www.gnu.org/software/gnunet/)
* [gnupg-2.2.18](https://www.gnu.org/software/gnupg/)
* [guile-ncurses-3.0](https://www.gnu.org/software/guile-ncurses/)
* [health-3.6.2](https://www.gnu.org/software/health/)
* [libidn-3.3.0](https://www.gnu.org/software/libidn/)
* [libredwg-0.9.2](https://www.gnu.org/software/libredwg/)
* [libtasn1-4.15.0](https://www.gnu.org/software/libtasn1/)
* [linux-libre-5.4-gnu](https://www.gnu.org/software/linux-libre/)
* [mailutils-3.8](https://www.gnu.org/software/mailutils/)
* [mcron-1.1.3](https://www.gnu.org/software/mcron/)
* [mes-0.21](https://www.gnu.org/software/mes/)
* [parallel-20191122](https://www.gnu.org/software/parallel/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>. You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

This month, we welcome Mark Weaver as maintainer of GNUzilla.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### FSF and other free software events

 * March 14-15, 2020, Boston, MA, [LibrePlanet 2020: Free the Future](https://www.fsf.org/events/libreplanet-2020-march14-15-registration-open)
 * July 22-28, Zacatecas, Mexico, [GUADEC 2020](https://www.gnome.org/news/2019/11/guadec-2020-announcement/)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2019supporters.html>

This month, a big Thank GNU to:

* Camille Akmut
* Elliot Rosenberger
* Felix March Tantoso 
* Hideki IGARASHI
* Jean-Louis Abraham 
* Matteo Frigo
* Michael Lewis 
* René Genz
* Roland Pesch 
* Rich Haase
* Trevor Spiteri
* Zack Grannan 

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF (and allowed public
appreciation) in the past month:

* Archit Pandey (Wget)
* Axis Communications AB (findutils)
* David Odell (Diffutils)
* Davide Gerhard (GNU Radio)
* Grant Cox (GNU Radio)
* Keith Bershatsky (Emacs)
* Raffael Stocker (Emacs)
* Thomas Rummel (GDB)
* Takehiro Sekine (GNU Radio)
* Yuichiro Kaneko (Bison)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Translations of the *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2019/diciembre>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2019/decembre>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2019/dezembro>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something here
for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software patents,
Digital Restrictions Management (DRM), free software adoption,
OpenDocument, and more.

###

Copyright © 2019 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

