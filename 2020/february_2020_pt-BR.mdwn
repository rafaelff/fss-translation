# Suporte ao Software Livre
Edição 142, fevereiro de 2020

Bem-vindo(a) ao *Suporte ao Software Livre*, um resumo de notícias e
ações mensais da Free Software Foundation (FSF) -- sendo lido por você
e mais 225.142 outros ativistas. São 6.259 a mais em relação ao mês
passado!

### Diga à Microsoft para reciclar socialmente o Windows 7. Libertem-no!

*24 de janeiro*

A Microsoft deu alguns passos na direção certa, como liberar alguns
componentes pequenos mas importantes do Windows como Software Livre.
Queremos que eles acelerem essas mudanças. Precisamos que a Microsoft
prove ao mundo que seu "amor" ao Software Livre não é apenas uma
campanha publicitária e que eles não estão apenas colhendo os
benefícios do Software Livre, a fim de explorar os usuários,
pressionando-os a confiar no [Serviço como Substituto de
<em>software</em>](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html).

Eles podem fazer isso lançando o Windows 7 sob uma licença de Software
Livre. A história do Software Livre nos mostrou que o software
não precisa expirar e pode até mesmo ser escrito para durar cinquenta
anos. E agora que esta versão do sistema operacional atingiu
seu "fim de vida", eles não têm um bom motivo para não fazê-lo.

Nossa petição ultrapassou a meta de 7.777 assinantes, mas incentivamos
você a adicionar a sua voz também! Fique de olho no nosso <em>site
</em> e nas contas das redes sociais para ver o que faremos a seguir.

[**Assine a petição aqui até 5 de
fevereiro!**](https://www.fsf.org/windows/upcycle-windows-7)

 * <https://www.fsf.org/blogs/community/tell-microsoft-to-upcycle-windows-7-set-it-free>

## ÍNDICE

* Primeiro anúncio do LibrePlanet 2020, além de um novo local!
* Traga o planeta para o LibrePlanet patrocinando um participante
* O LibrePlanet 2020 precisa de você: voluntarie-se hoje!
* Chegou a hora de encerrar o processo de isenções anti-evasão do DMCA e por um fim no DRM
* Não caia na armadilha do rato: não se apaixone pela Disney+
* Encontro Boston CiviCRM em busca de novo organizador
* GNU Guile 3.0.0 lançado
* GNU Linux-libre 5.5 agora disponível; exigiu mais <em>deblobbing</em> do que o habitual
* Aplicativo Ring doorbell empacotado com rastreadores de terceiros
* Tópico no Reddit: Recentemente, fiz uma ressonância magnética total de cabeça. Usando Software Livre, consegui arrancar meu cérebro e transformá-lo em uma luz de mesa.
* EA vai quebrar o aplicativo de telefone Tetris
* Notícias de janeiro do GNU Emacs
* Junte-se à FSF e amigos na atualização do Diretório de Software Livre
* Recurso em destaque do LibrePlanet: LibrePlanet:Conference/2020
* Holofote GNU com Mike Gerwitz: 16 novos lançamentos GNU!
* Próximos eventos da FSF e do Software Livre
* Agradecimentos do GNU!
* Contribuições para o copyright do GNU
* Traduções do *Free Software Supporter*
* Entre em ação com a FSF!

Leia esse boletim <em>on-line</em> aqui:
<https://www.fsf.org/free-software-supporter/2020/february>

Incentive seus amigos a se inscrever e nos ajudar a aumentar nosso
público, adicionando nosso <em>widget</em> de assinante ao seu <em>site</em>.

  * Inscreva-se: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Perdeu uma notícia? Você pode recuperá-la nos boletins passados em
<https://www.fsf.org/free-software-supporter>.

Deseja ler este boletim traduzido para outro idioma? Role a página
até o final, para ler o *Suporte* em inglês, francês ou espanhol.

* * *

### Primeiro anúncio do LibrePlanet 2020, além de um novo local!

*15 de janeiro*

A primeira palestra do [LibrePlanet 2020](https://libreplanet.org/2020/)
foi anunciada! Brewster Kahle, do <em>Hall</em>da fama da Internet, é
reconhecido como o fundador do Internet Archive, uma organização sem
fins lucrativos dedicada a preservar a história cultural da
<em>web</em>.

Mais palestrantes serão anunciados em breve, mas enquanto isso: o
LibrePlanet tem um novo local, o Centro de Eventos Back Bay de Boston!
Fizemos um grande <em>tour</em> por lá e não poderíamos estar mais
felizes com a nossa escolha. Estamos confiantes de que o Centro de
Eventos será um ótimo anfitrião para a conferência de tecnologia e
justiça social que todos viemos a conhecer e amar. É o lugar certo
para nós (e o movimento) darmos os próximos passos para libertar o
futuro. O Centro de Eventos Back Bay fica perto do transporte público
e da sede da FSF.

Para mais atualizações sobre o LibrePlanet 2020, fique de olho no
<em>site</em> da conferência, <https://libreplanet.org/2020>. E apesar
de estarmos sempre felizes em ver os participantes não inscritos, é
altamente recomendável que você [se inscreva no LibrePlanet
hoje](https://my.fsf.org/civicrm/event/info?id=87&reset=1)!

 * <https://www.fsf.org/news/first-libreplanet-2020-keynote-announcement-internet-archive-founder-brewster-kahle>
 * <https://www.fsf.org/blogs/community/libreplanet-2020-well-see-you-at-the-back-bay-events-center-in-boston-ma>

### Traga o planeta para o LibrePlanet patrocinando um participante

*8 de janeiro*

A FSF se orgulha do fato de que o público do LibrePlanet e os oradores
são oriundos de diversas origens, países e culturas. Acreditamos que
quem quiser participar ou falar na conferência não deve ser contida
por encargos financeiros; portanto, se você tem alguns dólares
sobrando, por que não [fazer uma
doação](https://my.fsf.org/civicrm/contribute/transact?reset=1&id=60&pk_campaign=frfall2019&pk_source=lpscholar)
para ajudar mais algumas pessoas a participar do LibrePlanet? Você
estará apoiando uma comunidade de Software Livre robusta e
diversificada, ajudando a reduzir a barreira financeira para quem
precisa da ajuda.

 * <https://www.fsf.org/blogs/community/bring-the-planet-to-libreplanet-by-sponsoring-an-attendee>

### O LibrePlanet 2020 precisa de você: voluntarie-se hoje!

*28 de janeiro*

O LibrePlanet está chegando em 14 e 15 de março e precisamos que VOCÊ
faça do principal encontro mundial de entusiastas de Software Livre um
sucesso. São necessários voluntários para várias tarefas diferentes no
LibrePlanet, de uma equipe de audiovisual para apontar câmeras e
ajustar microfones, a monitores de salas para introduzir os
palestrantes, a equipe de limpeza para fazer nossa conferência
aparecer e desaparecer no Centro de Eventos, e muito mais! Você pode
ser voluntário por tanto ou tão pouco tempo quanto você quiser, se
você optar por ajudar por uma hora ou duas, ou a totalidade dos dois
dias. De qualquer forma, forneceremos uma camisa MUITO bonita do
LibrePlanet 2020 do seu tamanho, além de admissão para toda a
conferência e almoço, e nossa eterna gratidão.

Se você estiver interessado em fazer parte da equipe de voluntários,
entre em contato com o nosso capitão voluntário Matt Lavallee em
<resources@fsf.org>: informe o tamanho da sua camiseta e em qual
sessão de treinamento você pode participar.

 * <https://www.fsf.org/blogs/community/libreplanet-2020-needs-you-volunteer-today>

### Chegou a hora de encerrar o processo de isenções anti-evasão do DMCA e por um fim no DRM

*22 de janeiro*

Nós apelamos mais uma vez para o fim das provisões anti-evasão das
Disposições da Lei de Direitos Autorais do Milênio Digital (DMCA, na
sigla em inglês), e o absurdo processo de isenções que o acompanha. Os
usuários têm o direito de controlar sua própria computação e o direito
de compartilhar as ferramentas que criamos com outras pessoas. O DMCA
viola esses direitos.

Não deveríamos ter que lutar pelo que já é moralmente nosso. Mas nós
vamos lutar e não vamos parar, não importa quantos obstáculos eles
lancem na nossa frente. Não podemos parar até que todos os usuários
tenham seus direitos restaurados.

 * <https://www.defectivebydesign.org/blog/it_time_end_dmca_anticircumvention_exemptions_process_and_put_stop_drm>

### Não caia na armadilha do rato: não se apaixone pela Disney+

*28 de janeiro*

Como a bruxa com seu caldeirão, os executivos da Disney estavam
inventando algo ruim quando eles estavam fabricando a Disney+. Era
preciso apenas a quantidade certa de veneno para ficar palatável: não
o suficiente para afastar todos, mas não tão pouco que os usuários
possam realmente fazer uma captura de tela do filme que eles são
assistindo. Malévola é mais do que apenas um personagem de um filme da
Disney; é um descritor adequado para o comportamento da própria Disney
quando se trata do ataque à cultura através do Gerenciamento Digital
de Restrições Digitais (DRM).

 * <https://www.defectivebydesign.org/blog/spring_mouse_trap_dont_fall_disney>

### Encontro Boston CiviCRM em busca de novo organizador

*23 de janeiro*

O encontro CiviCRM da FSF em Boston está à procura de membros da
comunidade interessados em assumir e reviver este encontro. Em uma
época, este encontro teve cerca de doze pessoas todos os meses, mas no
últimos dois ou três anos, caiu para uma a três. Nós sabemos que
existem pessoas na área de Boston trabalhando em organizações sem
fins lucrativos e que usam ou consideram usar o CiviCRM como uma
parte importante de seus trabalhos. Gostaríamos muito que elas se
reunissem, mas não temos mais tempo para organizar os encontros.

 * <https://www.fsf.org/blogs/sysadmin/civicrm-meetup-looking-for-new-organizer>

### GNU Guile 3.0.0 lançado

*16 de janeiro, por Andy Wingo*

Estamos extasiados e aliviados ao anunciar o lançamento do GNU Guile
3.0.0. Este é o primeiro lançamento da série de novas versões estáveis
3.0. Veja o comunicado de lançamento para obter detalhes completos e
um <em>link</em> para <em>download</em>.

 * <https://www.gnu.org/software/guile/news/gnu-guile-300-released.html>

### GNU Linux-libre 5.5 agora disponível; exigiu mais <em>deblobbing</em> do que o habitual

*27 de janeiro, por Michael Larabel*

Recém-lançada a versão Linux 5.5, a tripulação da Free Software
Foundation Latin America estreou o <em>downstream</em> de seu GNU
Linux-libre 5.5, que continua focado em desobstruir (<em>deblobbing
</em>) o núcleo de <em>drivers</em> que exigem <em>firmware</em>
proprietário, e remover outros códigos/funcionalidades que dependem de
<em>bits</em> de <em>software</em> não livres, e remover a capacidade
de carregar módulos do Kernel de código fechado.

 * <https://www.phoronix.com/scan.php?page=news_item&px=Linux-Libre-5.5-GNU>

### Aplicativo Ring doorbell empacotado com rastreadores de terceiros

*27 de janeiro, por Bill Budington*

O Ring não é apenas um produto que permite aos usuários espionar seus
vizinhos. A empresa também o utiliza para vigiar seus clientes. Uma
investigação do aplicativo Ring doorbell para Android, realizada pela
EFF, descobriu que ele é embalado com rastreadores de terceiros,
enviando uma infinidade de informações de identificação pessoal (PII
na sigla em inglês) dos clientes. Foi descoberto que quatro principais
empresas de análise e marketing estavam recebendo informações como
nomes, endereços IP privados, operadoras de rede móvel,
identificadores persistentes e dados de sensores nos dispositivos de
clientes pagantes.

O perigo no envio de pequenas informações é que empresas de
<em>analytics</em> e de rastreamento são capazes de combinar esses
<em>bits</em> para formar uma imagem exclusiva do dispositivo do
usuário. Esse todo coeso representa uma impressão digital que segue o
usuário enquanto ele interage com outros aplicativos e usa seu
dispositivo, essencialmente fornecendo aos rastreadores a capacidade
de espionar o que um usuário está fazendo na vida digital e quando
está fazendo. Tudo isso ocorre sem notificação ou consentimento
significativos do usuário e, na maioria dos casos, sem nenhuma maneira
de mitigar os danos causados.

Não há nada de surpreendente em nenhuma dessas notícias, mas é apenas
outro motivo para avisar seus amigos e familiares para não usarem
esses produtos!

 * <https://www.eff.org/deeplinks/2020/01/ring-doorbell-app-packed-third-party-trackers>

### Tópico no Reddit: Recentemente, fiz uma ressonância magnética total de cabeça. Usando Software Livre,consegui arrancar meu cérebro e transformá-lo em uma luz de mesa.

*24 de janeiro, por User_browser_*

Um projeto de Software Livre bacana: esse pôster do Reddit usou uma
série de programas de Software Livre para criar um modelo de seu
próprio cérebro e criou um modelo 3D macabramente delicioso que agora
funciona como uma lâmpada de mesa! Deixe-nos saber se você tentar
duplicar seus resultados -- gostaríamos de ouvir sobre as maneiras
pelas quais nossos apoiadores estão usando Software Livre de forma
criativa.

(Nós deixamos o <em>link</em> old.reddit.com como a referência abaixo
porque ele não requer rodar [JavaScript
proprietário](https://www.fsf.org/bulletin/2018/fall/escaping-the-javascript-trap-with-librejs).)


 * <https://old.reddit.com/r/3Dprinting/comments/et91oc/i_recently_had_a_full_head_mri_using_free/ffev4aq/>

### EA vai quebrar o aplicativo de telefone Tetris

*23 de janeiro, por Rob Beschizza*

Um aplicativo oficial e popular do Tetris deixará de funcionar em
abril. Usuários de Tetris Blitz iniciaram o jogo ontem para receber
uma mensagem sarcástica de "é hora de dizer adeus" da empresa. Os
usuários adoraram esta versão do jogo porque você poderia jogá-la com
uma mão, mas na era do Gestão Digital de Restrições (DRM), apenas
porque você pagou por algo não significa que você é o proprietário.

 * <https://boingboing.net/2020/01/23/ea-bricks-tetris-phone-app.html>

### Notícias de janeiro do GNU Emacs

*27 de janeiro, por Sacha Chua*

Nestas edições, kits para iniciante no Emacs sem pacotes de terceiros;
uma introdução suave à configuração do Emacs; Tema do horizonte para o
Emacs; começando com a codificação ao vivo em Python no Emacs; e mais!

 * [Notícias do Emacs de 2020-01-27](https://sachachua.com/blog/category/geek/emacs/#post-29516)
 * [Notícias do Emacs de 2020-01-20](https://sachachua.com/blog/category/geek/emacs/#post-29514)
 * [Notícias do Emacs de 2020-01-13](https://sachachua.com/blog/category/geek/emacs/#post-29513)
 * [Notícias do Emacs de 2020-01-06](https://sachachua.com/blog/category/geek/emacs/#post-29507)

### Junte-se à FSF e amigos na atualização do Diretório de Software Livre

Dezenas de milhares de pessoas visitam <https://directory.fsf.org>
todos os meses para descobrir <em>software</em> livre. Cada registro
no Diretório contém uma grande quantidade rica de informações úteis
desde categorias básicas e descrições até controle de versão, canais
de IRC, documentação e licenciamento. O Diretório de Software Livre
tem sido um ótimo recurso para usuários de <em>software</em> na última
década, mas precisa de sua ajuda para manter-se atualizado com novos e
interessantes projetos de <em>software</em> livre.

Para ajudar, participe das nossas reuniões semanais do IRC às
sextas-feiras. As reuniões acontecem no canal #fsf no irc.freenode.org
e geralmente incluem um punhado de regulares, bem como recém-chegados.
O Freenode é acessível a partir de qualquer cliente de IRC -- Todos
são bem-vindos!

A próxima reunião é sexta-feira, 7 de fevereiro, das 13h às 16h BRT
(16h à 19h UTC). Detalhes aqui:

  * <https://directory.fsf.org/wiki/Main_Page>

### Recurso em destaque do LibrePlanet: LibrePlanet:Conference/2020

Todos os meses [na wiki do
LibrePlanet](https://libreplanet.org/wiki/Main_Page), destacamos um
recurso que é útil e interessante -- geralmente um recurso que poderia
receber sua ajuda.

Para este mês, estamos destacando LibrePlanet:Conference/2020, que
fornece informações sobre a próxima conferência do LibrePlanet, que
vai acontecer aqui em Boston em 14-15 de março! Esta página está aqui
para ajudar você a facilitar reuniões com outros apoiadores de
<em>software</em> livre na conferência, incluindo jantares temáticos.
Você está convidado a adotar, espalhar e melhorar este importante
recurso.

  * <https://libreplanet.org/wiki/LibrePlanet:Conference/2020>

Você tem uma sugestão para o recurso em destaque do próximo mês?
Nos avise em <campaigns@fsf.org>.

### Holofote GNU com Mike Gerwitz: 16 novos lançamentos GNU!

16 novos lançamentos GNU neste último mês (até 26 de janeiro de
2020):

* [bison-3.5.1](https://www.gnu.org/software/bison/)
* [gmp-6.2.0](https://www.gnu.org/software/gmp/)
* [gnuhealth-3.6.2](https://www.gnu.org/software/health/)
* [gnunet-0.12.2](https://www.gnu.org/software/gnunet/)
* [grep-3.4](https://www.gnu.org/software/grep/)
* [gsasl-1.8.1](https://www.gnu.org/software/gsasl/)
* [guile-3.0.0](https://www.gnu.org/software/guile/)
* [help2man-1.47.12](https://www.gnu.org/software/help2man/)
* [hyperbole-7.0.8](https://www.gnu.org/software/hyperbole/)
* [kawa-3.1.1](https://www.gnu.org/software/kawa/)
* [libredwg-0.10.1](https://www.gnu.org/software/libredwg/)
* [make-4.3](https://www.gnu.org/software/make/)
* [mes-0.22](https://www.gnu.org/software/mes/)
* [parallel-20200122](https://www.gnu.org/software/parallel/)
* [sed-4.8](https://www.gnu.org/software/sed/)
* [unifont-12.1.04](https://www.gnu.org/software/unifont/)

Para anúncios da maioria dos novos lançamentos GNU, inscreva-se
na lista de discussão info-gnu:
<https://lists.gnu.org/mailman/listinfo/info-gnu>.

Para baixar: quase todo <em>software</em> GNU está disponível em
<https://ftp.gnu.org/gnu/>, ou, de preferência, em um dos espelhos em
<https://www.gnu.org/prep/ftp.html>. Você pode usar a URL
<https://ftpmirror.gnu.org/> para ser redirecionado automaticamente
para um espelho próximo e atualizado.

Vários pacotes GNU, assim como o sistema operacional GNU como um
todo, estão procurando mantenedores e outras formas de auxílio: veja
<https://www.gnu.org/server/takeaction.html#unmaint> se você quiser
ajudar. A página geral sobre como ajudar o GNU está em
<https://www.gnu.org/help/help.html>.

Se você tem um programa que funcionando, totalmente ou parcialmente,
que gostaria de oferecer ao projeto GNU como um pacote GNU, veja
<https://www.gnu.org/help/evaluation.html>.

Como sempre, sinta-se à vontade para escrever para nós em
<maintainers@gnu.org> com quaisquer perguntas sobre o GNU
ou sugestões para futuras edições.

### Próximos eventos da FSF e do Software Livre

 * 3 de fevereiro, 2020, Bruxelas, Bélgica, [International Copyleft Conference](https://2020.copyleftconf.org/)
 * 14-15 de março, 2020, Boston, MA, [LibrePlanet 2020: Free the Future](https://www.fsf.org/events/libreplanet-2020-march14-15-registration-open)
 * 22-28 de julho, 2020, Zacatecas, México, [GUADEC 2020](https://www.gnome.org/news/2019/11/guadec-2020-announcement/)

### Agradecimentos do GNU!

Agradecemos a todos que doam à Free Software Foundation e gostaríamos
de dar um reconhecimento especial às pessoas que doaram US$ 500,00 ou
mais no último mês.

  * <https://www.gnu.org/thankgnus/2020supporters.html>

Neste mês, um grande Agradecimento do GNU para:

* Adam Lewis
* Alex Boulette
* Alexandre BLANC
* Alison Chaiken
* Andrew Tosh 
* Arcanite Solutions
* Brewster Kahle
* Brian Strand
* Catena Cyber
* Conan Chiles
* Dominic Walden
* Georges Sancosme 
* Guillaume REMBERT
* Guus Sliepen
* Harry Mangalam 
* Inouye Satoru
* Jason Heise 
* Jean-Francois Blavier 
* Jerome Quinn
* John McFarland
* John Owen
* Julia Kreger
* Kevin McCarthy
* Leslie Hawthorn
* Li-Cheng Tai 
* Lincoln Clarete
* Luke Shumaker 
* Marcus Pemer 
* Mario Habdija
* Mark Harris
* Martin Jansche
* Masaru KIMURA
* Michael Cornelius 
* Michael HENDERSON
* Michael Mauger 
* Mikhail Pomaznoy
* Nathan Pooley 
* Orlando Wingbrant
* Pablo Otero 
* Paul Allen
* Peter Kunze 
* Philipp Weis
* Puduvankunnil Udayakumar 
* Richard Harlow
* Stefan Maric
* Steve Wickert
* Terence O'Gorman 
* Thomas Saglio
* Tim Wilson
* Tony Chidester
* William Bolella
* Yidong Chong

Você pode adicionar seu nome a essa lista doando em
<https://donate.fsf.org/>.

### Contribuições para o copyright do GNU

Atribuir seus direitos autorais (_copyright_) à Free Software
Foundation nos ajuda a defender a GPL e manter o <em>software</em>
livre. As seguintes pessoas atribuíram seus direitos autorais à FSF
(e apreciação públicas) no mês passado:

* Jean-Christophe Helary (Texinfo)
* Justin Timmons (Emacs)
* Klaus Weiss (Emacs)
* Ladislav Michl (GNUstep)
* May Shao (glibc)
* Michael Piscopo (GNU Radio)
* Valerii Zapodovnikov (GNU Radio)
* Vincent Imbimbo (Bison)

Deseja ver seu nome nesta lista? Contribua para o GNU e atribua seus
direitos autorais à FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Traduções do *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2020/febrero>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2020/fevrier>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Free Software Supporter is available in English. To see the English
version, click here:
<https://www.fsf.org/free-software-supporter/2020/february>

**To change the user's preferences and receive the next issues of
  Supporter in English, click here:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Entre em ação com a FSF!

Contribuições de milhares de membros individuais possibilitam trabalho
da FSF. Você pode contribuir unindo-se em <https://my.fsf.org/join>.
Se você já é um membro, você pode ajudar a indicar novos membros
(e ganhar algumas recompensas) adicionando uma linha com seu número
de membro à sua assinatura de e-mail como:

  Sou membro da FSF -- ajude-nos a apoiar a liberdade de
  <em>software</em> <https://my.fsf.org/join>

ou em inglês:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

A FSF está sempre procurando voluntários
(<https://www.fsf.org/volunteer>). Da agitação ao hacking, da
coordenação de problemas ao preenchimento de envelopes -- há algo
aqui para todo mundo fazer. Além disso, confira a nossa seção de
campanhas (<https://www.fsf.org/campaigns>) e entre em ação contra
patentes de <em>software</em>, Gestão Digital de Restrições (DRM),
adoção de <em>software</em> livre, OpenDocument e mais.


###

Copyright © 2020 Free Software Foundation, Inc.

Esta obra está licenciada sob uma licença Creative Commons
Atribuição 4.0 Não Adaptada. Para ver uma cópia desta licença, visite
<https://creativecommons.org/licenses/by/4.0/deed.pt_BR>.

