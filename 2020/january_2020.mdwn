# Free Software Supporter
Issue 141, January 2020

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 218,883 other activists. That's 2,156 more than last month!

<a href="https://www.fsf.org/resources/badges"><img src="https://static.fsf.org/nosvn/appeal2019/allimages2.png" width="600" style="margin-top:1px;"></a> 

### The FSF can't campaign for free software without your help 

The Free Software Foundation's (FSF) campaigns team works tirelessly
to spread the message to the world that all software must be
free. Campaigning for free software takes creativity, hard work, and a
dedicated community. We amplify free software organizations and
projects, mobilize activists, and provide resources. We're only three
people, but we are the point of connection for hundreds of thousands
of supporters annually. You hold the power to exponentially increase
this number. It is our goal to make the free software conversation a
kitchen table issue, and we need your help!

To ring in 2020, we're extending our offer for an exclusive gift to
all new or renewing annual associate members until **January
17th**. Please help us to reach our goal of 600 new members by
starting out the new year with a [new FSF
membership](https://my.fsf.org/join?pk_campaign=frfall2019&pk_source=decsupporter),
share our promotional images to start conversations about free
software, and urge your friends and family to join!

 * <https://www.fsf.org/blogs/community/the-fsf-cant-campaign-for-free-software-without-your-help>
 * <https://www.fsf.org/blogs/community/last-chance-to-help-us-reach-our-membership-goal-in-2019>

## TABLE OF CONTENTS

* Bringing the free software vision to 2020 
* Presenting: *ShoeTool* -- Happy Holidays from the FSF 
* The FSF tech team: Doing more for free software 
* Support FSF's copyleft and licensing work 
* Defective by Design: A resistance to restrictions 
* Replicant needs your help to liberate Android in 2020 
* LibrePlanet: Not just a conference, but a network 
* What's new in the GNU Press Shop 
* Setting the right example: Say no to the Elf on the Shelf 
* At SeaGL 2019, free software was in fine feather 
* Raleigh, North Carolina: good BBQ and great outreach for free software knowledge
* "The Rise of Skywalker" is a preview of our DRM-fueled dystopian future
* Victory: Brookline, Massachusetts votes to ban face surveillance
* Abbott Labs kills free tool that lets you own the blood-sugar data from your glucose monitor, saying it violates copyright law
* Colleges are turning students’ phones into surveillance machines, tracking the locations of hundreds of thousands
* Schools spy on kids to "prevent shootings," but there's no evidence it works
* Oregon FBI warns community to secure "smart" TVs
* Thunderbird, Enigmail and OpenPGP
* GNU Radio Hackfest at ESA in the Netherlands and at FOSDEM ’20
* December GNU Emacs news
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Group: Guix/FOSDEM2020
* GNU Spotlight with Mike Gerwitz: 14 new GNU releases!
* FSF and other free software events
* Thank GNUs!
* GNU copyright contributions
* Translations of the *Free Software Supporter*
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2020/january>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

Want to read this newsletter translated into another language? Scroll
to the end to read the *Supporter* in French, Spanish, or Portuguese.

***

### Bringing the free software vision to 2020 

*From December 27th*

2019 has been an eye-opening, transformative year for free software
and the Free Software Foundation (FSF), bringing some major changes
both internally and in the world around us. As we navigate these
changes, we are guided by the FSF's founding vision -- the four
freedoms that define free software, and our mission to make all
software be compatible with human freedom. It must be honest,
transparent, and shareable, and it must truly work in service of its
users.

 * <https://www.fsf.org/blogs/community/bringing-the-free-software-vision-to-2020>

### Presenting: *ShoeTool* -- Happy Holidays from the FSF 

*From December 23rd*

ShoeTool is an animated fairy tale about an elf shoemaker who thinks
he buys a machine to help him make shoes... only to find out that
there are strings attached to his "purchase." Please show your support
for free software and this video by promoting it on your social media
using the #shoetool hashtag!

Software restrictions, analogous to the kinds of restrictions our main
character Wendell runs into as a user of the promising ShoeTool, are
detrimental to our freedom, creativity, and jobs. We hope watching
Wendell's frustrations will shake things up in many homes and help
more people understand.

 * <https://www.fsf.org/blogs/community/presenting-shoetool-happy-holidays-from-the-fsf>

### The FSF tech team: Doing more for free software 

*From December 4th*

The Free Software Foundation (FSF) tech team works everyday to
maintain and improve the infrastructure that supports hundreds of free
software projects, along with the FSF itself, in its mission to create
a world where all software respects our freedom and dignity. We don't
outsource any of our daily software needs because we need to be sure
that they are done using only free software. Remember, there is no
"cloud," just other people's computers. For example: we don't
outsource our email, so every day we send over half a million messages
to thousands of free software hackers through the community mailing
lists we host. We also don't outsource our Web storage or networking,
so we serve tens of thousands of free software downloads -- over 1.5
terabytes of data -- a day. And our popularity, and the critical
nature of the resources we make available, make us a target for denial
of service attacks (one is ongoing as we write this), requiring
constant monitoring by the tech team, whose members take turns being
ready for emergency work so that the resources our supporters depend
on stay available.

 * <https://www.fsf.org/blogs/sysadmin/the-fsf-tech-team-doing-more-for-free-software>

### Support FSF's copyleft and licensing work 

*From December 13th*

The Free Software Foundation is a global leader for copyleft, and the
licensing team plays a vital role in disseminating useful knowledge
about free software while working to protect it. We accomplish this in
part by answering licensing questions from the public and by providing
resources like our list of free software licenses. We also increase
access to software freedom by managing the Respects Your Freedom
certification program, and cataloging free software through our
endorsed distributions program and the Free Software Directory. To
protect free software, we handle license compliance for the GNU
Project, resulting in a stronger community and more respect for the
power of copyleft.

We are proud to accomplish this as just two staff working with our
executive director, board, and legal counsel. These resources combined
make a potent force for software freedom, and your support will ensure
our work continues with the aim to do an even better job in 2020. Let
us share a bit about the work we did in 2019 and elaborate on why it
is so vital that this work continues.

 * <https://www.fsf.org/blogs/licensing/support-fsfs-copyleft-and-licensing-work>

### Defective by Design: A resistance to restrictions 

*From December 17th*

The FSF keeps a close eye on the headlines for threats to user freedom
coming from many different fronts, such as the way [Digital
Restrictions Management
(DRM)](https://www.defectivebydesign.org/what_is_drm_digital_restrictions_management)
impedes an individual's right to control their computers and
devices. The Defective by Design campaign is a place for us to
transform our digital dissent into in-person actions, canvassing, and
effective protests. This is a report-back on this campaign's progress
over the course of 2019.

 * <https://www.fsf.org/blogs/community/defective-by-design-a-resistance-to-restrictions>

### Replicant needs your help to liberate Android in 2020 

*From December 10th*

The Free Software Foundation (FSF) supports the work of several
important free software projects through fiscal sponsorship in a
program we call Working Together for Free Software. Because of the
ubiquity of mobile phone use in modern life, freeing these small
computers is an important task, which is why it's so crucial to
support the Replicant project, a free software mobile operating system
that emphasizes freedom, privacy, and security.

 * <https://www.fsf.org/blogs/community/replicant-needs-your-help-to-liberate-android-in-2020>

### LibrePlanet: Not just a conference, but a network 

*From December 18th*

Since 2010, the LibrePlanet wiki has provided a space for connection
between free software activists, with the following mission statement:
"To empower a global network of both local and project-based teams,
all working together to advance free software as a social movement for
user freedom."

This idea of participation and space for connection between free
software activists is what motivates both the wiki and the Free
Software Foundation's (FSF) yearly conference by the same name. Our
LibrePlanet wiki uses the same software as what powers Wikipedia,
which makes it the perfect tool for global collaborative work. We use
the wiki as a tool to help organize free software supporters all over
the world, so everyone can collaborate around their projects and
ideas, as well as communicate with each other using our mailing lists
for discussion and development, and our #LibrePlanet IRC channel on
Freenode.

 * <https://www.fsf.org/blogs/community/libreplanet-not-just-a-conference-but-a-network>

### What's new in the GNU Press Shop 

*From December 5th*

Greetings from the GNU Press Shop! This is an update on what's new and
exciting at the Free Software Foundation's (FSF) online store, your
source for GNU apparel, programming manuals, and more. We know at this
time of year you'll be looking for GNU gifts for your favorite free
software enthusiasts, and we have some terrific new picks for you to
choose from, including a long-awaited zip-up GNU hoodie, a new color
variation for the classic GNU head T-shirt, and more.

 * <https://www.fsf.org/blogs/gnu-press/whats-new-in-the-gnu-press-shop>

### Setting the right example: Say no to the Elf on the Shelf 

*From December 23rd*

As noted by writer Matt Beard in [*The
Guardian*](https://www.theguardian.com/commentisfree/2019/dec/15/elf-on-the-shelf-is-a-fun-and-festive-way-to-teach-your-child-to-submit-to-the-surveillance-state),
the latest iteration of teaching kids to accept [constant
surveillance](https://www.fsf.org/campaigns/surveillance) via holiday
tradition is the [Elf on the
Shelf](https://en.wikipedia.org/wiki/The_Elf_on_the_Shelf), a cheerful
little snitch whom parents hide in different spots every day in the
house. The idea is, the Elf watches what kids are up to, and if they
call their little sister a name or steal a cookie from the cookie jar,
the friendly household spy will tattle to Santa, who will add them to
the "naughty" list. Beware! We agree with Beard that this cutesy,
innocent-seeming "tradition" (which actually only dates back to 2005!)
communicates to children that someone is always watching them, and
that moreover, this is a perfectly normal thing. This should give us
pause, and cause us to think carefully about what kind of messages we
are sending in our behavior at home and with friends.

 * <https://www.fsf.org/blogs/community/setting-the-right-example-say-no-to-the-elf-on-the-shelf>

### At SeaGL 2019, free software was in fine feather 

*From December 9th*

While the satisfactions of software freedom are quite enjoyable on
your own, some of the greatest joys of free software come from our
opportunities to flock together with other members of our community:
to collaborate on our work, teach new skills, or simply show off new
achievements. A grassroots gathering like the Seattle GNU/Linux
Conference (SeaGL) is fun because it’s so thoroughly participatory:
everyone comes into the room with something they’re excited to tell
you about, and they’re equally excited to hear what you’re working
on. The people at the front of the room giving a keynote talk are just
as likely to be sitting next to you in the next session, so you can
tell them what you thought of their talk, and even find out how to
participate in their projects!

 * <https://www.fsf.org/blogs/community/at-seagl-2019-free-software-was-in-fine-feather>

### Raleigh, North Carolina: good BBQ and great outreach for free software knowledge

*From December 20th*

Although we have been really busy, we didn’t want to miss the chance
to share some specifics about our activities in October. That month,
members of our licensing and campaigns teams headed down to North
Carolina to spread the message of software freedom. First, on the 14th
& 15th, the Free Software Foundation (FSF) staffed a booth at the [ATO
conference](https://allthingsopen.org/) where we reminded hundreds of
people that freedom is better than just being open. Next, on October
16th, our licensing and compliance team held another [Continuing Legal
Education (CLE)
seminar](https://www.fsf.org/events/cle-seminar-20191016-raleigh).

 * <https://www.fsf.org/blogs/licensing/raleigh-north-carolina-good-bbq-and-great-outreach-for-free-software-knowledge> 

### "The Rise of Skywalker" is a preview of our DRM-fueled dystopian future

*From December 23rd by Jason Koebler*

Don't read this article if you're worried about spoilers! With that
said: it's wildly ironic that Disney, one of the most notorious
purveyors of Digital Restrictions Management (DRM), didn't seem to
notice that a major plot point in their new *Star Wars* film involves
a software lock that looks a lot like DRM. And the result is dire,
involving the destruction of *an entire planet*. Perhaps because they
couldn’t get these basic user rights enshrined a long time ago in a
galaxy far, far, away, we are increasingly dealing with this exact
problem here on Earth.

 * <https://www.vice.com/en_us/article/7kz9zg/the-rise-of-skywalker-is-a-preview-of-our-drm-fueled-dystopian-futur>

### Victory: Brookline, Massachusetts votes to ban face surveillance

*From December 20th by Nathan Sheard*

With the passage of Article 25 on December 11, our neighboring town of
Brookline, Massachusetts, became the fifth municipality in the nation
to ban its government agencies from using face surveillance, and the
second in Metro Boston, after Somerville. Face surveillance exists on
the same continuum of unacceptable invasions of privacy as [bulk
surveillance](https://www.fsf.org/blogs/community/free-software-fighting-back-against-bulk-surveillance),
and we applaud any progress against these abuses.

 * <https://www.eff.org/deeplinks/2019/12/victory-brookline-votes-ban-face-surveillance>

### Abbott Labs kills free tool that lets you own the blood-sugar data from your glucose monitor, saying it violates copyright law

*From December 12th by Cory Doctorow*

Diabettech is a hub that helps people with diabetes manage their
health, including by building "artificial pancreases," enabling them
to maintain vastly better control over their blood-sugar levels. When
Diabettech posted technical instructions and code to enable users to
extract their own data from the ironically named Abbott Labs Freestyle
Libre glucose monitor, however, Abbott Labs used US copyright law to
have the project deleted from GitHub. Abbott Labs maintains that the
project violates Section 1201 of the [Digital Millennium Copyright Act
(DMCA)](https://www.defectivebydesign.org/blog/2018_dmca_anticircumvention_exemption_process_some_progress_not_enough),
which prohibits bypassing "access controls" for copyrighted works. In
this case, though, the "copyrighted work" is data about your own
blood!

 * <https://boingboing.net/2019/12/12/they-literally-own-you.html>

### Colleges are turning students’ phones into surveillance machines, tracking the locations of hundreds of thousands

*From December 24th by Drew Harwell*

When Syracuse University freshmen walk into professor Jeff Rubin’s
"Introduction to Information Technologies" class, seven small
Bluetooth beacons hidden around the Grant Auditorium lecture hall
connect with an app on their smartphones and boost their “attendance
points.”

And when they skip class? The SpotterEDU app sees that, too, logging
their absence into a campus database that tracks them over time and
can sink their grade. It also alerts Rubin, who later contacts
students to ask where they’ve been. His 340-person lecture has never
been so full.

These systems represent a new low in intrusive technology,
infantilizing young adults in the precise place where they're expected
to take on adult responsibility, and continuing the training they
received, starting with the aforementioned Elf on the Shelf, to living
in a giant panopticon.

 * <https://www.washingtonpost.com/technology/2019/12/24/colleges-are-turning-students-phones-into-surveillance-machines-tracking-locations-hundreds-thousands/>

### Schools spy on kids to "prevent shootings," but there's no evidence it works

*From December 4th by Todd Feathers*

Spyware like GoGuardian, Bark and Gaggle are monitoring students'
Internet habits, both on and off school grounds. The complete lack of
independent research backing the claims these companies make while
they harvest kids' browsing history, social media activity, location,
and even keystrokes is alarming. Thankfully, kids and parents are
starting to fight back, and we encourage you to find out if these
programs are being used at your child's school and demand that they
respect your child's privacy instead.

 * <https://www.vice.com/en_uk/article/8xwze4/schools-are-using-spyware-to-prevent-shootingsbut-theres-no-evidence-it-works>

### Oregon FBI warns community to secure "smart" TVs

*From November 26th by Beth Anne Steele*

"Smart" TVs are smart enough to use facial recognition and spy on your
habits, but they aren't smart enough to respect your freedom. When
even the FBI is cautioning you to "consider whether you are willing to
take the risk," you know you've got a problem.

 * <https://www.fbi.gov/contact-us/field-offices/portland/news/press-releases/tech-tuesdaysmart-tvs/?=portland-field-office>

### Thunderbird, Enigmail and OpenPGP

*From October 8th by Ryan Sipes*

The Thunderbird project is happy to announce that for the future
Thunderbird 78 release, planned for summer 2020, they will add
built-in functionality for email encryption and digital signatures
using the OpenPGP standard. This new functionality will replace the
Enigmail add-on, which will continue to be supported until Thunderbird
68 end of life, in the Fall of 2020.

 * <https://blog.thunderbird.net/2019/10/thunderbird-enigmail-and-openpgp/>

### GNU Radio Hackfest at ESA in the Netherlands and at FOSDEM ’20

*From December 20 by FOSDEM*

Just as the year closes, it’s time to remind people that two fantastic
events are going to take place at the end of January / the beginning
of February: FOSDEM 2020 will be taking place at the ULB campus in
Brussels, Belgium, and just before the conference, the GNU Radio
Hackfest will be taking place from January 28-30 in Noordwijk. This is
a limited headcount event, so get your [free
tickets](https://tickets.gnuradio.org/hackfest2001/) today!

 * <https://www.gnuradio.org/news/2019-12-19-fosdem/>

### December GNU Emacs news

*From December 23rd by Sacha Chua*

In these issues: Getting started on Doom Emacs, an enjoyable "test
yourself" challenge, dealing with expired ELPA GPG keys, configuring
Emacs from scratch, and more!

 * [2019-12-23 Emacs news](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29505)
 * [2019-12-16 Emacs news](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29504)
 * [2019-12-09 Emacs news](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29503)
 * [2019-12-02 Emacs news](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29501)

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, January 3rd, from 12pm to 3pm EDT (16:00
to 19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: Group: Guix/FOSDEM2020

Every month on [the LibrePlanet
wiki](https://libreplanet.org/wiki/Main_Page), we highlight one
resource that is interesting and useful -- often one that could use
your help.

For this month, we are highlighting Guix/FOSDEM2020, which provides
information about the annual GNU Guix (un)conference, happening on
January 30-31, 2020, two days before the FOSDEM conference in
Brussels, Belgium. You are invited to adopt, spread and improve this
important resource.

  * <https://libreplanet.org/wiki/Group:Guix/FOSDEM2020>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 14 new GNU releases!

14 new GNU releases in the last month (as of December 27, 2019):

* [artanis-0.4.1](https://www.gnu.org/software/artanis/)
* [bison-3.5](https://www.gnu.org/software/bison/)
* [gama-2.08](https://www.gnu.org/software/gama/)
* [global-6.6.4](https://www.gnu.org/software/global/)
* [gnuhealth-3.6.5](https://www.gnu.org/software/health/)
* [gnunet-0.12.1](https://www.gnu.org/software/gnunet/)
* [gnupg-2.2.19](https://www.gnu.org/software/gnupg/)
* [libcdio-10.2+2.0.1](https://www.gnu.org/software/libcdio/)
* [libmicrohttpd-0.9.69](https://www.gnu.org/software/libmicrohttpd/)
* [libredwg-0.9.3](https://www.gnu.org/software/libredwg/)
* [nano-4.7](https://www.gnu.org/software/nano/)
* [parallel-20191222](https://www.gnu.org/software/parallel/)
* [taler-0.6.0](https://www.gnu.org/software/taler/)
* [vc-dwim-1.9](https://www.gnu.org/software/vc-dwim/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>.  You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

This month, we welcome Martin Schanzenbach as comaintainer of GNUnet.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help.  The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### FSF and other free software events

 * February 1-2, 2020, Brussels, Belgium, [FOSDEM '20](https://fosdem.org/2020/)
 * February 3, 2020, Brussels, Belgium, [International Copyleft Conference](https://2020.copyleftconf.org/)
 * March 14-15, 2020, Boston, MA, [LibrePlanet 2020: Free the Future](https://www.fsf.org/events/libreplanet-2020-march14-15-registration-open)
 * July 22-28, Zacatecas, Mexico, [GUADEC 2020](https://www.gnome.org/news/2019/11/guadec-2020-announcement/)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2020supporters.html>

This month, a big Thank GNU to:

* Aaron Grothe 
* Andrew Khosravian 
* Antonio Carzaniga
* Boone Gorges 
* Daniel Gillmor 
* Daniel Riek
* Dara Adib
* David Turner
* Dock Williams 
* Dominic Walden
* Donald Craig
* Etienne Grossmann 
* Iñaki Arenaza 
* Jelte van der Hoek 
* Jonathan Howell
* Josh Ventura
* Mark Boenke 
* Morten Lind 
* Olivier Warin 
* René Genz
* REZWANUZZAMAN CHOWDHURY
* Russell McManus
* Shyama Mandal 
* Simon Josefsson
* Stephen Longfield
* Thomas Saglio

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF (and allowed public
appreciation) in the past month:

* Barry Duggan (GNU Radio)
* László Várady (Bison)
* Sergey Belyashov (binutils)
* Tim Ruehsen (glibc, GCC, binutils)
* Tom Gillespie (Emacs)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Translations of the *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2020/enero>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2020/janvier>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2020/janeiro>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something here
for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software patents,
Digital Restrictions Management (DRM), free software adoption,
OpenDocument, and more.

###

Copyright © 2020 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

