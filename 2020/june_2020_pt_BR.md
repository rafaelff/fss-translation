# Free Software Supporter
Issue 146, June 2020

*Read and share online:
<https://www.fsf.org/free-software-supporter/2020/june>*

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 227,711 other activists. That's 315 more than last month!

### FSF gives freedom-respecting videoconferencing to all associate members

*From May 28th*

The Free Software Foundation (FSF) is now offering all FSF associate
members free "as in freedom" videoconferencing via an exclusive FSF
Jitsi Meet instance as an additional [associate member
benefit](https://www.fsf.org/associate/benefits). In order to be able
to provide a sustainable and reliable service, we are offering the
ability to create conversations on the server exclusively to
[associate members](https://my.fsf.org/join). Members can create a
channel using their member credentials, but then any person or group
can participate in the conversation. Nonmembers can be invited, but
cannot start a channel.

**[Information about how to use the FSF videoconferencing instance for
associate
members](https://www.fsf.org/associate/about-the-fsf-jitsi-meet-server)**

This is just one of many efforts we've made in the past months to push
back against increased societal pressure to use nonfree software to
communicate with collaborators, friends, and loved ones during the
COVID-19 pandemic, and after.

 * <https://www.fsf.org/blogs/community/fsf-gives-freedom-respecting-videoconferencing-to-all-associate-members>
 * <https://www.fsf.org/news/free-software-foundation-announces-freedom-respecting-videoconferencing-for-its-associate-members>

## TABLE OF CONTENTS

* Don’t miss your chance to win fabulous prizes: Get your friends to join the FSF!
* Remote education does not require giving up rights to freedom and privacy
* A roundup of recent updates to our licensing materials: November 2019 to April 2020
* Microsoft Build: Same old recycled stuff, no upcycling
* A new way to enjoy LibrePlanet 2020 sessions: Podcast format
* FSFE nudges emergency ventilator project towards a free software license
* Apple whistleblower goes public over "lack of action"
* Introducing Amin Bandali, intern with the FSF tech team
* Patent case against GNOME resolved (and more GNOME news)
* MediaGoblin 0.10.0 released
* Introducing Inkscape 1.0
* This free software collective is taking Malayalam computing to the next level
* GCC 10.1 released
* SeaGL going virtual due to COVID-19 aka novel coronavirus
* HOPE 2020 will be an online event: Call for sessions open
* May GNU Emacs news
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: Activism Guide
* GNU Spotlight with Mike Gerwitz: 12 new GNU releases!
* FSF and other free software events
* Thank GNUs!
* GNU copyright contributions
* Translations of the *Free Software Supporter*
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2020/june>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

Want to read this newsletter translated into another language? Scroll
to the end to read the *Supporter* in French, Spanish, or Portuguese.

***

### Don’t miss your chance to win fabulous prizes: Get your friends to join the FSF!

*From May 26th*

The LibrePlanet 2020 Virtual Raffle has been extended to June 7th! In
order for you to qualify to win a prize, new members have to sign up
using your referral link. You will find your personal referrer link on
the dashboard after logging in at <https://my.fsf.org/>. To see the
prize list, and find out how many referrers you need for each prize,
check out our original announcement of the raffle at
<https://www.fsf.org/blogs/community/virtual-libreplanet-raffle-encourage-others-to-join-fsf-and-win-prizes>.

 * <https://www.fsf.org/blogs/community/don2019t-miss-your-chance-to-win-fabulous-prizes-get-your-friends-to-join-the-fsf>

### Remote education does not require giving up rights to freedom and privacy

*From May 14th*

The [increased
use](https://www.bizjournals.com/birmingham/news/2020/04/01/birmingham-tech-firm-sees-surge-in-hires-inbound.html)
of proprietary test-administering software and other proprietary
educational software is a dangerous development, both because of the
software's proprietary nature, and because of its inherent purpose of
exposing a student's, or in some cases a family's, data to the
proctor. In schemes like these, the user ends up sacrificing both
personal information and biometric data. Because the software is
proprietary, there's no possibility of understanding how it works --
besides leaking personal data, it could also create security concerns
or deliver bad quality tests (and results). Requiring students to cede
control over their entire computer to a test proctoring company is
fundamentally unjust. Worse, we cannot be sure that any of these
nonfree software dependencies and their accompanying surveillance
techniques will be rolled back after social distancing guidelines are
no longer enforced.

It is important that decisions made in the education sector are first
and foremost ethically motivated. Here at the FSF, we have started a
free communications working group. Initiatives include a [remote
communication email
list](https://lists.libreplanet.org/mailman/listinfo/remotecommunication),
as well as a [collaborative resource
page](https://libreplanet.org/wiki/Remote_Communication) for
documenting and sharing free communication tools to help spread
awareness of the ethical choices that can be made. We have also been
assisting educational professionals in offering their classes online
using only free software. And we have been reading many stories about
activism in education from the larger community, and want to share
those with you. They have inspired and motivated us. We need more
people like this around the world to be vocal and critical about
infringements on user freedom in the area of remote learning.

 * <https://www.fsf.org/blogs/community/remote-education-does-not-require-giving-up-rights-to-freedom-and-privacy>

### A roundup of recent updates to our licensing materials: November 2019 to April 2020

*From May 7th*

We recently added a new license to our our list of Various Licenses
and Comments about Them, as well as a few other minor updates to that
page. We also revamped our materials on seminars on free software
licensing and GPL compliance. What follows is a brief rundown of those
changes.

 * <https://www.fsf.org/blogs/licensing/a-roundup-of-recent-updates-to-our-licensing-materials-november-2019-to-april-2020>

### Microsoft Build: Same old recycled stuff, no upcycling

*From May 21st*

Often, a proprietary software company's silence can speak as loudly as
their latest campaign against a computer user's right to freedom. This
is the case with Microsoft's developer-centric "Build" event. While
Microsoft announced a few more welcome additions to its free software
output, it missed the opportunity to demonstrate a real commitment to
user freedom by upcycling its recently abandoned Windows 7 operating
system under a free software license.

 * <https://www.fsf.org/blogs/community/microsoft-build-same-old-recycled-stuff-no-upcycling>

### A new way to enjoy LibrePlanet 2020 sessions: Podcast format

*From May 8th*

Looking for some audio entertainment to get you through a slow
afternoon, or to accompany you on a walk through the park? LibrePlanet
2020: Free the Future sessions are now available as audio files! We
have uploaded them in conjunction with an RSS feed you can import into
your favorite podcasting app or RSS reader, enabling you to discover
new talks and catch all of the ones that you might have missed using a
free podcast app like AntennaPod via Android, or gPodder, if you are
on your desktop computer.

 * <https://www.fsf.org/blogs/community/libreplanet-2020-audio-now-available-online>

### FSFE nudges emergency ventilator project towards a free software license

*From May 14th by Nico Rikken*

After a nudge by the FSFE, the Dutch OpenAIR initiative has provided
licenses on their material to support reuse.

In the Netherlands, the OperationAIR initiative was started to cope
with COVID-19 by developing an easily producible emergency ventilator
for which parts could mainly be sourced locally. This project was
started on March 16 by Professor Jaap Harlaar and students of the
Department of BioMechanical Engineering of Delft Technical University
in order to ensure enough ventilator capacity for treating COVID-19
patients. The team intended their design to be publicly available for
reuse. All documentation, technical design, and source code was
published in a coherent fashion on their Web site.

 * <https://fsfe.org/news/2020/news-20200514-01.en.html>

### Apple whistleblower goes public over "lack of action"

*From May 20th by Alex Hern*

A former Apple contractor who helped blow the whistle on the company’s
program to listen to users’ Siri recordings has decided to go public,
in protest at the lack of action taken as a result of the disclosures.

In a letter announcing his decision, sent to all European data
protection regulators, Thomas le Bonniec said: “It is worrying that
Apple (and undoubtedly not just Apple) keeps ignoring and violating
fundamental rights and continues their massive collection of data."

“I am extremely concerned that big tech companies are basically
wiretapping entire populations despite European citizens being told
the EU has one of the strongest data protection laws in the
world. Passing a law is not good enough: it needs to be enforced upon
privacy offenders.”

Continual, flagrant privacy violations [are far from the only reason
to avoid Apple
products](https://www.fsf.org/bulletin/2019/spring/its-not-just-about-privacy):
read more about how Apple routinely tramples user rights at
<https://www.fsf.org/blogs/community/apple-app-store-anniversary-marks-ten-years-of-proprietary-appsploitation>.

 * <https://www.theguardian.com/technology/2020/may/20/apple-whistleblower-goes-public-over-lack-of-action>

### Introducing Amin Bandali, intern with the FSF tech team

*From May 29th*

Hi there, I'm Amin Bandali, often just bandali on the interwebs. I
wear a few different hats around GNU as a maintainer, Web master, and
Savannah hacker, and I'm very excited to be extending that to the Free
Software Foundation (FSF) as an intern with the FSF tech team for
spring 2020.

 * <https://www.fsf.org/blogs/sysadmin/introducing-bandali-intern-with-the-fsf-tech-team>

### Patent case against GNOME resolved (and more GNOME news)

*From May 20th by the GNOME Foundation*

The GNOME Foundation, Rothschild Patent Imaging, and Leigh
M. Rothschild are pleased to announce that the patent dispute between
Rothschild Patent Imaging and GNOME has been settled.

In this walk-away settlement, GNOME receives a release and covenant
not to be sued for any patent held by Rothschild Patent
Imaging. Further, both Rothschild Patent Imaging and Leigh Rothschild
are granting a release and covenant to any software that is released
under an existing Open Source Initiative approved license (and
subsequent versions thereof), including for the entire Rothschild
portfolio of patents, to the extent such software forms a material
part of the infringement allegation.

Neil McGovern, executive director for the GNOME Foundation said “I’m
exceptionally pleased that we have concluded this case. This will
allow us to refocus our attention on creating a free software desktop,
and will ensure certainty for all [free] software in [the] future.”

It's been a big few months for GNOME, and there are several exciting
initiatives afoot, including funding for a new campaign in
Africa. GNOME also welcomed their Google Summer of Code students,
including [Free Software Award
winner](https://www.fsf.org/news/lets-encrypt-jim-meyering-and-clarissa-lima-borges-receive-fsfs-2019-free-software-awards)
Clarissa Borges.

 * <https://www.gnome.org/news/2020/05/patent-case-against-gnome-resolved/>
 * <https://www.gnome.org/news/2020/05/growing-together-with-gnome/>
 * <https://www.gnome.org/news/2020/05/gnome-welcomes-google-summer-of-code-2020-students/>

### MediaGoblin 0.10.0 released

*From May 1st by Ben Sturmfels*

We’re pleased to announce the release of MediaGoblin 0.10.0! It’s been
a while between releases for MediaGoblin, but work has continued
steadily. Highlights of this release include a new plugin for
displaying video subtitles and support for transcoding and displaying
video in multiple resolutions. There have also been a large number of
smaller improvements and bug fixes which are listed in the release
notes.

After enabling the new subtitles plugin, you can upload and edit
captions for your videos. Multiple subtitle tracks are supported, such
as for different languages. This feature was added by Saksham Agrawal
during Google Summer of Code 2016 and mentored by Boris Bobrov. The
feature has been available for some time on the master branch, but it
definitely deserves a mention for this release.

 * <https://mediagoblin.org/news/mediagoblin-0.10.0-release.html>

### Introducing Inkscape 1.0

*From May 4th by the Inkscape team*

After a little over three years in development, the team is excited to
launch the long awaited Inkscape 1.0 into the world. This
volunteer-built free software vector editor is used and recommended by
the FSF.

Built with the power of a team of volunteers, Inkscape
represents the work of many hearts and hands from around
the world, ensuring that it remains available free for everyone
to download and enjoy. In fact, translations for over 20 out of all 88
languages were updated for version 1.0, making the software more
accessible to people from all over the world.

A major milestone was achieved in enabling Inkscape to use a more
recent version of the software used to build the editor's user
interface (namely GTK+3). Users with HiDPI (high resolution) screens
can thank teamwork that took place during the 2018 Boston Hackfest for
setting the updated-GTK wheels in motion.

 * <https://inkscape.org/news/2020/05/04/introducing-inkscape-10/>

### This free software collective is taking Malayalam computing to the next level

*From May 6th by Azmia Riaz*

Swathanthra Malayalam Computing (SMC) is a free software collective in
India that was created with the intention of enabling the use of
Malayalam script in computers and mobile devices. Set up in 2002 by
Byju Muthukadan, a graduate of NIT Calicut, it espouses the ideology
of the FSF. The idea is not to simply make software free of cost, but
also to uphold the freedom behind how the language is incorporated
into technological devices. And SMC wants the community of Malayalam
speakers involved in the
solution. ([Malayalam](https://en.wikipedia.org/wiki/Malayalam) is one
of 22 scheduled languages of India, spoken by nearly 2.88% of Indians;
it is also spoken by linguistic minorities in neighboring states.)

 * <https://www.edexlive.com/people/2020/may/06/this-free-software-collective-is-taking-malayalam-computing-to-the-next-level-11834.html>

### GCC 10.1 released

*From May 7th by GCC*

The GNU Project and the GCC developers are pleased to announce the
release of GCC 10.1.

This is a major release, containing new features (as well as
many other improvements) relative to GCC 9.x.

GCC is one of the oldest programs in the GNU operating system, having
released its first version more than 33 years ago.

 * <https://gcc.gnu.org/gcc-10/>

### SeaGL going virtual due to COVID-19 aka novel coronavirus

*From June 1st by SeaGL organizers*

We have made the exciting decision to take SeaGL entirely virtual. We
are happy to follow in the footsteps of other terrific open source
conferences who also want to keep our communities together during this
time. The coronavirus has outlasted early predictions, so we are
taking steps to ensure the longevity of SeaGL as a community in the
event that we are still (or again) under shelter-in-place orders or
need to avoid gatherings. The conference will be held online on
November 13-14, 2020.

 * <https://seagl.org/news/2020/05/05/virtualconf-2020.html>

### HOPE 2020 will be an online event: Call for sessions open

*From May 19th by HOPE organizers*

The 2020 Hackers On Planet Earth conference (HOPE) will take place
online from July 25 through August 2, 2020. Hackers from around the
world will convene virtually for nine days of online presentations,
workshops, collaboration, and entertainment.

Health risks in 2020 make large gatherings and travel impractical for
attendees. Simultaneously, there is tremendous need for the creativity
and skill that hackers offer. HOPE 2020 will showcase the efforts
hackers are making to seek solutions to today's biggest challenges.

Shifting to an entirely online format means HOPE attendees from around
the world will convene from wherever they are to experience the same
types of great presentations and workshops that HOPE is known
for. This is a different way of doing things, and the HOPE community
will be there to help presenters do a good job.

Check out the link below to find out how to submit a talk!

 * <https://www.hope.net/news.html>

### May GNU Emacs news

*From May 25th by Sacha Chua*

In these issues: the state of Emacs Lisp on Guile; GNU Emacs raison
d'etre; analyzing data science code with R and Emacs; and more!

  * [2020-05-25](https://sachachua.com/blog/2020/05/2020-05-25-emacs-news/)
  * [2020-05-18](https://sachachua.com/blog/2020/05/2020-05-18-emacs-news/)
  * [2020-05-11](https://sachachua.com/blog/2020/05/2020-05-11-emacs-news/)
  * [2020-05-04](https://sachachua.com/blog/2020/05/2020-05-04-emacs-news/)

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit directory.fsf.org each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, June 5, from 12pm to 3pm EDT (16:00 to
19:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: Activism Guide

Every month on [the LibrePlanet
wiki](https://libreplanet.org/wiki/Main_Page), we highlight one
resource that is interesting and useful -- often one that could use
your help.

For this month, we are highlighting the Activism Guide, which is a
how-to guide for software freedom, digital rights, and free culture
activism. You are invited to adopt, spread and improve this important
resource.

  * <https://libreplanet.org/wiki/Activism_Guide>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 12 new GNU releases!

12 new GNU releases in the last month (as of May 26, 2020):

* [bison-3.6.2](https://www.gnu.org/software/bison/)
* [denemo-2.4.0](https://www.gnu.org/software/denemo/)
* [emms-5.4](https://www.gnu.org/software/emms/)
* [freeipmi-1.6.5](https://www.gnu.org/software/freeipmi/)
* [gcc-10.1.0](https://www.gnu.org/software/gcc/)
* [gdb-9.2](https://www.gnu.org/software/gdb/)
* [gnuastro-0.12](https://www.gnu.org/software/gnuastro/)
* [gnuhealth-3.6.4](https://www.gnu.org/software/health/)
* [mediagoblin-0.10.0](https://www.gnu.org/software/mediagoblin/)
* [nano-4.9.3](https://www.gnu.org/software/nano/)
* [nettle-3.6](https://www.gnu.org/software/nettle/)
* [parallel-20200522](https://www.gnu.org/software/parallel/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>. You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like to
offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### FSF and other free software events

 * July 22-28, 2020, online, [GUADEC 2020](https://www.gnome.org/news/2020/04/guadec-2020-moves-to-online-conference/)
 * October 18-20, 2020, Raleigh, NC, [ATO](https://2020.allthingsopen.org/)
 * November 13-14, 2020, online, [SeaGL](https://seagl.org/news/2020/05/05/virtualconf-2020.html)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2020supporters.html>

This month, a big Thank GNU to:

* Dario Armani
* David Klann
* Ken SENOO
* Ron Hume

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF (and allowed public
appreciation) in the past month:

* Bryan Wyatt (GDB, GCC, Binutils)
* Dima Akater (Emacs)
* James Thomas (Emacs)
* John Ravi (GCC)
* Juan Luis Rizos Garcia (Gnuastro)
* Kevin Foley (Emacs)
* Michael Builov (Gawk)
* Michael Weghorn (GDB)
* Naoya Yamashita (Emacs)
* Nicolas Bértolo (Emacs)
* Roland Coeurjoly (Emacs)
* Stephen Casner (Binutils)
* Yoosuk Sim (GCC)
* Yuuki Harano (Emacs)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Translations of the *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2020/junio>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2020/juin>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2020/junho>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something here
for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software patents,
Digital Restrictions Management (DRM), free software adoption,
OpenDocument, and more.

###

Copyright © 2020 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

