# Free Software Supporter
Issue 143, March 2020

Welcome to the *Free Software Supporter*, the Free Software
Foundation's (FSF) monthly news digest and action update -- being read
by you and 226,506 other activists. That's 1,364 more than last month!

### Hot off the presses: A sneak peek at the LibrePlanet 2020 schedule 

*From February 20th*

On March 14th and 15th, 2020, the free software community will come
together at the [Back Bay Events
Center](https://libreplanet.org/2020/getting-around/) to learn,
exchange ideas, catch up with friends, and plan the future of the
movement.

**[Register today!](https://my.fsf.org/civicrm/event/info?id=87&reset=1)
As always, FSF [associate members](https://my.fsf.org/join) and
students attend gratis.**

You can now dive into the [schedule of
speakers](https://libreplanet.org/2020/speakers/) already confirmed
for LibrePlanet 2020, with some more surprises to come!

We also encourage you to use the newly redesigned [LibrePlanet wiki
page](https://libreplanet.org/wiki/LibrePlanet:Conference) to learn
about and organize your own social events in and around the
conference.

 * <https://www.fsf.org/blogs/community/hot-off-the-presses-a-sneak-peek-at-the-libreplanet-2020-schedule>
 * <https://libreplanet.org/2020/>
 * <https://libreplanet.org/2020/speakers/>
 * <https://www.fsf.org/blogs/community/libreplanet-2020-needs-you-volunteer-today>

## TABLE OF CONTENTS

* Environmental activist Shannon Dosemagen joins FSF conference keynote lineup 
* Register today for LibrePlanet -- or organize your own satellite instance 
* Coming soon: A new site for fully free collaboration 
* Why freeing Windows 7 opens doors 
* Charity Navigator awards the FSF coveted four-star rating for the seventh time in a row 
* GNU-FSF cooperation update
* "I Love Free Software Day": Swipe (copy)left on dating apps 
* Thank you for supporting the FSF 
* (pre-)FOSDEM +++ ILoveFS +++ Community
* I Love Free Software Day on the go: The Replicant operating system in practice
* Wearable microphone jamming: The "bracelet of silence" is also a bracelet of freedom!
* Let's Encrypt has issued a billion certificates
* GCC source repository converted to git
* In case you ever want to unicycle 21,000 miles: You can find your way with OpenStreetMap!
* February GNU Emacs news
* Join the FSF and friends in updating the Free Software Directory
* LibrePlanet featured resource: LibrePlanet:Conference/2020
* GNU Spotlight with Mike Gerwitz: 22 new GNU releases!
* FSF and other free software events
* Thank GNUs!
* GNU copyright contributions
* Translations of the *Free Software Supporter*
* Take action with the FSF!

View this issue online here:
<https://www.fsf.org/free-software-supporter/2020/march>

Encourage your friends to subscribe and help us build an audience by
adding our subscriber widget to your Web site.

  * Subscribe: <https://www.fsf.org/free-software-supporter/subscribe>
  * Widget: <https://www.fsf.org/associate/widget>

Miss an issue? You can catch up on back issues at
<https://www.fsf.org/free-software-supporter>.

Want to read this newsletter translated into another language? Scroll
to the end to read the *Supporter* in French, Spanish, or Portuguese.

***

### Environmental activist Shannon Dosemagen joins FSF conference keynote lineup 

*From February 28th*

Shannon Dosemagen is the second confirmed keynote speaker for the
LibrePlanet conference. Dosemagen is the co-founder and current
executive director of Public Lab, a nonprofit organization creating
local environmental science solutions following the free software
philosophy, and winner of the FSF's Award for Projects of Social
Benefit. Shannon Dosemagen is an environmental health advocate and a
community science champion, and is enthusiastic about free systems and
technology that support the creation of a more just and equitable
future. At LibrePlanet, Dosemagen will discuss her experience
democratizing science to address environmental problem-solving.

 * <https://www.fsf.org/news/environmental-activist-shannon-dosemagen-joins-fsf-conference-keynote-lineup>

### Register today for LibrePlanet -- or organize your own satellite instance 

*From February 14th*

We envision that some day there will be satellite LibrePlanet
conference instances all over the globe livestreaming our annual
conference on technology and social justice -- and you can create your
own today! All you need is a venue, a screen, and a schedule of
LibrePlanet events, which we'll be releasing soon. This year, a free
software supporter in Ontario, Canada, has confirmed an event, and we
encourage you to host one, too.

 * <https://www.fsf.org/blogs/community/register-today-for-libreplanet-or-organize-your-own-satellite-instance>

### Coming soon: A new site for fully free collaboration 

*From February 25*

As we said in an [end-of-year post](https://www.fsf.org/blogs/sysadmin/the-fsf-tech-team-doing-more-for-free-software) highlighting our work supporting
free software development and infrastructure, the FSF is planning to
launch a public code hosting and collaboration platform ("forge"), to
launch in 2020. Members of the FSF tech team are currently reviewing
ethical Web-based software that helps teams work on their projects,
with features like merge requests, bug tracking, and other common
tools.

The new site will complement the current GNU and non-GNU Savannah
servers, which we will continue to support and improve, in
collaboration with their awesome volunteer team.

Up next for the FSF tech team is to do more research about systems
that have met our initial requirements, in order to find the best
options available. Once we know what we're interested in, we'll start
trying them out and performing more extensive tests.

Stay tuned to hear from us about the software stack we end up
choosing, and for our site launch announcement!

 * <https://www.fsf.org/blogs/sysadmin/coming-soon-a-new-site-for-fully-free-collaboration>

### Why freeing Windows 7 opens doors 

*From February 13th*

Since its launch on January 24th, we've had an overwhelming amount of
support in [our call to "upcycle" Windows
7](https://www.fsf.org/windows/upcycle-windows-7). After receiving
just a little short of twice of our goal of 7,777 signatures, our next
step was to mail Microsoft an upcycled hard drive along with the
signatures to Microsoft's corporate offices. It's as easy as copying
the source code, giving it a license notice, and mailing it back to
us. As the guardian of the most popular free software license in the
world, we're ready to give them all of the help we can. All they have
to do is ask.

We want them to show exactly how much love they have for the "open
source" software they mention in their advertising. If they really do
love free software -- and we're willing to give them the benefit of
the doubt -- they have the opportunity to show it to the world. We
hope they're not just capitalizing on the free software development
model in the most superficial and exploitative way possible: by using
it as a marketing tool to fool us into thinking that they care about
our freedom.

 * <https://www.fsf.org/blogs/community/why-freeing-windows-7-opens-doors>

### Charity Navigator awards the FSF coveted four-star rating for the seventh time in a row 

*From February 18th*

This month, Charity Navigator, an independent evaluator of US-based
nonprofit charities, awarded the FSF a four-star rating, the highest
available. According to the confirmation letter from Charity Navigator
president Michael Thatcher, this rating demonstrates the FSF's "strong
financial health and commitment to accountability and transparency."
This is our seventh time in a row receiving the coveted four-star
rating! Only 7% of the charities that Charity Navigator evaluates have
gotten this many in a row, and they assess over 9,000 charities a
year.

 * <https://www.fsf.org/blogs/community/charity-navigator-awards-the-fsf-coveted-four-star-rating-for-the-seventh-time-in-a-row>

### GNU-FSF cooperation update

*From February 6th*

The Free Software Foundation and the GNU Project leadership are
defining how these two separate groups cooperate. Our mutual aim is to
work together as peers, while minimizing change in the practical
aspects of this cooperation, so we can advance in our common free
software mission.

Alex Oliva, Henry Poole and John Sullivan (board members or officers
of the FSF), and Richard Stallman (head of the GNU Project), have been
meeting to develop a general framework which will serve as the
foundation for further discussion about specific areas of
cooperation. Together we have been considering the input received from
the public on <fsf-and-gnu@fsf.org> and
<gnu-and-fsf@gnu.org>. Comments from the community on this effort were
solicited through February 13th.

 * <https://www.fsf.org/news/gnu-fsf-cooperation-update>

### "I Love Free Software Day": Swipe (copy)left on dating apps 

*From February 11th*

Every year, Free Software Foundation Europe (FSFE) encourages
supporters to celebrate Valentine’s Day as “I Love Free Software Day,”
a day for supporters to show their gratitude to the people who enable
them to enjoy software freedom, including maintainers, contributors,
and other activists. It seems appropriate on this holiday to once
again address how seeking love on the Internet is, unfortunately,
laden with landmines for your freedom and privacy. But today, I’m also
going to make the argument that our community should think seriously
about developing a freedom-respecting alternative.

 * <https://www.fsf.org/blogs/community/on-i-love-free-software-day-swipe-copy-left-on-dating-apps>

### Thank you for supporting the FSF 

*From February 10th*

On January 17th, we closed the Free Software Foundation (FSF)'s end of
the year fundraiser and associate membership drive, bringing 368 new
associate members to the FSF community.

It is your support of the FSF that makes all of our work
possible. Your generosity impacts us on a direct level. It doesn't
just keep the lights on, but is also the source of our motivation to
fight full-time for software freedom. Your support is at the heart of
our work advocating for the use of copyleft and the GPL. It's also
what brought seventeen new devices to the RYF program this year, and
is what drives our campaigning against Digital Restrictions Management
(DRM). We are deeply grateful for the new memberships and donations we
have received this year, not to mention the existing members and
recurring donors that have enabled us to reach this point.

 * <https://www.fsf.org/blogs/community/thank-you-for-supporting-the-fsf>

### (pre-)FOSDEM +++ ILoveFS +++ Community

*From February 26 by Erik Albers*

From our own pre-FOSDEM event, to the exciting FOSDEM weekend, to I
Love Free Software Day, February was full of exciting news for the
FSFE. We used these occasions to present our work, as well as to offer
communities around Europe the opportunity to present their own. Read
about our booths and presentations, about love and upcoming events in
our February Newsletter.

 * <https://fsfe.org/news/nl/nl-202002.en.html>

### I Love Free Software Day on the go: The Replicant operating system in practice

*From February 14th by André Ockers*

On I Love Free Software Day 2020 I’d like to pay attention to and
thank the [Replicant operating system](https://www.replicant.us/),
which is in active development and empowers users to use free software
on the go.

As a user with a non-technical background it was an honor and a
privilege to attend the Replicant Birds of a Feather meeting at FOSDEM
2020. There I concluded that my choice for Replicant not only helps
the environment and strengthens the sustainability of my hardware, but
also that the project is in active development and will support more
contemporary hardware.

Donate to the Replicant project at
<https://my.fsf.org/civicrm/contribute/transact?reset=1&id=19>!

 * <https://en.o7s.eu/2020/02/14/i-love-free-software-on-the-go-the-replicant-operating-system-in-practice/>

### Wearable microphone jamming: The "bracelet of silence" is also a bracelet of freedom!

*From February 19th by Yuxin Chen, Huiying Li, Shan-Yuan Teng, Steven
Nagels, Zhijing Li, Pedro Lopes, Ben Y. Zhao, and Haitao Zheng*

Researchers at the University of Chicago came up with a "bracelet of
silence" to prevent Alexa and other in-home electronic spies from
listening in on conversations. Since they published their source code,
it's also a bracelet of freedom!

 * <http://sandlab.cs.uchicago.edu/jammer/>

### Let's Encrypt has issued a billion certificates

*From February 27th by Josh Aas and Sarah Gran*

Congratulations to Let's Encrypt, who issued their billionth
certificate on February 27, 2020! Let's Encrypt is a free, automated
certificate authority, run for the public’s benefit, which we use here
at the FSF, and in this blog post, they used this milestone as an
opportunity to reflect on what has changed for them, and for the
Internet, leading up to this event.

 * <https://letsencrypt.org/2020/02/27/one-billion-certs.html#>

### GCC source repository converted to git

*From January 13th by Joseph Myers*

The repository is now open for commits. Snapshot generation and
DATESTAMP updates from cron are now enabled. As of this reporting,
scripts to update online documentation were in process but not
finished. *Please help with updating documentation for using git with
GCC*. <https://gcc.gnu.org/ml/gcc-patches/2020-01/msg00623.html> has
a list of things that ought to be covered in git.html and
gitwrite.html (in the gcc-wwwdocs.git repository), most of which
aren't covered yet, and in general any hints and tips about git usage
that have come up in recent discussions should be added to the
documentation if not already there.

 * <https://gcc.gnu.org/ml/gcc/2020-01/msg00204.html>

### In case you ever want to unicycle 21,000 miles: You can find your way with OpenStreetMap!

*From February 9th by Caitlin Giddings*

Next time someone tells you that Google Maps is necessary, tell them
about this unicyclist who traveled 21,000 miles across the globe using
OpenStreetMap via Maps.me!

Nineteen-year-old Ed Pratt left his home in Somerset, England, on a
mission to become the first person to circle the globe on a
unicycle. Three years and 21,000 miles later -- after crossing Europe,
the Middle East, Asia, Australia, New Zealand, and the US -- he rolled
back to his starting point and a cheering 500-person crowd, successful
in both his final dismount (he was worried about that) and a new
record.

 * <https://www.outsideonline.com/2407533/unicycle-travel-gear>

### February GNU Emacs news

*From February 24th by Sacha Chua*

In these issues: ripgrepping with Helm, spicing up your prompt,
introducing Org-Roam, Emacs finally learns to ctrl-F, and more!

  * [2020-02-24](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29540)
  * [2020-02-17](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29539)
  * [2020-02-10](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29538)
  * [2020-02-03](https://sachachua.com/blog/category/geek/emacs/emacs-news/#post-29524)

### Join the FSF and friends in updating the Free Software Directory

Tens of thousands of people visit <https://directory.fsf.org> each month to
discover free software. Each entry in the Directory contains a wealth
of useful information, from basic category and descriptions to version
control, IRC channels, documentation, and licensing. The Free Software
Directory has been a great resource to software users over the past
decade, but it needs your help staying up-to-date with new and
exciting free software projects.

To help, join our weekly IRC meetings on Fridays. Meetings take place
in the #fsf channel on irc.freenode.org, and usually include a handful
of regulars as well as newcomers. Freenode is accessible from any IRC
client -- Everyone's welcome!

The next meeting is Friday, March 6th, from 12pm to 3pm EST (17:00 to
20:00 UTC). Details here:

  * <https://directory.fsf.org/wiki/Main_Page>

### LibrePlanet featured resource: LibrePlanet:Conference/2020

Every month on [the LibrePlanet
wiki](https://libreplanet.org/wiki/Main_Page), we highlight one
resource that is interesting and useful -- often one that could use
your help.

For this month, we are once again highlighting
LibrePlanet:Conference/2020, which provides information about the
upcoming LibrePlanet conference, happening here in Boston on March
14-15! This page is here to help you facilitate meetups with other
free software supporters at the conference, including themed dinners;
you can also sign up to do a Lightning Talk at
<https://libreplanet.org/wiki/LibrePlanet:Conference/2020/Lightning_Talks>. You
are invited to adopt, spread and improve this important resource.

  * <https://libreplanet.org/wiki/LibrePlanet:Conference/2020>

Do you have a suggestion for next month's featured resource? Let us
know at <campaigns@fsf.org>.

### GNU Spotlight with Mike Gerwitz: 22 new GNU releases!

22 new GNU releases in the last month (as of February 28, 2020):

* [binutils-2.34](https://www.gnu.org/software/binutils/)
* [bison-3.5.2](https://www.gnu.org/software/bison/)
* [datamash-1.6](https://www.gnu.org/software/datamash/)
* [ed-1.16](https://www.gnu.org/software/ed/)
* [gdb-9.1](https://www.gnu.org/software/gdb/)
* [glibc-2.31](https://www.gnu.org/software/libc/)
* [gnuhealth-3.6.3](https://www.gnu.org/software/health/)
* [guile-debbugs-0.0.3](https://www.gnu.org/software/guile-debbugs/)
* [gwl-0.2.0](https://www.gnu.org/software/gwl/)
* [hyperbole-7.1.0](https://www.gnu.org/software/hyperbole/)
* [jacal-1c6](https://www.gnu.org/software/jacal/)
* [libmicrohttpd-0.9.70](https://www.gnu.org/software/libmicrohttpd/)
* [libtasn1-4.16.0](https://www.gnu.org/software/libtasn1/)
* [nano-4.8](https://www.gnu.org/software/nano/)
* [ncurses-6.2](https://www.gnu.org/software/ncurses/)
* [octave-5.2.0](https://www.gnu.org/software/octave/)
* [parallel-20200222](https://www.gnu.org/software/parallel/)
* [scm-5f3](https://www.gnu.org/software/scm/)
* [screen-4.8.0](https://www.gnu.org/software/screen/)
* [shepherd-0.7.0](https://www.gnu.org/software/shepherd/)
* [slib-3b6](https://www.gnu.org/software/slib/)
* [wb-2b4](https://www.gnu.org/software/wb/)

For announcements of most new GNU releases, subscribe to the info-gnu
mailing list: <https://lists.gnu.org/mailman/listinfo/info-gnu>.

To download: nearly all GNU software is available from
<https://ftp.gnu.org/gnu/>, or preferably one of its mirrors from
<https://www.gnu.org/prep/ftp.html>.  You can use the URL
<https://ftpmirror.gnu.org/> to be automatically redirected to a
(hopefully) nearby and up-to-date mirror.

This month, we welcome Sanjay Bhatnagar as maintainer of the new package
[fussy](https://www.gnu.org/software/fussy/).

A number of GNU packages, as well as the GNU operating system as a
whole, are looking for maintainers and other assistance: please see
<https://www.gnu.org/server/takeaction.html#unmaint> if you'd like to
help. The general page on how to help GNU is at
<https://www.gnu.org/help/help.html>.

If you have a working or partly working program that you'd like
to offer to the GNU project as a GNU package, see
<https://www.gnu.org/help/evaluation.html>.

As always, please feel free to write to us at <maintainers@gnu.org>
with any GNUish questions or suggestions for future installments.

### FSF and other free software events

 * March 14-15, 2020, Boston, MA, [LibrePlanet 2020: Free the Future](https://www.fsf.org/events/libreplanet-2020-march14-15-registration-open)
 * April 24-26, 2020, Bellingham, WA, [LFNW 2020](https://www.linuxfestnorthwest.org/conferences/2020)
 * July 22-28, 2020, Zacatecas, Mexico, [GUADEC 2020](https://www.gnome.org/news/2019/11/guadec-2020-announcement/)

### Thank GNUs!

We appreciate everyone who donates to the Free Software Foundation,
and we'd like to give special recognition to the folks who have
donated $500 or more in the last month.

  * <https://www.gnu.org/thankgnus/2020supporters.html>

This month, a big Thank GNU to:

* Ed Price
* Evan Klitzke
* James Wilson
* Pat Ryan
* René Genz
* Valerio Poggi 

You can add your name to this list by donating at
<https://donate.fsf.org/>.

### GNU copyright contributions

Assigning your copyright to the Free Software Foundation helps us
defend the GNU GPL and keep software free. The following individuals
have assigned their copyright to the FSF (and allowed public
appreciation) in the past month:

* Christophe Seguinot (GNU Radio)
* Igor Antonio Auad Freire (GNU Radio)
* Pieter van Oostrum (Emacs)
* tsuucat (Emacs)
* Vyacheslav Petrishchev (Wget)

Want to see your name on this list? Contribute to GNU and assign your
copyright to the FSF.

* <https://www.gnu.org/server/takeaction.html#dev>

### Translations of the *Free Software Supporter*

El Free Software Supporter está disponible en español. Para ver la
versión en español haz click aqui:
<https://www.fsf.org/free-software-supporter/2020/marzo>

**Para cambiar las preferencias de usuario y recibir los próximos
  números del Supporter en español, haz click aquí:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

Le Free Software Supporter est disponible en français. Pour voir la
version française cliquez ici:
<https://www.fsf.org/free-software-supporter/2020/mars>

**Pour modifier vos préférences et recevoir les prochaines
  publications du Supporter en français, cliquez ici:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

O Free Software Supporter está disponível em português. Para ver a
versão em português, clique aqui:
<https://www.fsf.org/free-software-supporter/2020/marco>

**Para alterar as preferências do usuário e receber as próximas
  edições do Supporter em português, clique aqui:**
  <https://my.fsf.org/civicrm/profile/create?reset=1&gid=34&id={contact.contact_id}&{contact.checksum}>

### Take action with the FSF!

Contributions from thousands of individual members enable the FSF's
work. You can contribute by joining at <https://my.fsf.org/join>. If
you're already a member, you can help refer new members (and earn some
rewards) by adding a line with your member number to your email
signature like:

  I'm an FSF member -- Help us support software freedom!
  <https://my.fsf.org/join>

The FSF is always looking for volunteers
(<https://www.fsf.org/volunteer>). From rabble-rousing to hacking,
from issue coordination to envelope stuffing -- there's something here
for everybody to do. Also, head over to our campaigns section
(<https://www.fsf.org/campaigns>) and take action on software patents,
Digital Restrictions Management (DRM), free software adoption,
OpenDocument, and more.

###

Copyright © 2020 Free Software Foundation, Inc.

This work is licensed under the Creative Commons Attribution 4.0
Unported License. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

