VENV       := venv
DESTDIR    := public
PYTHON     := $(shell which python3)
TREE       := $(shell which tree)

SRCS       := $(wildcard **/*_pt-BR.mdwn)
DESTS       = $(addprefix $(DESTDIR)/,$(patsubst %.mdwn,%.html,$(SRCS)))

.PHONY: build pages help clean

help:
	@echo "Please use 'make <target>' where <target> is one of:"
	@echo "  build     Build HTML from Markdown files and store in '$(DESTDIR)/'"
	@echo "  pages     Set up pages for publishing (also runs 'build' target)"
	@echo "  clean     Clean files and directory yielded by the above targets"
	@echo "  help      Print this message and quit"
	@echo ""
	@echo "Hint: a single page can built by overriding SRCS's value with a"
	@echo "     .mdwn file, e.g. make build SRCS=2020/april-2020_pt_BR.mdwn"
	@echo ""

build: venv $(DESTS)

pages: build checktree makeindexes

$(DESTDIR)/%.html: %.mdwn
	@echo "Processing $< ..."
	@mkdir -p $(@D)
	@$(VENV)/bin/python -m markdown -v -e UTF-8 -o html $< > $@

venv:
	@echo "Setting up the environment ..."
	@if [ -z "$(PYTHON)" ]; then \
	  @echo "'python' binary not found, but required. Please install it." >&2; \
	  @exit 1; \
	fi
	@if [ ! -d $(VENV) ]; then $(PYTHON) -m venv $(VENV); fi
	@$(VENV)/bin/python -m pip install --upgrade pip
	@$(VENV)/bin/pip install --upgrade markdown

checktree:
	@if [ -z "$(TREE)" ]; then \
	  @echo "'tree' binary not found, but required. Please install it." >&2; \
	  @exit 1; \
	fi

makeindexes:
	dirs="$(shell find $(DESTDIR) -type d)"; \
	title="FSS pt_BR translation"; \
	h1="Free Software Supporter pt_BR translation"; \
	for dir in $${dirs}; do \
	  echo "Creating $${dir}/index.html ..."; \
	  cd $${dir} && \
	  $(TREE) -H '.' -L 2 --noreport --charset utf-8 -I index.html > index.html && \
	  sed -i "/<title>/s/Directory Tree/$${title}/" index.html && \
	  sed -i "s|Directory Tree|$${dir} - $${h1}|" index.html && \
	  cd $${OLDPWD}; \
	done

clean:
	rm -rf $(DESTDIR) $(VENV)
