# fss-translation

[![build status](https://gitlab.com/rafaelff/fss-translation/badges/master/pipeline.svg)](https://gitlab.com/rafaelff/fss-translation/badges/master/pipeline.svg)

Project to coordinate the Brazilian Portuguese localization of
[Free Software Supporter](https://www.fsf.org/fss/). <br>
See the HTML-rendered translation at
https://rafaelff.gitlab.io/fss-translation

Projeto para coordenar a localização para português brasileiro do
[Suporte ao Software Livre](https://www.fsf.org/fss/). <br>
Veja a tradução renderizada em HTML em
https://rafaelff.gitlab.io/fss-translation

## Status de tradução:

<details open>
  <summary>
      <strong>[2020](https://www.fsf.org/free-software-supporter/2020)</strong>
  </summary>

|   Mês     |      Arquivo markdown        |                Link para artigo na FSF                            |   Status    |
|-----------|------------------------------|-------------------------------------------------------------------|-------------|
| Janeiro   | [january_2020_pt-BR.mdwn]    | https://www.fsf.org/free-software-supporter/2020/janeiro          | Concluído   |
| Fevereiro | [february_2020_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2020/fevereiro        | Concluído   |
| Março     | [march_2020_pt-BR.mdwn]      | https://www.fsf.org/free-software-supporter/2020/marco            | Concluído   |
| Abril     | [april_2020_pt-BR.mdwn]      | https://www.fsf.org/free-software-supporter/2020/abril-p          | Concluído   |
| Maio      | [may_2020_pt-BR.mdwn]        | https://www.fsf.org/free-software-supporter/2020/maio             | Concluído   |
| Junho     | [june_2020_pt-BR.mdwn]       | https://www.fsf.org/free-software-supporter/2020/junho            | Trabalhando |
| Julho     |                              |                                                                   |             |
| Agosto    |                              |                                                                   |             |
| Setembro  |                              |                                                                   |             |
| Outubro   |                              |                                                                   |             |
| Novembro  |                              |                                                                   |             |
| Dezembro  |                              |                                                                   |             |

</details>

<details>
  <summary>
      <strong>[2019](https://www.fsf.org/free-software-supporter/2019)</strong>
  </summary>


|   Mês     |      Arquivo markdown        |                Link para artigo na FSF                            |   Status    |
|-----------|------------------------------|-------------------------------------------------------------------|-------------|
| Janeiro   | [january_2019_pt-BR.mdwn]    | https://www.fsf.org/free-software-supporter/2019/janeiro          | Concluído   |
| Fevereiro | [february_2019_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2019/fevereiro        | Concluído   |
| Março     | [march_2019_pt-BR.mdwn]      | https://www.fsf.org/free-software-supporter/2019/marco            | Concluído   |
| Abril     | [april_2019_pt-BR.mdwn]      | https://www.fsf.org/free-software-supporter/2019/abril-portuguese | Concluído   |
| Maio      | [may_2019_pt-BR.mdwn]        | https://www.fsf.org/free-software-supporter/2019/maio             | Concluído   |
| Junho     | [june_2019_pt-BR.mdwn]       | https://www.fsf.org/free-software-supporter/2019/free-software-supporter-edicao-134-junho-2019  | Concluído   |
| Julho     | [july_2019_pt-BR.mdwn]       | https://www.fsf.org/free-software-supporter/2019/julho            | Concluído   |
| Agosto    | [august_2019_pt-BR.mdwn]     | https://www.fsf.org/free-software-supporter/2019/agosto-pr        | Concluído   |
| Setembro  | [september_2019_pt-BR.mdwn]  | https://www.fsf.org/free-software-supporter/2019/setembro         | Concluído   |
| Outubro   | [october_2019_pt-BR.mdwn]    | https://www.fsf.org/free-software-supporter/2019/outubro          | Concluído   |
| Novembro  | [november_2019_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2019/novembro         | Concluído   |
| Dezembro  | [december_2019_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2019/dezembro         | Concluído   |

</details>

<details>
  <summary>
    <strong>[2018](https://www.fsf.org/free-software-supporter/2018)</strong>
  </summary>

|   Mês     |      Arquivo markdown        |                Link para artigo na FSF                             |   Status    |
|-----------|------------------------------|--------------------------------------------------------------------|-------------|
| Maio      | [may_2018_pt-BR.mdwn]        | https://www.fsf.org/free-software-supporter/2018/maio              | Concluído   |
| Junho     | [june_2018_pt-BR.mdwn]       | https://www.fsf.org/free-software-supporter/2018/junho             | Concluído   |
| Julho     | [july_2018_pt-BR.mdwn]       | https://www.fsf.org/free-software-supporter/2018/julho             | Concluído   |
| Agosto    | [august_2018_pt-BR.mdwn]     | https://www.fsf.org/free-software-supporter/2018/agosto-portuguese | Concluído   |
| Setembro  | [september_2018_pt-BR.mdwn]  | https://www.fsf.org/free-software-supporter/2018/setembro          | Concluído   |
| Outubro   | [october_2018_pt-BR.mdwn]    | https://www.fsf.org/free-software-supporter/2018/outubro           | Concluído   |
| Novembro  | [november_2018_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2018/novembro          | Concluído   |
| Dezembro  | [december_2018_pt-BR.mdwn]   | https://www.fsf.org/free-software-supporter/2018/dezembro          | Concluído   |

</details>

## Procedimento para tradução dos boletins mensais do FSS

1. Obtenha o boletim mensal do FSS com campaigns@fsf.org
2. Adicione este arquivo junto com uma cópia a ser traduzida, distinguindo-a com "_pt-BR", ao *branch* `master` dentro da pasta do ano correspondente (ex.: arquivo `april_2020.mdwn` deve ter uma cópia não traduzida `april_2020_pt-BR.mdwn` e ambos devem ser colocados na pasta `2020/`)
3. Edite o arquivo README adicionando esta entrada de tradução em andamento, utilizando um status "Trabalhando", ainda no *branch* `master`
3. Em seguida, crie novo *branch* com nome composto mês e ano; por exemplo: ```git checkout -b abril-2020```
4. Sem alterar nada neste novo *branch*, crie uma nova *merge request* <abbr title="Work In Progress">WIP</abbr> a partir deste *branch* para coordenar os trabalhos de tradução; por exemplo: ```git push --set-upstream origin abril-2020```
5. Faça as traduções neste *branch* até que esteja 100% pronto
6. Uma vez pronto a tradução do boletim FSS, edite o README renomeie a *merge request* retirando `WIP: ` do nome para possibilitar fazer a mesclagem do *branch* de tradução ao *master*
7. Faça o merge, e envie o arquivo Markdown traduzido do FSS para a FSF publicar

Preferencialmente, mas não obrigatoriamente, use mensagens de commit comuns durante a tradução para facilitar encontrar no histórico do repositório:

| Sugestão de mensagem de commit | Descrição |
|--------------------------------|-----------|
| *Prepara MÊS ANO para ser traduzido* | Commit no qual se adiciona os arquivos Markdown ao repositório, junto com atualização da tabela de traduções com status "Trabalhando" deste arquivo README (atenção para erros de Ctrl+C e Ctrl+V, como link para ano anterior) |
| *Traduz primeira parte de MÊS ANO* | Commit adicionando tradução parcial para o mês; no caso, somente da primeira metade supondo divisão entre duas pessoas |
| *Traduz segunda parte de MÊS ANO* | Commit adicionando tradução parcial para o mês; no caso, somente da segunda metade supondo divisão entre duas pessoas |
| *Ajustes finais em MÊS ANO* | Ajustes envolvendo correção de erros de escrita (revisar com corretor ortográfico e com leitura), verificação de links (tradução de máquina costuma quebrar `](` em `] (`, o que é ruim) quebra de linha em 70 caracteres, testar o resultado (ex., usando [Markdown-Editor]) |

[january_2020_pt-BR.mdwn]: 2020/january_2020_pt-BR.mdwn
[february_2020_pt-BR.mdwn]: 2020/february_2020_pt-BR.mdwn
[march_2020_pt-BR.mdwn]: 2020/march_2020_pt-BR.mdwn
[april_2020_pt-BR.mdwn]: 2020/april_2020_pt-BR.mdwn
[may_2020_pt-BR.mdwn]: 2020/may_2020_pt-BR.mdwn
[june_2020_pt-BR.mdwn]: 2020/june_2020_pt-BR.mdwn

[january_2019_pt-BR.mdwn]: 2019/january_2019_pt-BR.mdwn
[february_2019_pt-BR.mdwn]: 2019/february_2019_pt-BR.mdwn
[march_2019_pt-BR.mdwn]: 2019/march_2019_pt-BR.mdwn
[april_2019_pt-BR.mdwn]: 2019/april_2019_pt-BR.mdwn
[may_2019_pt-BR.mdwn]: 2019/may_2019_pt-BR.mdwn
[june_2019_pt-BR.mdwn]: 2019/june_2019_pt-BR.mdwn
[july_2019_pt-BR.mdwn]: 2019/july_2019_pt-BR.mdwn
[august_2019_pt-BR.mdwn]: 2019/august_2019_pt-BR.mdwn
[september_2019_pt-BR.mdwn]: 2019/september_2019_pt-BR.mdwn
[october_2019_pt-BR.mdwn]: 2019/october_2019_pt-BR.mdwn
[november_2019_pt-BR.mdwn]: 2019/november_2019_pt-BR.mdwn
[december_2019_pt-BR.mdwn]: 2019/december_2019_pt-BR.mdwn

[may_2018_pt-BR.mdwn]: 2018/may_2018_pt-BR.mdwn
[june_2018_pt-BR.mdwn]: 2018/june_2018_pt-BR.mdwn
[july_2018_pt-BR.mdwn]: 2018/july_2018_pt-BR.mdwn
[august_2018_pt-BR.mdwn]: 2018/august_2018_pt-BR.mdwn
[september_2018_pt-BR.mdwn]: 2018/september_2018_pt-BR.mdwn
[october_2018_pt-BR.mdwn]: 2018/october_2018_pt-BR.mdwn
[november_2018_pt-BR.mdwn]: 2018/november_2018_pt-BR.mdwn
[december_2018_pt-BR.mdwn]: 2018/december_2018_pt-BR.mdwn

[Markdown-Editor]: https://pypi.org/project/Markdown-Editor